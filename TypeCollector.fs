﻿module LuaAnalyzer.TypeCollector

open Type
open TypedSyntax
open Context

type TypeCollector = {

    /// The name of the type collector. Used to report compatiblity errors
    /// among multiple type collecters.
    name : string

    /// Takes a string specifying a module path, a string specifying a file name, and a syntax tree
    /// for the file located at that path, and returns an external typename's range if any, 
    /// a list of names of the internal types defined in the file along with the ranges
    /// their definitions appear in, as well as a list of subtype edges defined 
    /// in the module. Typically, a module will add implicit subtype edges from 
    /// its internal types to its external type, as well as subtype edges from
    /// the external type to the types of any classes or traits it inherits from.
    /// In addition, a list of all errors encountered is returned.
    typeGraphBuilder : string -> string -> TypedStatement -> Option<(int*int)*List<string*(int*int)>*List<string*string>>*List<Error>
    
    /// If this type collector expects the type graph to be a tree, this should
    /// be set to true; this will cause errors whenever a viloation of the tree 
    /// property is detected.
    ///
    /// Otherwise, the type graph is expected to be a DAG.
    /// In either case, errors will be produced if the graph contains 
    /// cycles.
    typeGraphIsTree : bool

    /// Takes a string specifying a module path, a string specifying a file name, and a syntax tree
    /// for the file located at that path. Returns a pair (ext,cons),
    /// where ext is the external type defined by this module and cons is its constructor type
    /// In addition, a list of all errors encountered is returned.
    externalTypeBuilder : TypeEnvironment -> string -> string -> TypedStatement -> Option<Type*Type>*List<Error>
    
    /// Takes a type environment, a global context, a module name, a file name, and a syntax tree
    /// for the file located at that path. Returns a list of all internal
    /// types defined by the module if any.
    /// In addition, returns: 
    /// 1.) errors - a list of all errors encountered is returned
    /// 2.) dependencies - a list of names of all modules containing types whose structure was leveraged to build the internal types of the specified module.
    ///                    modules defining supertypes need not be included in this list; we always treat those as dependencies.
    internalTypeBuilder : Context -> string -> string -> TypedStatement -> Map<string, Type>*List<Error>*List<string>
    
    /// Given a string specifying a path, along with an ast,
    /// prepare the ast for typechecking by giving types to parameters and possibly 
    /// adding ascriptions.
    decorate : Context -> string -> TypedStatement -> TypedStatement 

    /// Detects miscellaneous, class-system specific errors in the syntax tree
    /// params: Type environment, module path, file path, syntax tree
    detectMiscErrors : Context -> string -> string -> TypedStatement -> unit
}

/// All type collectors which were loaded from plugins
let typeCollectors : Ref< List<TypeCollector> > = ref [] 

let addTypeCollector (collector : TypeCollector) =
    typeCollectors := collector :: !typeCollectors

let getTypeCollectorByName (typeCollectorName : string) : Option<TypeCollector> =
    List.tryFind (fun coll -> coll.name = typeCollectorName) !typeCollectors 
