﻿module LuaAnalyzer.HandLexer

open Token
open Annotations

type GenList<'T> = System.Collections.Generic.List<'T>

/// Index of the next character to lex
let i = ref 0

/// The string that we are lexing
let s = ref ""

/// The index of the beginning of the current token's
/// lexeme
let tokenStart = ref 0

let lineBoundaries = new GenList<int>()

/// The number of the current line
let line = ref 0

/// The absolute offset of the beginning of the current line
let lineStart = ref 0

let currentFileName = ref ""

/// Maps token positions to the annotations associated with those
/// tokens
let annotationMap : Ref< Map<int, Annotation> > = ref (Map.empty)

/// the string, start character offset, and type of the current annotation, if any
let annotation : Ref<Option<string*int*AnnotationCommentType>> = ref None

/// the header annotation for the current file, if any
let headerAnnotation : Ref< Option<Annotation> > = ref None


/// Every time we finish lexing an annotation, we pass it to this function,
/// which records it if it is the module header annotation
let processAnnotation() =
    match !annotation with
    | Some(contents,startPos,commentType) ->
        let parsedAnnotation = parseAnnotation contents startPos commentType !currentFileName
        match parsedAnnotation with
        | (_,ModuleHeaderTagSet(_,_),_,_) as header ->
            headerAnnotation := Some(header)
        | _ ->
            ()
    | _ ->
        ()

let ret = new GenList<TokenInfo>(1200)

let getColumn () = 
    (!i)-(!lineStart)

let isDigit(x : char) =
    (x >= '0' && x <= '9')

let isAlpha(x : char) =
    (x >= 'a' && x <= 'z') ||
    (x >= 'A' && x <= 'Z') ||
    x = '_'

let isSpace(x : char) : bool =
    (x = '\t') || (x = '\r') || (x = '\n') || (x = ' ')

let notIdChar(x : char) : bool =
    not ( (isAlpha x) || (isDigit x) )

let addAnnotation () =
    match !annotation with
    | Some (contents,pos,commentType) ->
        annotationMap := (!annotationMap).Add(ret.Count, Annotations.parseAnnotation contents pos commentType !currentFileName) 
    | None ->
        ()

let lexName() : TokenInfo =
    addAnnotation()

    let str = !s
    while !i < (!s).Length && ((isDigit (str.Chars !i)) || (isAlpha (str.Chars !i))) do
        i := !i + 1
        
    let i0,i1 = !tokenStart, !i
    (Token.NAME, Some((!s).Substring(i0, i1-i0)), i0)

let matches (matchStr : string) =
    let str = !s

    if matchStr.Length > str.Length - !i then
        false
    else
        let a = (str.Substring(!i, matchStr.Length) = matchStr)
        let b = (!i + matchStr.Length = str.Length) ||  (notIdChar (str.Chars (!i + matchStr.Length)))
        a && b 

exception MalformedNumber of string
/// LexError(Error message, position, line)
exception LexError of string*int*int*GenList<int>

let lexStringSingle () : TokenInfo =
    let str = !s
    let tokenStart = !i

    //Skip past '
    i := !i + 1

    let slashCount = ref 0

    while !i < str.Length && not ((str.Chars !i) = '\'' && !slashCount%2 = 0) do
        if (str.Chars !i) = '\n' then
            lineBoundaries.Add(!i)
            line := !line + 1
            lineStart := !i + 1

        if (str.Chars !i) = '\\' then
            slashCount := !slashCount + 1
        else
            slashCount := 0

        i := !i + 1

    if !i = str.Length then
        (Token.BAD_TOKEN, Some("string beginning at line " + line.ToString() + " unfinished"), tokenStart)   
    else
        i := !i + 1
        (Token.STRING, Some(str.Substring(tokenStart+1, !i-tokenStart-2)), tokenStart+1)

let lexStringDouble () : TokenInfo =
    let str = !s
    let tokenStart = !i

    let slashCount = ref 0

    //Skip past "
    i := !i + 1

    while !i < str.Length && not ((str.Chars !i) = '\"' && !slashCount%2 = 0) do
        if (str.Chars !i) = '\n' then
            lineBoundaries.Add(!i)
            line := !line + 1
            lineStart := !i + 1

        if (str.Chars !i) = '\\' then
            slashCount := !slashCount + 1
        else
            slashCount := 0

        i := !i + 1

    if !i = str.Length then
        (Token.BAD_TOKEN,Some("string beginning at line " + line.ToString() + " unfinished"),tokenStart+1)
    else 
        i := !i + 1
        (Token.STRING, Some(str.Substring(tokenStart+1, !i-tokenStart-2)), tokenStart+1)

let lexComment() =
    let str = !s
    let commentStart = !i
    let commentStartLine = !line

    let lexLineComment () =
        while not (!i >= str.Length || ((str.Chars !i) = '\n')) do
            i := !i + 1
        lineBoundaries.Add(!i)
        i := !i + 1
        let additionalContents = str.Substring(commentStart+2, !i-1-(commentStart+2))
        match !annotation with
        | Some(contents,startPos,ShortCommentAnnotation) ->
            annotation := Some(contents + "\n" + additionalContents,startPos,ShortCommentAnnotation)
        | _ ->
            processAnnotation()
            annotation := Some(additionalContents, commentStart, ShortCommentAnnotation)

    let lexLongComment (openCount : int) =
        let finished = ref false
        while not !finished do
            while not ((str.Chars !i) = ']') do
                if (str.Chars !i) = '\n' then
                    lineBoundaries.Add(!i)
                    line := !line + 1
                    lineStart := !i + 1

                i := !i + 1

                if (!i = str.Length) then
                    raise( LexError("Comment started at line " + commentStartLine.ToString() + " never closed.", commentStart, commentStartLine, lineBoundaries) )
                
            i := !i + 1

            if (!i = str.Length) then
                raise( LexError("Comment started at line " + commentStartLine.ToString() + " never closed.", commentStart, commentStartLine, lineBoundaries) )

            let count = ref 0
            while ( (str.Chars !i) = '=') do
                i := !i + 1
                   
                if (!i = str.Length) then
                    raise( LexError("Comment started at line " + commentStartLine.ToString() + " never closed.", commentStart, commentStartLine, lineBoundaries) )
                
                count := !count + 1
            

            if (str.Chars !i) = ']' && !count = openCount then
                i := !i + 1

                if (!i = str.Length) then
                    raise( LexError("Comment started at line " + commentStartLine.ToString() + " never closed.", commentStart, commentStartLine, lineBoundaries) )
                
                let startPos = commentStart+openCount+4
                let endPos = (!i-openCount-2)
                annotation := Some(str.Substring(startPos,endPos-startPos), startPos, LongCommentAnnotation)
                processAnnotation()
                finished := true
            
    if (str.Chars (!i+2)) = '[' then
        i := !i + 3
        let openCount = ref 0
        while (str.Chars !i) = '=' do
            i := !i + 1
            openCount := !openCount + 1

        if (str.Chars !i) = '[' then
            lexLongComment !openCount
        else
            lexLineComment()
            
    else
        lexLineComment()

let lexLongString () : TokenInfo =
    let str = !s
    let stringStart = !i
    let stringStartLine = !line
    
    let lexInterior (openCount : int) =
        let finished = ref false
        while not !finished do
            while not ((str.Chars !i) = ']') do
                if (str.Chars !i) = '\n' then
                    lineBoundaries.Add(!i)
                    line := !line + 1
                    lineStart := !i + 1

                i := !i + 1

                if (!i = str.Length) then
                    raise( LexError("String started at line " + stringStartLine.ToString() + " never closed.", stringStart, stringStartLine, lineBoundaries) )
                

            i := !i + 1
            
            if (!i = str.Length) then
                raise( LexError("String started at line " + stringStartLine.ToString() + " never closed.", stringStart, stringStartLine, lineBoundaries) )
            
            let count = ref 0
            while (str.Chars !i) = '=' do
                i := !i + 1

                if (!i = str.Length) then
                    raise( LexError("String started at line " + stringStartLine.ToString() + " never closed.", stringStart, stringStartLine, lineBoundaries) )
                

                count := !count + 1
        
            if (str.Chars !i) = ']' && !count = openCount then
                i := !i + 1
                finished := true
        
        let startPos = stringStart+openCount+2
        let endPos = !i-openCount-2
        (Token.STRING, Some(str.Substring(startPos,endPos-startPos)), startPos)

    i := !i + 1
    let openCount = ref 0
    while (str.Chars !i) = '=' do
        i := !i + 1
        openCount := !openCount + 1

    if (str.Chars !i) = '[' then
        lexInterior !openCount
    else
        (Token.BAD_TOKEN, Some("expected '['"), !i)

let lexHexNumber () : TokenInfo =
    let str = !s
    let tokenStart = !i

    let isHexDigit (c : char) =
        ( (c >= '0') && (c <= '9') ) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f')

    let eatDigits() = 
        while isHexDigit (str.Chars !i) do
            i := !i + 1

    i := !i + 2

    if not (isHexDigit (str.Chars !i)) then
        raise(MalformedNumber("expected hex digit"))

    eatDigits()
    if (str.Chars !i) = '.' then
        i := !i + 1
    eatDigits()

    if (str.Chars !i) = 'P' || (str.Chars !i) = 'p' then
        i := !i + 1

        if (str.Chars !i) = '+' || (str.Chars !i) = '-' then
            i := !i + 1

        if not (isHexDigit (str.Chars !i)) then
            raise(MalformedNumber("expected hex digit"))

        eatDigits()

    (Token.NUMBER, Some(str.Substring(tokenStart,!i-tokenStart)), tokenStart)
    

let lexNumber () : TokenInfo =
    let str = !s

    let eatDigits() = 
        while !i < str.Length && isDigit (str.Chars !i) do
            i := !i + 1
    
    let eatExponent() =
        //advance past 'e'
        i := !i + 1

        if (str.Chars !i) = '+' || (str.Chars !i) = '-' then
            i := !i + 1

        if not (isDigit (str.Chars !i)) then
            raise(MalformedNumber("expected digit"))
        else
            eatDigits()

    let eatNumber() =
        eatDigits()

        if (str.Chars !i) = '.' then
            i := !i + 1

            if isSpace (str.Chars !i) then
                ()
            elif isDigit (str.Chars !i) then
                eatDigits()
                if (str.Chars !i) = 'e' then
                    eatExponent()
                else
                    ()
            else
                ()

        elif (str.Chars !i) = 'e' then
            eatExponent() 
        else
            ()

    try
        eatNumber()
        //TODO: there's no need to parse the number into a numeric value for now,
        //so we just pretend it's 0
        (Token.NUMBER,Some (str.Substring(!tokenStart,!i - !tokenStart)), !tokenStart)
    with
    | MalformedNumber(error) ->
        (Token.BAD_TOKEN, Some ("malformed number -- " + error), !tokenStart)
             


let lexTokenA() : TokenInfo =
    i := !i + 1
    let next = (!s).Chars !i
    match next with
    | 'n' ->
        i := !i + 1
        let next = (!s).Chars !i
        match next with
        | 'd' ->
            i := !i + 1
            (Token.AND,None,!tokenStart)
        | _ ->
            lexName()
    | _ ->
        lexName()

let lexTokenB() : TokenInfo =
    if matches "break" then
        i := !i + 5
        (Token.BREAK, None, !tokenStart) 
    else
        lexName()

let lexTokenD() : TokenInfo =
    if matches "do" then
        i := !i + 2
        (Token.DO, None, !i-2)
    else
        lexName() 

let lexTokenE() : TokenInfo =
    if matches "elseif" then
        i := !i + 6
        (Token.ELSEIF, None, (!i-6))
    else if matches "else" then
        i := !i + 4
        (Token.ELSE, None, (!i-4))
    else if matches "end" then
        i := !i + 3
        (Token.END, None, (!i-3))
    else
        lexName()

let lexTokenF () : TokenInfo =
    if matches "for" then
        i := !i + 3
        (Token.FOR, None, (!i-3))
    elif matches "false" then
        i := !i + 5
        (Token.FALSE, None, (!i-5))
    elif matches "function" then
        i := !i + 8
        addAnnotation()
        (Token.FUNCTION, None, !i-8)
    else
        lexName()

let lexTokenI () : TokenInfo =
    if matches "if" then
        i := !i + 2
        (Token.IF, None, !i-2)
    elif matches "in" then
        i := !i + 2
        (Token.IN, None, !i-2)
    else
        lexName()

let lexTokenL () : TokenInfo =
    if matches "local" then
        i := !i + 5
        addAnnotation()
        
        (Token.LOCAL,None, !i-5)
    else
        lexName()

let lexTokenN () : TokenInfo =
    if matches "nil" then
        i := !i + 3
        (Token.NIL, None, !i-3)
    elif matches "not" then
        i := !i + 3
        (Token.NOT, None, !i-3)
    else
        lexName()

let lexTokenO () : TokenInfo =
    if matches "or" then
        i := !i + 2
        (Token.OR, None, !i-2)
    else
        lexName()

let lexTokenR () : TokenInfo =
    if matches "repeat" then
        i := !i + 6
        (Token.REPEAT, None, !i-6)
    elif matches "return" then
        i := !i + 6
        (Token.RETURN, None, !i-6)
    else
        lexName()

let lexTokenT () : TokenInfo =
    if matches "then" then
        i := !i + 4
        (Token.THEN, None, !i-4)
    elif matches "true" then
        i := !i + 4
        (Token.TRUE, None, !i-4)
    else
        lexName()

let lexTokenU () : TokenInfo =
    if matches "until" then
        i := !i + 5
        (Token.UNTIL, None, !i-5)
    else
        lexName()
        
let lexTokenW () : TokenInfo =
    if matches "while" then
        i := !i + 5
        (Token.WHILE, None, !i-5)
    else
        lexName()

let lexTokenDot () : TokenInfo =
    let str = !s
    if not (str.Chars (!i+1) = '.') then
        if isDigit (str.Chars (!i+1)) then
            lexNumber()
        else
            i := !i + 1
            (Token.DOT, None, !i-1)
    else if not (str.Chars (!i+2) = '.') then
        i := !i + 2
        (Token.CONCAT, None, !i-2)
    else
        i := !i + 3
        (Token.VARARGS, None, !i-3)

let lexTokenEq () : TokenInfo =
    let str = !s
    if str.Chars (!i+1) = '=' then
        i := !i + 2
        (Token.EQUAL, None, !tokenStart)
    else
        i := !i + 1
        (Token.ASSIGN, None, !tokenStart)

let lexTokenGt () : TokenInfo =
    let str = !s
    if str.Chars (!i + 1) = '=' then
        i := !i + 2
        (Token.GREATER_OR_EQUAL, None, !i-2)
    else
        i := !i + 1
        (Token.GREATER, None, !i-1)

let lexTokenLt () : TokenInfo =
    let str = !s
    if str.Chars (!i + 1) = '=' then
        i := !i + 2
        (Token.LESS_OR_EQUAL, None, !i-2)
    else
        i := !i + 1
        (Token.LESS, None, !i-1)

let lexTokenTilde () : TokenInfo =
    let str = !s
    if str.Chars (!i + 1) = '=' then
        i := !i + 2
        (Token.NOT_EQUAL, None, !i-2)
    else
        i := !i + 1
        (Token.BAD_TOKEN, Some("expected '=' after '~'"), !i-1)

/// Returns the next token and advances the cursor to the end
/// of the token's lexeme.
let rec lexToken () : Token*Option<string>*int =
    let next = (!s).Chars !i
    tokenStart := !i
    match next with
    | 'a' ->
        lexTokenA()
    | 'b' ->
        lexTokenB()
    | 'd' ->
        lexTokenD()
    | 'e' ->
        lexTokenE()
    | 'f' ->
        lexTokenF()
    | 'i' ->
        lexTokenI()
    | 'l' ->
        lexTokenL()
    | 'n' ->
        lexTokenN()
    | 'o' ->
        lexTokenO()
    | 'r' ->
        lexTokenR()
    | 't' ->
        lexTokenT()
    | 'u' ->
        lexTokenU()
    | 'w' ->
        lexTokenW()
    | '.' ->
        lexTokenDot()
    | '=' ->
        lexTokenEq()
    | '>' ->
        lexTokenGt()
    | '<' ->
        lexTokenLt()
    | '~' ->
        lexTokenTilde()
    | '#' ->
        let ret = (Token.LENGTH,None,!i)
        i := !i + 1
        ret
    | '\"' ->
        lexStringDouble()
    | '\'' ->
        lexStringSingle()
    | '+' ->
        let ret = (Token.PLUS,None,!i)
        i := !i + 1
        ret
    | '-' ->
        let ret = (Token.MINUS,None,!i)
        i := !i + 1
        ret
    | '*' ->
        let ret = (Token.MULT,None,!i)
        i := !i + 1
        ret
    | '/' ->
        let ret = (Token.DIV,None,!i)
        i := !i + 1
        ret
    | '^' ->
        let ret = (Token.POW,None,!i)
        i := !i + 1
        ret
    | '%' ->
        let ret = (Token.MOD,None,!i)
        i := !i + 1
        ret
    | ',' ->
        let ret = (Token.COMMA,None,!i)
        i := !i + 1
        ret
    | '(' ->
        let ret = (Token.OPEN_PAREN,None,!i)
        i := !i + 1
        ret
    | ')' ->
        let ret = (Token.CLOSE_PAREN,None,!i)
        i := !i + 1
        ret
    | '{' ->
        let ret = (Token.OPEN_BRACKET,None,!i)
        i := !i + 1
        ret
    | '}' ->
        let ret = (Token.CLOSE_BRACKET,None,!i)
        i := !i + 1
        ret
    | '[' ->
        if ((!s).Chars (!i+1)) = '=' || ((!s).Chars (!i+1)) = '[' then
            lexLongString()
        else
            let ret = (Token.OPEN_SQUARE_BRACKET,None,!i)
            i := !i + 1
            ret
    | ']' ->
        let ret = (Token.CLOSE_SQUARE_BRACKET,None,!i)
        i := !i + 1
        ret
    | ':' ->
        let ret = (Token.METHOD_IND,None,!i)
        i := !i + 1
        ret
    | ';' ->
        let ret = (Token.SEMI_COLON,None,!i)
        i := !i + 1
        ret
    | '!' ->
        let ret = (Token.EOF,None,!i)
        i := !i + 1
        ret
    | '_' ->
        lexName()
    | a when (isAlpha a) ->
        lexName()
    | d when (d = '0') && ( ((!s).Chars (!i+1)) = 'x' || ((!s).Chars (!i+1)) = 'X' ) ->
        lexHexNumber()
    | d when (isDigit d) ->
        lexNumber()
    | _ ->
        let ret = (Token.BAD_TOKEN,None,!i)
        i := !i + 1
        ret

let eatWhitespace () =
    let str = !s

    processAnnotation()
    annotation := None

    let start = !i

    while !i < (str.Length-1) && (!s).Chars !i = '-' && (!s).Chars (!i + 1) = '-' do
        lexComment()

    while !i < (!s).Length && isSpace ((!s).Chars !i) do
        if (!s).Chars !i = '\n' then
            let prevBoundary = lineBoundaries.[lineBoundaries.Count-1]
            let lineContents = str.Substring(prevBoundary+1,!i-prevBoundary)
            if lineContents.Trim() = "" then
                // if an annotation begins on the first line of the file and does not decorate a syntax item, it is a header
                if (!annotation).IsSome then 
                    let contents,startPosition,commentType = (!annotation).Value
                    let parsedAnnotation = Annotations.parseAnnotation contents startPosition commentType !currentFileName
                    let startsOnFstLine = lineBoundaries.Count = 1 || (snd3 (!annotation).Value) < lineBoundaries.[1]
                    match parsedAnnotation with
                    | (desc,EmptyTagSet,errors,range) when startsOnFstLine ->
                        headerAnnotation := Some(desc,ModuleHeaderTagSet([],[]),errors,range)
                    | _ ->
                        ()

                processAnnotation()
                annotation := None

            lineBoundaries.Add(!i)
            line := !line + 1
            lineStart := !i + 1

        i := !i + 1

        while !i < (str.Length-1) && (!s).Chars !i = '-' && (!s).Chars (!i + 1) = '-' do
            lexComment()

let ret3 () = 3

/// Returns an array of (id, str, pos) triples, where
/// id is a token identifier, str is an optional string for comment annotations
/// and semantic information, and pos is the start position of the token.
/// also returns an optional header annotation (if one is contained in the file), 
/// a map from annotations to token indices, and an array of line boundaries 
/// where each element of the array is the character index directly preceding 
/// the first character of a new line
let lex (target : string) (fileName : string) : GenList<TokenInfo>*Option<Annotation>*Map<int,Annotation>*GenList<int> =
    ret.Clear()
    i := 0
    currentFileName := fileName
    annotation := None
    annotationMap := Map.empty
    headerAnnotation := None
    tokenStart := 0
    //'0' is not contained in any token... we add this to avoid running of the end of the string
    s := target+"\0" 
    line := 0
    lineBoundaries.Clear()
    lineBoundaries.Add(-1)
    lineStart := 0
    eatWhitespace()
    
    while !i < target.Length do
        ret.Add(lexToken())
        eatWhitespace()
    
    lineBoundaries.Add(target.Length)

    ret.Add(Token.EOF,None,target.Length)
    ret.Add(Token.EOF,None,target.Length)
    ret.Add(Token.EOF,None,target.Length)
    ret.Add(Token.EOF,None,target.Length)
    ret.Add(Token.EOF,None,target.Length)
    ret.Add(Token.EOF,None,target.Length)
    ret.Add(Token.EOF,None,target.Length)
    ret.Add(Token.EOF,None,target.Length)
    ret.Add(Token.EOF,None,target.Length)
    ret.Add(Token.EOF,None,target.Length)
    ret.Add(Token.EOF,None,target.Length)
    (ret,!headerAnnotation,!annotationMap,lineBoundaries)