﻿module LuaAnalyzer.Analyzer

open Utils
open Type
open TypedSyntax
open TypeChecker
open Syntax
open TypeCollector
open ProjectChecker
open Plugins
open ErrorList

open System.ComponentModel.Composition
open System.ComponentModel.Composition.Hosting

/// The standard .NET generic list class. (A resizable array.)
type GenList<'T> = System.Collections.Generic.List<'T>

let getErrorFromPos (pos : int) (stat : Statement) =
    match getStatError pos stat with
    | Some x ->
        x
    | None ->
        null

let hasErrors (stat : Statement) (annotationErrorsIncluded : bool) =
    (listStatErrors annotationErrorsIncluded stat).Length > 0

/// C# compatible forms for types
type CSRecordType = {
    name : string
    desc : string
    fields : GenList<string*string*string>
    methods : GenList<string*string*string>
}

type CSFunctionType = {
    desc : string
    formals : GenList<string*string*string>
    hasVarPars : bool
    varParsDesc : string
    rets : GenList<string*string>
    hasVarRets : bool
    varRetsDesc : string
    isLatent : bool
    isMethod : bool
    defLocation : DefinitionLocation
    /// the range of the call being queried
    callRange : Range
}

let makeCSFunction (fieldDesc : string) (rng : Range) (ty : Type) =
    match ty with
    | CallableTy(tyDesc,formals,varPars,rets,varRets,_,latent,defLocation) ->
        let formalsList = new GenList<string*string*string>()
        for (name,desc,ty) in formals do
            formalsList.Add(name,desc,ty.ToString())

        let retList = new GenList<string*string>()
        for (desc,ty) in rets do
            retList.Add(desc,ty.ToString())
        
        {
            desc = if not (tyDesc = "") then tyDesc else fieldDesc
            formals = formalsList
            hasVarPars = varPars.IsSome
            varParsDesc = if varPars.IsSome then varPars.Value else ""
            rets = retList
            hasVarRets = varRets.IsSome
            varRetsDesc = if varRets.IsSome then varRets.Value else ""
            isLatent = latent
            isMethod = false
            defLocation = defLocation
            callRange = rng
        }
    | _ ->
        failwith "API definition error: overloads should only contain callable types"

let makeCSRecord (ty : Type) : CSRecordType =
    match ty with
    | RecordTy(name,desc,_,_,_,metaset,fields,_,methods,_,_) when fields.IsEmpty && methods.IsEmpty && metaset.Index.IsSome ->
        match metaset.Index with
        | Some(_,NumberTy,elemTy,_) ->
            {
                name = "{" + elemTy.ToString() + "}"
                desc = "An array of " + elemTy.ToString() + "s" + if not (desc = "") then "\n" + desc else ""
                fields = new GenList<string*string*string>()
                methods = new GenList<string*string*string>()
            }
        | Some(_,keyTy,valTy,_) ->
            {
                name = "{" + keyTy.ToString() + " => " + valTy.ToString() + "}"
                desc = "An map with keys of type " + keyTy.ToString() + " and values of type " + valTy.ToString() +
                       if not (desc = "") then "\n" + desc else ""
                fields = new GenList<string*string*string>()
                methods = new GenList<string*string*string>()
            }
        | _ ->
            failwith "unreachable"
    | RecordTy(name,desc,_,_,_,_,fields,fieldsList,methods,_,_) ->
        let newFieldList = new GenList<string*string*string>()
        if List.isEmpty fieldsList then
            for kv in fields do
                let key, value = kv.Key, kv.Value
                let name,desc,ty = key, value.desc, value.ty.ToString()
                newFieldList.Add(name,desc,ty)
        else
            for key,value in fieldsList do
                let name,desc,ty = key, value.desc, value.ty.ToString()
                newFieldList.Add(name,desc,ty)

        let methodList = new GenList<string*string*string>()
        for kv in methods do
            let name,desc,ty = kv.Key, kv.Value.desc, kv.Value.ty.ToString()
            methodList.Add(name,desc,ty)

        {
            name = name
            desc = desc
            fields = newFieldList
            methods = methodList
        }
    | _ ->
        failwith "API definition error: overloads should only contain record types"


let getFieldOrMethodInfo (modules : seq<string*string*Lazy<string>*int64>) (lexp : Expr) (fieldOrMethodName : string) (targetFileName : string) =
    let stubCtxt = getStubContextInProject modules targetFileName
    let typed = typeExpr lexp
    let ctxt = stubCtxt.DontTrackErrors.IsNotLeftExpr
    match Type.Coerce ctxt.tenv (fst3 (typeCheckExprCompute ctxt typed)) with
    | RecordTy(name,desc,_,_,_,_,fields,_,methods,_,loc) ->
        let isMethod, (fieldDesc, fieldTy, loc) = 
            if fields.ContainsKey fieldOrMethodName then
                let field = fields.Item fieldOrMethodName
                false, (field.desc,field.ty,field.loc)
            else if methods.ContainsKey fieldOrMethodName then
                let {desc=desc;ty=ty;isAbstract=_;loc=loc} = methods.Item fieldOrMethodName
                true, (desc,ty,loc)
            else
                false, ("", UnknownTy, NoLocation)

        match fieldTy with
        | FunctionTy(_,_,_,_,_,_,_,_) ->
            new GenList<CSFunctionType>([makeCSFunction fieldDesc (-1,-1) fieldTy])
        | OverloadTy(overloads) ->
            new GenList<CSFunctionType>(List.map (makeCSFunction fieldDesc (-1,-1)) overloads)
        | _ ->
            new GenList<CSFunctionType>([])
    | _ ->
        new GenList<CSFunctionType>([])

let getCallInfo (modules : seq<string*string*Lazy<string>*int64>) (lexp : Expr) (targetFileName : string) : GenList<CSFunctionType> =
    let ret = new GenList<CSFunctionType>()
    let stubCtxt = getStubContextInProject modules targetFileName
    let typed = typeExpr lexp
    let exprTy,_,_ = typeCheckExprCompute stubCtxt.DontTrackErrors.IsNotLeftExpr typed 
    match Type.Coerce stubCtxt.tenv exprTy with
    | CallableTy(desc,formals,varPars,rets,varRets,varargs,latent,defLocation) as ty ->
        new GenList<CSFunctionType>([makeCSFunction "" (-1,-1) ty])
    | OverloadTy(overloads) ->
        new GenList<CSFunctionType>(List.map (makeCSFunction "" (-1,-1)) overloads)        
    | _ ->
        new GenList<CSFunctionType>()

let getRecordInfo (modules : seq<string*string*Lazy<string>*int64>) (lexp : Expr) (targetFileName : string) : System.Object =
    let stubCtxt = getStubContextInProject modules targetFileName
    let typed = typeExpr lexp
    let stubCtxt = stubCtxt.DontTrackErrors.IsNotLeftExpr
    let exprTy,_,_ = typeCheckExprCompute stubCtxt typed
    match Type.Coerce stubCtxt.tenv exprTy with
    | RecordTy(name,desc,_,_,_,_,fields,fieldsList,methods,_,_) ->
        // fieldList/methodList entries represent (name/type/description)
        let newFieldList = new GenList<string*string*string>()
        if fieldsList.IsEmpty then
            for kv in fields do
                let name,field = kv.Key, kv.Value
                if field.isVisible then
                    newFieldList.Add(name,field.ty.ToString(),field.desc)
        else
            for name, field in fieldsList do
                if field.isVisible then
                    newFieldList.Add(name,field.ty.ToString(),field.desc)
        
        let methodList = new GenList<string*string*string>()
        for kv in methods do
            let name = kv.Key
            let {desc=desc;ty=ty;isAbstract=_;loc=_} = kv.Value
            methodList.Add(name,ty.ToString(),desc)            
        
        {
            name = name
            desc = desc
            fields = newFieldList
            methods = methodList    
        } :> System.Object
    | _ ->
        null

/// Copies all library modules from the UserData directory to the project directory
/// Returns a list of project-relative file names of all library modules
let syncLibModules (projectDir : string) =
    let libModules = Set.ofSeq (Seq.collect (fun (x : ITypeCollectorPlugin) -> x.GetLibPaths()) !plugins)
    
    for modFile : string in libModules do
        // make sure the directory containing the file (in the project) exists 
        let dirChain = modFile.Split('\\')
        let dir = ref ""
        for i in 0 .. dirChain.Length-2 do
            dir := !dir + "\\" + dirChain.[i]

            if not (System.IO.Directory.Exists(projectDir + !dir)) then
                ignore (System.IO.Directory.CreateDirectory(projectDir + !dir))

        let writeTime = System.IO.File.GetLastWriteTime
        let exists = System.IO.File.Exists
        let projFile = projectDir + "\\" + modFile
        let pluginsFile = pluginsDir + "\\" + modFile

        // copy from plugin directory to project directory
        if (not (exists(projectDir + "\\" + modFile))) ||
           (not (writeTime(projectDir + "\\" + modFile) = writeTime(pluginsDir + "\\" + modFile))) then
            System.IO.File.Copy(pluginsDir + "\\" + modFile, projectDir + "\\" + modFile,true)
        
    libModules

let getProjectErrors (modules : seq<string*string*Lazy<string>*int64>) 
                     (projectDir : string) 
                     (typeCheck : bool) 
                     : GenList<string*string*Range>*bool =

    LuaAnalyzer.ProjectChecker.typeCheckProject modules typeCheck None

let getFileErrors (modules : seq<string*string*Lazy<string>*int64>) 
                  (projectDir : string) 
                  (typeCheck : bool) 
                  (targetFile : string) 
                  : GenList<string*string*Range>*bool =

    LuaAnalyzer.ProjectChecker.typeCheckProject modules typeCheck (Some(targetFile))
    
let getFileCall (modules : seq<string*string*Lazy<string>*int64>) 
                (targetFile : string) 
                : GenList<CSFunctionType>*Option<CSRecordType>*bool =

        let stubCtxt = getStubContextInProject modules targetFile
        let functionTys =
            match stubCtxt.closestEnclosingFunctionCall with
            | Some(optField,rng,ty) ->
                let fieldDesc =
                    match optField with
                    | Some(field) ->
                        field.desc
                    | None ->
                        ""
                match Type.Coerce stubCtxt.tenv ty with
                | CallableTy(desc,formals,varPars,rets,varRets,varargs,latent,defLocation) as ty ->
                    new GenList<CSFunctionType>([makeCSFunction fieldDesc rng ty])
                | OverloadTy(overloads) ->
                    new GenList<CSFunctionType>(List.map (makeCSFunction fieldDesc rng) overloads)        
                | _ ->
                    new GenList<CSFunctionType>()
            | None ->
                new GenList<CSFunctionType>([])
        let recordTy = Option.map makeCSRecord stubCtxt.closestEnclosingExpectedRecord
        let hasRecord = stubCtxt.closestEnclosingExpectedRecord.IsSome
        functionTys,recordTy,hasRecord       

let getModuleDescription (targetModuleContents : string) =
    try 
        let _,header,_,_ = HandLexer.lex targetModuleContents ""
    
        match header with
        | Some(desc,Annotations.ModuleHeaderTagSet(_),_,_) ->
            desc
        | None ->
            "No description provided."
        | _ ->
            failwith "unreachable"
    with
    | HandLexer.LexError(msg,pos,ln,boundaries) ->
        "Lexical error in file"

/// Gets the line at which calls to require--i.e. lines of the form:
/// 'local modname = require("modname"); are inserted into.
/// This should be near the top of the file, but below the header.
let getRequireInsertPos (targetModuleContents : string) =
    let _,header,_,lineBoundaries = HandLexer.lex targetModuleContents ""
    match header with
    | Some(_,_,_,(startInd,endInd)) ->
        let boundaryInd = Seq.findIndex (fun x -> x >= endInd) lineBoundaries
        lineBoundaries.[min (boundaryInd+1) (lineBoundaries.Count-1)]+1
    | _ ->
        lineBoundaries.[min 2 (lineBoundaries.Count-1)]+1

type Composite () =
         
    [<ImportMany>]
    [<DefaultValue>]
    val mutable public collectors : seq<ITypeCollectorPlugin>

    member self.init() =
        let catalog = new AggregateCatalog()
        
        //GameKitchen's subdirectory under appdata 
        catalog.Catalogs.Add(new DirectoryCatalog(pluginsDir))

        let container = new CompositionContainer(catalog)
        container.ComposeParts(self)        

/// Initialize all type collector plugins
let init () =
    let comp = new Composite()
    comp.init()
        
    let pluginNames = ref Set.empty

    for collector in comp.collectors do
        collector.Init()
        if not ( (!pluginNames).Contains (collector.GetName()) ) then
            pluginNames := (!pluginNames).Add( collector.GetName() )
            plugins := collector :: !plugins

