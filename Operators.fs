﻿module LuaAnalyzer.Operators

type BinOp =
    | OpAdd
    | OpSub
    | OpMul
    | OpDiv
    | OpMod
    | OpPow
    | OpConcat
    | OpNe
    | OpEq
    | OpLt
    | OpLe
    | OpGt
    | OpGe
    | OpAnd
    | OpOr
    | OpInd
    | OpSelect
    | OpMethInd

    override this.ToString() =
        match this with
        | OpAdd ->
            "+"
        | OpSub ->
            "-"
        | OpMul ->
            "*"
        | OpDiv ->
            "/"
        | OpMod ->
            "%"
        | OpPow ->
            "^"
        | OpConcat ->
            ".."
        | OpNe ->
            "~="
        | OpEq ->
            "=="
        | OpLt ->
            "<"
        | OpLe ->
            "<="
        | OpGt ->
            ">"
        | OpGe ->
            ">="
        | OpAnd ->
            "and"
        | OpOr ->
            "or"
        | OpSelect ->
            "."
        | OpInd ->
            // we can't use a char here, because it corresponds to '.' and '[ ]'
            "[ ]" 
        | OpMethInd ->
            ":"

and UnOp =
    | OpNegate
    | OpNot
    | OpLen

    override this.ToString() =
        match this with
        | OpNegate ->
            "(unary -)"
        | OpNot ->
            "not"
        | OpLen ->
            "#"
