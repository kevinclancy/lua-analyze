﻿module LuaAnalyzer.HandParser

open Token
open Syntax
open Annotations
open Operators

type GenList<'T> = System.Collections.Generic.List<'T>
type GenLinkedList<'T> = System.Collections.Generic.LinkedList<'T>
type IList<'T> = System.Collections.Generic.IList<'T>
type IEnumerable<'T> = System.Collections.Generic.IEnumerable<'T>

exception ParseException of string*Range
exception RecoveryException of string*Range

type ITokenIterator =
    /// moves the iterator forward one token
    abstract member Advance : unit -> unit
    /// moves the iterator back one token
    abstract member Retract : unit -> unit
    /// true iff the token iterator is at the end of the stream
    abstract member AtEnd : unit -> bool
    /// the current token
    abstract member Current : unit -> TokenInfo
    abstract member Prev : unit -> TokenInfo
    /// the position of the token in the stream
    abstract member Position : unit -> int
    /// PeekAhead(n) retrieves the token n positions ahead of the current one
    abstract member PeekAhead : int -> TokenInfo

type LinkedListTokenIterator (target : GenLinkedList<TokenInfo>) =
    let node = ref target.First
    let ind = ref 0

    interface ITokenIterator with
        member self.Advance () =
            ind := !ind + 1
            node := (!node).Next

        member self.Retract () =
            ind := !ind - 1
            node := (!node).Previous

        member self.AtEnd () =
            !ind >= target.Count

        member self.Current () =
            (!node).Value

        member self.Prev () =
            (!node).Previous.Value

        member self.Position () =
            !ind

        member self.PeekAhead (n : int) =
            let peekNode = ref (!node)
            for i in 1..n do
                peekNode := (!peekNode).Next

            (!peekNode).Value

type ListTokenIterator (target : GenList<TokenInfo>) =
    let ind = ref 0

    interface ITokenIterator with
        member self.Advance () =
            ind := !ind + 1

        member self.Retract () =
            ind := !ind - 1
             
        member self.AtEnd () =
            !ind >= target.Count

        member self.Current () =
            target.[!ind]

        member self.Prev () =
            target.[!ind-1]

        member self.Position () =
            !ind

        member self.PeekAhead (n : int) =
            target.[!ind+n]

//type LinkedListTokenIterator (target : LinkedList<TokenInfo>) =
//    interface ITokenIterator with
//        ()

type TokenProcessor (iterator : ITokenIterator, annotationMap : Map<int,Annotation>, lastPos : int) =

    member self.Skip() =
        iterator.Advance()

    member self.Consume(tok : Token) =
        if not (iterator.AtEnd()) then
            match iterator.Current() with
                | (targetTok,_,_) when targetTok = tok ->
                    iterator.Advance()
                | (targetTok,_,_) ->
                    raise(ParseException("expected token "+tok.ToString()+". instead got "+targetTok.ToString(), getTokenRange (iterator.Current()) ))
        else
            if not (tok = Token.EOF) then
                raise(ParseException("expected token "+Token.EOF.ToString()+". instead got " + (tok.ToString()), getTokenRange (iterator.Current()) ))
            else
                ()

    member self.ConsumeOption(toks : Token list) =
        match iterator.Current() with
        | (targetTok,_,_) ->
            match List.tryFind (fun x -> x = targetTok) toks with
            | Some _ ->
                iterator.Advance()
            | _ ->
                raise(ParseException("tried to find one of ... instead got" + targetTok.ToString(), getTokenRange (iterator.Current()) ))

    member self.Recover(sync : List<Token>, matchTok : Token, openers : List<Token>, openerRange : int*int) =
        let balance = ref 1
        let i = ref 0
        iterator.Retract()
        while not (!balance = 0) do
            let curr = self.PeekAhead(!i)

            if  Option.isSome (List.tryFind (fun x -> x = self.PeekAhead(!i)) sync) then
                balance := !balance - 1
            else
                if self.PeekAhead(!i) = Token.EOF then
                    iterator.Advance()
                    raise(ParseException("no matching " + sync.ToString() + " for " + matchTok.ToString(), openerRange))
                elif Option.isSome (List.tryFind (fun x -> x = self.PeekAhead(!i)) openers) then
                    balance := !balance + 1

                i := !i + 1

        iterator.Advance()
    

    member self.PeekAnnotation() =
        annotationMap.TryFind (iterator.Position())

    // This is not a guaranteed recovery, but it is an attempt.
    // May end up skipping over multiple statement
    member self.RecoverStat() =
        let balance = ref 0
        let finished = ref false
        let enders = [
            Token.CLOSE_PAREN
            Token.CLOSE_BRACKET
            Token.CLOSE_SQUARE_BRACKET
            Token.NUMBER
            Token.STRING
            Token.NIL
            Token.VARARGS
            Token.NAME
            Token.END
            Token.UNTIL
        ]

        let starters = [
            Token.LOCAL
            Token.NAME
            Token.IF
            Token.WHILE
            Token.DO
            Token.FUNCTION
            Token.REPEAT
            Token.FOR
        ]

        let isEnder (tok : Token) =
            Option.isSome (List.tryFind (fun x -> x = tok) enders)

        let isStarter (tok : Token) =
            Option.isSome (List.tryFind (fun x -> x = tok) starters)

        while not !finished do
            if self.Peek() = Token.EOF then
                finished := true
            else
                let grounded = !balance = 0
                let prevEnder = iterator.Position() > 0 && (isEnder(self.PeekPrev()))
                let currStarter = isStarter (self.Peek())
                if grounded && prevEnder && currStarter then
                    finished := true
                elif grounded && (self.Peek() = Token.END || self.Peek() = Token.EOF) then
                    finished := true
                else
                    if self.Peek() = Token.FUNCTION || self.Peek() = Token.IF || self.Peek() = Token.DO then
                        balance := !balance + 1
                    elif self.Peek() = Token.END then
                        balance := !balance - 1

                    iterator.Advance()

    member self.RecoverArg(openerRng : Range) =
        let balance = ref 1
        let i = ref 0
        while not (!balance = 1 && (self.PeekAhead(!i) = Token.CLOSE_PAREN || self.PeekAhead(!i) = Token.COMMA)) do
            if  self.PeekAhead(!i) = Token.CLOSE_PAREN then
                balance := !balance - 1
            else
                if self.PeekAhead(!i) = Token.EOF then
                    raise(ParseException("no matching " + Token.CLOSE_PAREN.ToString() + " for " + Token.OPEN_PAREN.ToString(), openerRng))
                elif self.PeekAhead(!i) = Token.OPEN_PAREN then
                    balance := !balance + 1
            i := !i + 1
                    
        iterator.Advance()

    member self.RecoverWhile(whileTokenRange : int*int) = 
        self.Recover(
            [Token.DO], 
            Token.WHILE, 
            [Token.FOR;Token.WHILE], 
            whileTokenRange
        )
                
    member self.RecoverDo(doTokenRange : int*int) =
        self.Recover(
            [Token.END], 
            Token.DO, 
            [Token.IF;Token.FUNCTION;Token.DO], 
            doTokenRange
        )

    member self.RecoverIf(ifTokenRange : int*int) = 
        self.Recover(
            [Token.THEN], 
            Token.IF, 
            [Token.IF;Token.ELSEIF], 
            ifTokenRange
        )

    member self.RecoverElseIf(elseifTokenRange : int*int) = 
        self.Recover(
            [Token.THEN], 
            Token.ELSEIF, 
            [Token.IF;Token.ELSEIF], 
            elseifTokenRange
        )

    member self.RecoverThen(thenTokenRng : int*int) =
        let sync = [Token.ELSE; Token.ELSEIF; Token.END]
        let openers = [Token.IF;Token.FUNCTION;Token.DO]
        let closers = [Token.END]
        let balance = ref 1
        let finished = ref false
        while not (!finished) do
            if  Option.isSome (List.tryFind (fun x -> x = self.Peek()) sync) && !balance = 1 then
                finished := true
            elif Option.isSome (List.tryFind (fun x -> x = self.Peek()) closers) then
                balance := !balance - 1
                iterator.Advance()
            else
                if self.Peek() = Token.EOF then
                    raise(RecoveryException("no matching " + sync.ToString() + " for " + Token.THEN.ToString(), thenTokenRng))
                elif Option.isSome (List.tryFind (fun x -> x = self.Peek()) openers) then
                    balance := !balance + 1

                iterator.Advance()
    
    member self.RecoverElse(elseTokenRange : int*int) =
        self.Recover(
            [Token.END],
            Token.ELSE,
            [Token.IF;Token.FUNCTION;Token.DO],
            elseTokenRange
        )

    member self.RecoverFunction(functionTokenRange : int*int) = 
        self.Recover(
            [Token.END], 
            Token.FUNCTION, 
            [Token.IF;Token.FUNCTION;Token.DO], 
            functionTokenRange
        )

    member self.CurrentPos() =
        if not( iterator.AtEnd() ) then
            match iterator.Current() with
            | (_, _, pos) ->
                pos      
        else
            lastPos  

    member self.PrevEndPos() =
        match (getTokenRange (iterator.Prev())) with
        | (_,fin) ->
            fin

    member self.TryConsume(tok : Token) : bool =
        if not ( iterator.AtEnd() ) then
            match iterator.Current() with
            | (targetTok,_,_) when targetTok = tok ->
                iterator.Advance()
                true
            | _ ->
                false
        else
            false

    member self.PeekAhead(n : int) =
        match iterator.PeekAhead(n) with
        | (tok, _, _) ->
            tok

    member self.Peek() : Token =
        fst3 (iterator.Current())
    
    member self.NextIsFrom(lst : List<Token>) =
        if Option.isSome (List.tryFind (fun x -> (self.Peek() = x)) lst) then
            ()
        else
            raise(ParseException("Tried to find one of " + lst.ToString() + ". Instead got" + self.Peek().ToString(), self.PeekRange()))

    member self.PeekRange() : Range =
        
        if not (iterator.AtEnd()) then
            getTokenRange (iterator.Current())
        else
            (lastPos,lastPos)

    member self.PeekNext() : Token =
        self.PeekAhead(1)
    
    member self.PeekPrev() : Token =
        match iterator.Prev() with
        | (tok,_,_) ->
            tok

    member self.PeekVal(tok : Token) : string*int =
        if not (iterator.AtEnd()) then
            match iterator.Current() with
            | (found, Some value, pos) when tok = found ->
                (value, pos)
            | (found, None, pos) when tok = found ->
                ("", pos)
            | (found,_,fndPos) ->
                raise(ParseException("expected " + tok.ToString() + ". got " + found.ToString(), getTokenRange (iterator.Current()) ))
            | _ ->
                failwith "wtf"
        else
            ("",0)

let ref_chunk = ref<TokenProcessor -> bool -> Statement> (fun it inLoop -> Break(0,0))
let chunk it = !ref_chunk it

let ref_expr = ref<TokenProcessor -> bool -> Expr> (fun it inLoop -> Nil(0,0))
let expr it = !ref_expr it

let private indexName (it : TokenProcessor) =
    let name,start = it.PeekVal(Token.NAME)
    let fin = start + name.Length
    it.Consume(Token.NAME)
    String(name, (start,fin))

let private singleVar (it : TokenProcessor) =
    let name,start = it.PeekVal(Token.NAME)
    let fin = start + name.Length
    it.Consume(Token.NAME)
    NameExpr(name, (start,fin))

let private expList1 (it : TokenProcessor) =
    let ret = ref [expr it false]
    while it.TryConsume(Token.COMMA) do
        ret := (expr it false) :: !ret
    (List.rev !ret)

let private fstArgExp (it : TokenProcessor) =
    let startPos = it.CurrentPos()
    expr it false

let private argExp (it : TokenProcessor) =
    let startPos = it.CurrentPos()
    it.Consume(Token.COMMA)
    expr it false

let private argExpList1 (it : TokenProcessor) (openRng : Range) =
    let ret = 
        try
            ref [fstArgExp it]
        with
        | ParseException(msg, rng) ->
            it.RecoverArg(openRng)
            ref [ErrorExpr(msg,rng)]

    while not (it.Peek() = Token.CLOSE_PAREN) do
        let next = 
            try
                argExp it
            with
            | ParseException(msg, rng) ->
                it.RecoverArg(openRng)
                ErrorExpr(msg,rng)

        ret := next :: !ret
    (List.rev !ret)

let private nameList1 (it : TokenProcessor) =
    let ret = ref [singleVar it]
    while it.TryConsume(Token.COMMA) do
        ret := (singleVar it) :: !ret
    (List.rev !ret)

let private prefixExp (it : TokenProcessor) (inLoop : bool) =
    match it.Peek() with
    | Token.OPEN_PAREN ->
        it.Skip()
        let ret = expr it inLoop
        it.Consume(Token.CLOSE_PAREN)
        ret
    | Token.NAME -> 
        singleVar it
    | _ ->
        raise(ParseException("expressions cannot begin with this token", it.PeekRange() ))

let private yindex (it : TokenProcessor) : Expr =
    it.Consume(Token.OPEN_SQUARE_BRACKET)
    let ret = expr it false
    it.Consume(Token.CLOSE_SQUARE_BRACKET)
    ret

let private recField (it : TokenProcessor) : ConstructorField =
    let start = it.CurrentPos()
    let annotation = it.PeekAnnotation()

    let lexp =
        if it.Peek() = Token.NAME then
            let name,pos = it.PeekVal(Token.NAME)
            it.Skip()
            String(name, (pos,pos+name.Length))
        else
            yindex it

    it.Consume(Token.ASSIGN)

    let rexp = expr it false
    let fin = it.PrevEndPos()
    RecField(annotation,lexp,rexp,(start,fin))

let private listField (it : TokenProcessor) : ConstructorField =
    let start = it.CurrentPos()
    let annotation = it.PeekAnnotation()

    let e = expr it false
    let fin = it.PrevEndPos()
    ListField(annotation,e,(start,fin))

let private constructr (it : TokenProcessor) : Expr =
    let start = it.CurrentPos()
    it.Consume(Token.OPEN_BRACKET)
    let fields : Ref<List<ConstructorField>> = ref []
    let finished = ref false
    while not (it.Peek() = Token.CLOSE_BRACKET) do
    
        match it.Peek() with
        | Token.NAME ->
            match it.PeekNext() with
            | Token.ASSIGN ->
                fields := (recField it) :: !fields
            | _ ->
                fields := (listField it) :: !fields
        | Token.OPEN_SQUARE_BRACKET ->
            fields := (recField it) :: !fields
        | _ ->
            fields := (listField it) :: !fields

        if not (it.Peek() = Token.CLOSE_BRACKET) then
            it.ConsumeOption([Token.COMMA; Token.SEMI_COLON])

    it.Consume(Token.CLOSE_BRACKET)
    let fin = it.PrevEndPos()
    Constructor(List.rev !fields, (start,fin))

let private funcArgs (it : TokenProcessor) : List<Expr> =
    match it.Peek() with
    | Token.OPEN_PAREN ->
        let openRng = it.PeekRange()
        it.Skip()
        if it.Peek() = Token.CLOSE_PAREN then
            it.Consume(Token.CLOSE_PAREN)
            []
        else
            let ret = argExpList1 it openRng
            it.Consume(Token.CLOSE_PAREN)
            ret

    | Token.OPEN_BRACKET ->
        [constructr it]
    | Token.STRING ->
        let value,pos = it.PeekVal(Token.STRING)
        it.Skip()
        [String(value,(pos,pos+value.Length))]
    | _ ->
        raise(ParseException("Expected function arguments",(it.CurrentPos(),it.CurrentPos()+1)))

let private parlist (it : TokenProcessor) : List<Expr>*bool =
    let pars = ref [] 
    let hasVarArgs = ref false

    if not (it.Peek() = Token.CLOSE_PAREN) then
        let finished = ref false
        while not !finished do
            if it.TryConsume(Token.VARARGS) then
                hasVarArgs := true
            else
                pars := (singleVar it) :: !pars

            finished := !hasVarArgs || (not (it.TryConsume(Token.COMMA)))

    (List.rev !pars, !hasVarArgs)    


let private body (it : TokenProcessor) (functionTokenRange : int*int) =
    let startPos = it.PrevEndPos()

    it.Consume(Token.OPEN_PAREN)
    let pars,hasVarArgs = parlist it
    it.Consume(Token.CLOSE_PAREN)

    let body =
        try
            let ret = chunk it false
            it.Consume(Token.END)
            ret
        with
        | ParseException(msg,pos) ->
            it.RecoverFunction(functionTokenRange)
            it.Consume(Token.END)
            ErrorStatement(msg,pos)

    (pars,hasVarArgs,body)

let private primaryExp (it : TokenProcessor) (inLoop : bool) : Expr =
    let start = it.CurrentPos()
    let pre = prefixExp it inLoop

    let finished = ref false
    let ret = ref pre
    while not !finished do
        match it.Peek() with
        | Token.DOT ->
            it.Skip()
            let name = indexName it
            let fin = it.PrevEndPos()
            ret := BinOpExpr(OpSelect, !ret, name, (start,fin))
        | Token.METHOD_IND ->
            it.Skip()
            let methName = indexName it
            let args = funcArgs it
            let fin = it.PrevEndPos()
            let _,mfin = getExprRange(methName)
            ret := CallExpr(BinOpExpr(OpMethInd,!ret,methName,(start,mfin)), args, (start,fin))
        | Token.OPEN_SQUARE_BRACKET ->
            let y = yindex it
            let _,fin = getExprRange y
            ret := BinOpExpr(OpInd,!ret,y,(start,fin)) 
        | Token.OPEN_PAREN 
        | Token.STRING
        | Token.OPEN_BRACKET ->
            let args = funcArgs it
            let fin = it.PrevEndPos()
            ret := CallExpr(!ret, args, (start,fin))
        | _ ->
            finished := true

    !ret

let private simpleExp (it : TokenProcessor) (inLoop : bool) : Expr =
    match it.Peek() with
    | Token.NUMBER ->
        let str,pos = it.PeekVal(Token.NUMBER)
        it.Skip()
        Number(0.0,(pos,pos+str.Length))
    | Token.STRING ->
        let str,pos = it.PeekVal(Token.STRING)
        it.Skip()
        String(str,(pos,pos+str.Length))
    | Token.NIL ->
        let pos = it.CurrentPos()
        it.Skip()
        Nil(pos,pos+3)
    | Token.TRUE ->
        let pos = it.CurrentPos()
        it.Skip()
        True(pos,pos+4)        
    | Token.FALSE ->
        let pos = it.CurrentPos()
        it.Skip()
        False(pos,pos+5)
    | Token.VARARGS ->
        let pos = it.CurrentPos()
        it.Skip()
        VarArgs(pos,pos+3)
    | Token.OPEN_BRACKET ->
        constructr it
    | Token.FUNCTION ->
        let start = it.CurrentPos()
        let rng = it.PeekRange()
        it.Skip()
        let exprs,hasVarArgs,stmt = body it rng
        let fin = it.PrevEndPos()
        Function(None, exprs,hasVarArgs,stmt,None,(start,fin))
    | _ ->
        primaryExp it inLoop

let private getUnOpr (tok : Token) : Option<UnOp> =
    match tok with
    | Token.MINUS ->
        Some OpNegate
    | Token.NOT ->
        Some OpNot
    | Token.LENGTH ->
        Some OpLen
    | _ ->
        None

let private getBinOpr (tok : Token) : Option<BinOp> =
    match tok with
    | Token.PLUS ->
        Some OpAdd
    | Token.MINUS ->
        Some OpSub
    | Token.MULT ->
        Some OpMul
    | Token.DIV ->
        Some OpDiv
    | Token.MOD ->
        Some OpMod
    | Token.POW ->
        Some OpPow
    | Token.CONCAT ->
        Some OpConcat
    | Token.NOT_EQUAL ->
        Some OpNe
    | Token.EQUAL ->
        Some OpEq
    | Token.LESS ->
        Some OpLt
    | Token.LESS_OR_EQUAL ->
        Some OpLe
    | Token.GREATER ->
        Some OpGt
    | Token.GREATER_OR_EQUAL ->
        Some OpGe
    | Token.AND ->
        Some OpAnd
    | Token.OR ->
        Some OpOr
    | _ ->
        None

/// Returns a pair such that the first element is the left priority of
/// the operator and the second element is the right priority
let private getPriority (op : BinOp) : int*int =
    match op with
    | OpAdd | OpSub ->
        (6, 6)
    | OpDiv | OpMod | OpMul ->
        (7, 7)
    | OpPow ->
        (10,9)
    | OpConcat ->
        (5,4)
    | OpGt | OpGe | OpLt | OpLe | OpEq | OpNe ->
        (3,3)
    | OpAnd ->
        (2,2)
    | OpOr ->
        (1,1)
    | OpInd | OpSelect | OpMethInd ->
        (11,11)


let private unaryPriority = 8

let private getLP (op : BinOp) =
    match getPriority op with
    | (l,_) -> 
        l

let private getRP (op : BinOp) =
    match getPriority op with
    | (_,r) -> r

let rec private subExp (it : TokenProcessor) (limit : int) (inLoop : bool) =
    let start = it.CurrentPos()
    let uop = getUnOpr(it.Peek())

    let ret =
        if Option.isSome uop then
            it.Skip()
            let sub = subExp it unaryPriority inLoop
            let fin = it.PrevEndPos()
            ref( UnOpExpr(Option.get uop,sub,(start,fin)) )
        else
            ref (simpleExp it inLoop)

    let op = ref( getBinOpr(it.Peek()) )
    while Option.isSome !op && (getLP (Option.get !op)) > limit do
        let opVal = Option.get !op
        it.Skip()
        let nextOp = subExp it (getRP opVal) inLoop
        let fin = it.PrevEndPos()
        ret := BinOpExpr(opVal, !ret, nextOp, (start,fin))
        op := getBinOpr(it.Peek())

    !ret


let private blockFollow (tok : Token) : bool =
    match tok with
    | Token.ELSE
    | Token.ELSEIF
    | Token.END
    | Token.UNTIL
    | Token.EOF ->
        true
    | _ ->
        false

let private ifStatement (it : TokenProcessor) (inLoop : bool) =
    let testThenBlock () =
        //skip over IF or ELSEIF
        let opener = it.Peek()
        let openerRng = it.PeekRange()
        
        it.Skip()

        let cond, openerRange =
            try 
                let ret = expr it inLoop
                let openerRange = it.PeekRange()
                it.Consume(Token.THEN)
                ret, openerRange
            with
            | ParseException(msg,rng) ->
                if opener = Token.IF then
                    it.RecoverIf(openerRng)
                else
                    it.RecoverElseIf(openerRng)
                
                let openerRange = it.PeekRange()
                it.Consume(Token.THEN)

                ErrorExpr(msg,rng), openerRange
                
        let stat =
            try
                let ret = chunk it inLoop
                it.NextIsFrom([Token.ELSEIF;Token.ELSE;Token.END])
                ret
            with
            | ParseException(msg,rng) ->
                it.RecoverThen(openerRange)
                ErrorStatement(msg,rng)

        (cond, stat)

    let ifThens = ref [testThenBlock()]

    while it.Peek() = Token.ELSEIF do
        ifThens := testThenBlock() :: !ifThens 

    let elseExplicit = ref false

    if it.Peek() = Token.ELSE then
        it.Skip()
        ifThens := (True(0,0), chunk it inLoop) :: !ifThens
        elseExplicit := true
    else
        ifThens := (True(0,0), Sequence(None,new GenList<Statement>([]), (0,0))) :: !ifThens
        elseExplicit := false

    it.Consume(Token.END)

    If(List.rev !ifThens, !elseExplicit, (0,0))

let private whileStatement (it : TokenProcessor) =
    let start = it.CurrentPos()
    let startTokenRng = it.PeekRange()

    it.Skip();

    let cond, doRng = 
        try 
            let ret = expr it false
            let doRng = it.PeekRange()
            it.Consume(Token.DO)
            ret, doRng
        with
        | ParseException(msg,rng) ->
            it.RecoverWhile(startTokenRng)
            let doRng = it.PeekRange()
            it.Consume(Token.DO)
            ErrorExpr(msg,rng), doRng 
   
    let stat =
        try
            let ret = chunk it true
            it.Consume(Token.END)
            ret
        with
        | ParseException(msg,rng) ->
            it.RecoverDo(doRng)
            it.Consume(Token.END)
            ErrorStatement(msg,rng)

    let fin = it.PrevEndPos()

    While(cond,stat,(start,fin))

let private forBody (it : TokenProcessor) =
     it.Consume(Token.DO)

     try
        let ret = chunk it true
        it.NextIsFrom([Token.END])
        ret
     with
     | ParseException(str,rng) ->
        ErrorStatement(str,rng)
        
let private forNum (it : TokenProcessor) : Expr*Expr*Expr*Statement =
    //skip past '='
    it.Skip()

    let init = expr it false
    it.Consume(Token.COMMA)
    let limit = expr it false

    let step = 
        if it.Peek() = Token.COMMA then
            it.Skip()
            expr it true
        else
            Number(1.0,(0,0))

    let chunk = forBody it
    (init,limit,step,chunk)

let private forGeneric (it : TokenProcessor) (varName : Expr) =
    let localVars = ref [varName]
    while it.TryConsume(Token.COMMA) do
        localVars := (singleVar it) :: !localVars

    it.Consume(Token.IN)
    let gens = expList1 it
    let chunk = forBody it
    (List.rev !localVars,gens,chunk)

let private forStatement (it : TokenProcessor) =
   let start = it.CurrentPos()
   //skip past 'for'
   it.Skip()
   
   let varName = (singleVar it)
   
   match it.Peek() with
   | Token.ASSIGN ->
       let (init,limit,step,body) = forNum it
       it.Consume(Token.END)
       let fin = it.PrevEndPos()
       ForNum(varName,init,limit,step,body,(start,fin))
   | Token.COMMA
   | Token.IN ->
       let (names, exps, body) = forGeneric it varName
       it.Consume(Token.END)
       let fin = it.PrevEndPos()
       ForGeneric(names, exps, body, (start,fin))
   | _ ->
       raise(ParseException("expected '=' or ','", it.PeekRange()))
     
let private repeatStatement (it : TokenProcessor) : Statement =
    let start = it.CurrentPos()

    // skip 'repeat'
    it.Skip()
    let body = chunk it true
    it.Consume(Token.UNTIL)
    let cond = expr it false
    let fin = it.PrevEndPos()

    Repeat(cond, body, (start,fin))

let private funcName (it : TokenProcessor) : Expr*bool =
    let start = it.CurrentPos()
    let names = ref (singleVar it)

    while it.TryConsume(Token.DOT) do
        let nextVar = indexName it
        let _,fin = getExprRange(nextVar)
        names := BinOpExpr(OpSelect, !names, nextVar, (start,fin))
    
    let hasMethCall =
        if it.TryConsume(Token.METHOD_IND) then
            let nextVar = indexName it
            let _,fin = getExprRange(nextVar)
            names := BinOpExpr(OpSelect, !names, nextVar, (start,fin))
            true
        else
            false

    (!names, hasMethCall)

let private functionStatement (it : TokenProcessor) : Statement =
    let start = it.CurrentPos()
    let rng = it.PeekRange()
    let annotation = it.PeekAnnotation()
    it.Skip()
    
    let names, hasMethCall = funcName it
    let pars,hasVarArgs,stmt = body it rng
    let fin = it.PrevEndPos()

    if hasMethCall then
        Assign([names], [Function(None, NameExpr("self",(0,0))::pars,hasVarArgs,stmt,annotation, (start,fin))], annotation, (start,fin))
    else
        Assign([names], [Function(None, pars,hasVarArgs,stmt,annotation,(start,fin))], annotation, (start,fin))

let private localStatement (it : TokenProcessor) : Statement =
    let start = it.CurrentPos()
    let annotation = it.PeekAnnotation()
    it.Consume(Token.LOCAL)
    let names = nameList1 it

    let exprs : Ref<List<Expr>> = ref []
    if it.TryConsume(Token.ASSIGN) then
        exprs := expList1 it

    let fin = it.PrevEndPos()
    
    LocalAssign(names,!exprs,annotation,(start,fin)) 

let private localFunction (it : TokenProcessor) : Statement = 
    let start = it.CurrentPos()
    let rng = it.PeekRange()
    let ann = it.PeekAnnotation()
    it.Consume(Token.LOCAL)
    it.Consume(Token.FUNCTION)
    let name = singleVar it
    
    let nameStr =
        match name with
        | NameExpr(nameStr,_) ->
            nameStr
        | _ ->
            failwith "unreachable"
        
    let pars,hasVarArgs,stmt = body it rng
    let fin = it.PrevEndPos()

    LocalAssign([name], [Function(Some nameStr, pars,hasVarArgs,stmt,ann,(start,fin))], ann, (start,fin))    

let private returnStatement (it : TokenProcessor) =
    let start = it.CurrentPos()
    it.Consume(Token.RETURN)
    
    if blockFollow(it.Peek()) || it.Peek() = Token.SEMI_COLON then
        let fin = it.PrevEndPos()
        Return([], (start,fin))
    else
        let retExps = expList1 it
        let fin = it.PrevEndPos()
        Return(retExps,(start,fin))

let private assignmentStatement (it : TokenProcessor) (leadingExp : Expr) (leadingAnnotation : Option<Annotation>)=
    let start = it.CurrentPos()
    let lhs = ref [leadingExp]
    while it.TryConsume(Token.COMMA) do
        lhs := (primaryExp it false) :: !lhs

    it.Consume(Token.ASSIGN)

    let rhs = expList1 it
    let fin = it.PrevEndPos()

    Assign(List.rev !lhs, rhs, leadingAnnotation, (start,fin))

let private exprStatement (it : TokenProcessor) (inLoop : bool) = 
    let ann = it.PeekAnnotation()
    let e = primaryExp it inLoop

    match e with
    | CallExpr(_,_,rng) as x ->
        Call(x,rng)
    | _ ->
        assignmentStatement it e ann

let private statement (it : TokenProcessor) (inLoop : bool) : bool*Statement =
    match it.Peek() with
    | Token.IF ->
        false, ifStatement it inLoop
    | Token.WHILE ->
        false, whileStatement it
    | Token.DO ->
        let start = it.CurrentPos()
        it.Consume(Token.DO)
        let ret = chunk it inLoop
        it.Consume(Token.END)
        let fin = it.PrevEndPos()
        false, Do(ret,(start,fin))
    | Token.FOR ->
        false, forStatement it
    | Token.REPEAT ->
        false, repeatStatement it
    | Token.FUNCTION ->
        false, functionStatement it
    | Token.LOCAL ->
        if it.PeekNext() = Token.FUNCTION then
            false, localFunction it
        else
            false, localStatement it
    | Token.RETURN ->
        true, returnStatement it
    | Token.BREAK ->
        let pos = it.CurrentPos()
        
        if inLoop then
            true, (it.Skip(); Break(pos,pos+5))
        else
            false, (it.Skip(); ErrorStatement("breaks should only occur inside of loops", (pos,pos+5)))
    | _ ->
        false, exprStatement it inLoop

let private eatSemicolons (it : TokenProcessor) = 
    while it.TryConsume(Token.SEMI_COLON) do
        ()

ref_chunk := fun (it : TokenProcessor) (inLoop : bool) ->
    eatSemicolons it
    let statements = new GenList<Statement>([])
    while not (blockFollow (it.Peek())) do
        let isLast, nextStatement =
            let beforePos = it.CurrentPos()
            try 
                statement it inLoop
            with
            | ParseException(msg,rng) ->
                it.RecoverStat()
                if it.CurrentPos() = beforePos then
                    it.Skip()
                false, ErrorStatement(msg,rng)
        
        eatSemicolons it

        let nextStatement = 
            if isLast && not (blockFollow (it.Peek())) then
                ErrorStatement("statements of this sort may only appear at the end of a block",getStatementRange(nextStatement))
            else
                nextStatement

        statements.Add(nextStatement)
    
    if statements.Count = 0 then
        Sequence(None,statements,(it.CurrentPos(),it.CurrentPos()))
    else
        let start,_ = getStatementRange statements.[0]
        let _,fin = getStatementRange statements.[statements.Count-1]
        Sequence(None,statements,(start,fin))

ref_expr := fun (it : TokenProcessor) (inLoop : bool) -> subExp it 0 inLoop

let rec outerChunk (it : TokenProcessor) =
    
    let statements = new GenList<Statement>([])

    while not (it.Peek() = Token.EOF) do
        let isLast, nextStatement = 
            let beforePos = it.CurrentPos()
            try
                statement it false
            with
            | ParseException(msg,rng) ->
                it.RecoverStat()
                if it.CurrentPos() = beforePos then
                    it.Skip()

                false, ErrorStatement(msg,rng)

        let nextStatement = 
            if isLast && not (it.Peek() = Token.EOF) then
                ErrorStatement("statements of this sort may only appear at the end of a block",getStatementRange(nextStatement))
            else
                nextStatement

        statements.Add(nextStatement)
        eatSemicolons it

    if statements.Count = 0 then
        Sequence(None,statements,(it.CurrentPos(),it.CurrentPos()))
    else
        let start,_ = getStatementRange statements.[0]
        let _,fin = getStatementRange statements.[statements.Count-1]
        Sequence(None,statements,(start,fin))

let generateCloserList (tokenLine : seq<TokenInfo>) =
    
    /// pop head from openerStack if tok closes the head of the opener stack
    /// push tok to openerStack if tok is an opener
    let foldTokenInfo (openerStack : List<Token>) (tok,_,_) =
        match openerStack with
        | hd :: tl when (Option.exists (fun x -> x = tok) (getCloser hd)) ->
            let closer = getCloser hd
            let closersCloser = getCloser closer.Value
            if closersCloser.IsSome then
                closersCloser.Value :: tl
            else
                openerStack.Tail
        | _ ->
            if (getCloser tok).IsSome then
                tok :: openerStack
            else
                openerStack
                
    let unresolvedOpeners = Seq.fold foldTokenInfo [] tokenLine

    List.collect 
        (fun opener -> (Option.toList (getCloser opener))) 
        unresolvedOpeners

let parse (str : string) (repair : bool) (fileName : string) : Statement*GenList<int> = 
    try
        let tokens,header,annotationMap,boundaries = HandLexer.lex str fileName
    
        let parseOuterChunk (processor : TokenProcessor) =
            try
            
                let ret = 
                    match outerChunk processor with
                    | Sequence(_,stats,rng) ->
                        Sequence(header,stats,rng)
                    | _ ->
                        failwith "unreachable"

                processor.Consume(Token.EOF)
                ret,boundaries
            with
            | RecoveryException(msg,rng) ->
                ErrorStatement(msg,rng), boundaries        

        if repair then
            let tokenList = new GenLinkedList<TokenInfo>(tokens)        

        
            let marker =  
                Seq.tryFind 
                    (fun (tok,str,_) -> tok = Token.NAME && str = Some("lucbdnioua"))
                    (seqOfEnumerable tokenList)

            // scintilla hasn't been updated to lex long strings,
            // so its possible that marker is None if we're in a long string
            let markerTok,markerStr,markerStartCharInd =
                if marker.IsSome then marker.Value else tokenList.First.Value

            //TODO: all consecutive lines before the marker line which end in commas should be included here

            // we need to convert the line boundary from char pos to token pos
            // should be easy by looking to the left and right of marker
            let i = boundaries.FindIndex( fun (n) -> n > markerStartCharInd )
            let markerLineStartCharInd = boundaries.[i-1]
            let markerLineEndCharInd = boundaries.[i]

            let onMarkerLine ((_,_,pos) : TokenInfo) =
                markerLineStartCharInd <= pos && pos < markerLineEndCharInd

            // get all of the tokens whose starting positions are between the above two indices
            let markerLineTokens = 
                List.ofSeq 
                    (Seq.filter onMarkerLine (seqOfEnumerable tokenList))
        
            let closers = generateCloserList markerLineTokens

            let markerNode = tokenList.Find( (markerTok,markerStr,markerStartCharInd) )

            //assert( (fst3 markerNode.Next.Value) = Token.OPEN_PAREN )
            //assert( (fst3 markerNode.Next.Next.Value) = Token.CLOSE_PAREN )

            let insertionPoint = markerNode.Next.Next.Next
            let insertionIndex = (Seq.findIndex (fun x -> x = (markerTok,markerStr,markerStartCharInd)) tokenList)+3

            let iterator = new LinkedListTokenIterator(tokenList)
            let processor = new TokenProcessor(iterator,annotationMap,str.Length)
            let parseRepaired = ref false

            let stat,boundaries = parseOuterChunk processor
            let ret = ref (stat,boundaries)
        
            let shiftAnnotationMap (insertedTokens : int) (map : Map<int,Annotation>) charInd ann =
                if charInd < insertionIndex then
                    map.Add(charInd,ann)
                else
                    map.Add(charInd+insertedTokens,ann)

            for leadingComma in [false;true] do
            
                if leadingComma then
                    ignore( tokenList.AddBefore(markerNode, (Token.COMMA,None,-1)) )
            
                let tokinf (token : Token) =
                    (token,None,-1)

                let addCloser (closerToken : Token) =
                    ignore (tokenList.AddBefore(insertionPoint,(closerToken,Some(""),-1)))

                List.iter addCloser closers

                for extras in [[];[tokinf(Token.END)];[tokinf(Token.CLOSE_PAREN)];[tokinf(Token.CLOSE_BRACKET)];
                                [tokinf(Token.UNTIL);tokinf(Token.TRUE)];[tokinf(Token.COMMA)];[tokinf(Token.COMMA);(Token.NAME,Some(""),-1)]] do

                    if not !parseRepaired then
                
                        List.iter (fun (extra : TokenInfo) ->  ignore ( tokenList.AddBefore(insertionPoint,extra) ) ) extras
                
                        let insertedTokens = (if leadingComma then 1 else 0) + extras.Length
                        let annotationMap = Map.fold (shiftAnnotationMap insertedTokens) Map.empty annotationMap
                        let iterator = new LinkedListTokenIterator(tokenList)
                        let processor = new TokenProcessor(iterator,annotationMap,str.Length)
                        let stat,boundaries = parseOuterChunk processor
                
                        if (ErrorList.listStatErrors false stat).Length = 0 then
                            ret := (stat,boundaries)
                            System.Console.WriteLine("parse repaired using extras: " + extras.ToString() + if leadingComma then " and leading comma." else ".") 
                            parseRepaired := true
                
                        List.iter (fun _ -> tokenList.Remove(insertionPoint.Previous)) [1..extras.Length] 

                // remove up to 2 closers one at a time while there are still inserted closers in the stream
                for i in 1 .. min closers.Length 2 do
            
                    // try throwing on some arbitrary closers in case there are unresolved openers
                    // on preceding lines

                    for extras in [[];[tokinf(Token.END)];[tokinf(Token.CLOSE_PAREN)];[tokinf(Token.CLOSE_BRACKET)];
                                    [tokinf(Token.UNTIL);tokinf(Token.TRUE)];[tokinf(Token.COMMA)];[tokinf(Token.COMMA);(Token.NAME,Some(""),-1)]] do

                        if not !parseRepaired then
                            List.iter (fun (extra : TokenInfo) ->  ignore ( tokenList.AddBefore(insertionPoint,extra) ) ) extras
                
                            let insertedTokens = (if leadingComma then 1 else 0) + closers.Length - (i-1) + extras.Length
                            let annotationMap = Map.fold (shiftAnnotationMap insertedTokens) Map.empty annotationMap
                            let iterator = new LinkedListTokenIterator(tokenList)
                            let processor = new TokenProcessor(iterator,annotationMap,str.Length)
                            let stat,boundaries = parseOuterChunk processor
                
                            if (ErrorList.listStatErrors false stat).Length = 0 then
                                ret := (stat,boundaries)
                                parseRepaired := true
                
                            List.iter (fun _ -> tokenList.Remove(insertionPoint.Previous)) [1..extras.Length]
                                                 
                    tokenList.Remove(insertionPoint.Previous)
        
                if leadingComma then
                    ignore( tokenList.Remove(markerNode.Previous) )

            !ret
        else
            let iterator = new ListTokenIterator(tokens)
            let processor = new TokenProcessor(iterator,annotationMap,str.Length)
            parseOuterChunk processor
    with
    | HandLexer.LexError(msg,pos,ln,boundaries) ->
        ErrorStatement(msg,(pos,pos+1)),boundaries

let parseExp (str : string) : Expr =
    let tokens,_,annotationMap,boundaries = HandLexer.lex str ""
    let it = new ListTokenIterator(tokens)
    let processor = new TokenProcessor(it, annotationMap, str.Length)

    try
        let ret = primaryExp processor false
        processor.Consume(Token.EOF)
        ret
    with
    | RecoveryException(msg,rng)
    | ParseException(msg,rng) ->
        ErrorExpr(msg,rng)
    