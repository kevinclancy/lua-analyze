﻿module ClassicTypeCollector

open LuaAnalyzer.Type
open LuaAnalyzer.TypeChecker
open LuaAnalyzer.TypedSyntax
open LuaAnalyzer.ErrorList
open LuaAnalyzer.Utils

open Mono.Addins

exception GlobalStyleModuleDefinition of Range

let rec getName (currentFile : string) (ast : TypedStatement) =
    match ast with
    | LocalAssign(
        _,
        _,
        [NameExpr(name,rng)],
        [Constructor([],_)],
        _
      ) ->
        Some (name,rng)
    | Assign(
        _,
        _,
        [NameExpr(name,rng)],
        [Constructor([],_)],
        _
      ) ->
        raise( GlobalStyleModuleDefinition(rng) )
    | Sequence(s0,s1,_) ->
        let n0 = getName currentFile s0
        let n1 = getName currentFile s1

        match (n0,n1) with
        | ( Some (name0,range0), Some (name1,range1) ) ->
            addError
                currentFile 
                "multiple class definitions in file"
                range1
            addError
                currentFile
                "multiple class definitions in file"
                range0
            None
          | ( Some x , _ ) ->
            Some x
          | ( _ , Some x ) ->
            Some x
          | _ ->
            None 
    | _ ->
       None                    

type FieldMap = Map<string, string*Type*DefinitionLocation>
type MethodMap = Map<string, Method>

let rec returnsName (name : string) (ast : TypedStatement) =
    match ast with
    | Sequence(s0,DoNothing(_),_) ->
        returnsName name s0
    | Sequence(s0,s1,_) ->
        returnsName name s1
    | Return([NameExpr(x,_)],_) when x = name ->
        true
    | _ ->
        false

let typeGraphBuilder (modPath : string) (fileName : string) (ast : TypedStatement) =
    try
        match getName fileName ast with
        | Some(name,rng) when returnsName name ast ->
            Some (rng,[],[])
        | Some(name,rng) ->
            addError 
                fileName
                ("Module table is not returned. Add return " + name + " to the end of this file")
                rng
            None
        | _ ->
            None
    with
    | GlobalStyleModuleDefinition(rng) ->
        addError fileName "Global style module definitions not allowed" rng
        None

let rec getFields (fileName : string) (moduleName : string) (className : string) (ast : TypedStatement) : FieldMap =
    match ast with
    | Assign(
        annotation,
        _,
        [BinOpExpr(OpInd,NameExpr(x,_),String(fieldName,nameRng),_)], 
        [Function(desc,selfName,formals,varargs,rets,body,(startLoc,_))],
        _
      ) when x = className ->
        let fieldTy = 
            FunctionTy(
                desc, 
                formals,
                rets, 
                false, 
                (fileName, nameRng)
            )

        new FieldMap([(fieldName,(desc,fieldTy,(fileName,nameRng)))])
    | Assign(
        annotation,
        _,
        [BinOpExpr(OpInd,NameExpr(x,_),String(fieldName,nameRng),_)], 
        [expr],
        _
      ) when x = className ->
        let (desc,_,_,vars) = annotation
        match vars.Length with
        | 0 ->
            addError
                fileName 
                "non-function fields should be given type ascriptions"
                nameRng
            Map.empty
        | _ ->
            let (desc,ty) = Type.InterpretVarAnnotation annotation 0 
            new FieldMap([(fieldName, (desc,ty,(fileName,nameRng)))])
    | Sequence(s0,s1,_) ->
        //TODO: detect duplicate method definitions
        let f0 = getFields fileName moduleName className s0
        let f1 = getFields fileName moduleName className s1
        cover f0 f1
    | _ ->
        Map.empty

let externalTypeBuilder (env : TypeEnvironment) (modname : string) (path : string) (ast : TypedStatement) =
    let typeMap = env.typeMap
    match getName path ast with
    | Some (name, rng) ->
        let fields = getFields path modname name ast
        let ty = RecordTy(
            modname,
            "", //TODO: we need to yoink module descriptions
            None,
            EmptyMetamethodSet, 
            fields,
            Map.empty,
            (path,rng)
        )

        // Note that for classic modules, the constructor type *is* the instance type
        Some (ty,ty)
    | _ ->
        raise( CollectorError "could not build external type" )

let internalTypeBuilder (env : TypeEnvironment) 
                        (globalCtxt : Context) 
                        (modname : string) 
                        (fileName : string)
                        (ast : TypedStatement) =
    
    Map.empty

let decorate (env : TypeEnvironment) (modname : string) (ast : TypedStatement) =
    ast

let detectMiscErrors (tenv : TypeEnvironment) (modulePath : string) (filePath : string) (ast : TypedStatement) =
    ()

[<Extension>]
type Initializer() =
    interface LuaAnalyzer.Analyzer.ITypeCollectorPlugin with
        member self.Init () =
            LuaAnalyzer.TypeChecker.addTypeCollector {
                name = "Classic Module System"
                typeGraphBuilder = typeGraphBuilder
                typeGraphIsTree = true
                externalTypeBuilder = externalTypeBuilder
                internalTypeBuilder = internalTypeBuilder
                decorate = decorate
                detectMiscErrors = detectMiscErrors
            }


