﻿module LuaAnalyzer.TypeChecker

open Syntax
open System.IO
open System.Windows.Forms

open Type
open TypedSyntax
open ErrorList
open Context
open TypeCollector
open SubtypeGraphBuilder
open Annotations
open Operators

type GenList<'T> = System.Collections.Generic.List<'T>

/// Information about some program term which was queried by the user 
/// (by, for example, hovering the mouse over that term).
type QueryResponse = {
    /// A comment description of the term which was queried
    desc : string
    /// The type of the term which was queried if it is a value
    /// If the term being queried is a typename, this should be None
    ty : Option<Type>
    /// The location which the queried term was defined
    loc : DefinitionLocation
    /// The range that the queried term occurs in.
    rng : Range
}

/// Before passing a type to an intellisense query, we pass it into this
/// function so that we do not display a TupleTy or UserDefinedTy
let prepareType env = Type.Untuple >> (Type.Unfold env)

/// Holds a context intentionally extracted from a certain location in 
/// the program. For example, whenever the user makes an intellisense query,
/// we extract the context wherever the caret is at.
let refStubContext : Ref<Context> = ref Context.empty

/// Holds information about a program term which was queried by the user.
let refQueryResponse : Ref< Option<QueryResponse> > = ref None

let currentFileName : Ref<string> = ref ("")

let refTypeCheckStat : Ref<Context -> TypedStatement -> unit> = ref( fun _ _ -> () )

/// Traverses a statement in order to generate typechecking errors or
/// query for information about a program element or the context at some point 
/// in the program.
let typeCheckStat (ctxt : Context) (stat : TypedStatement) =
    (!refTypeCheckStat) ctxt stat

let refTypeCheckExprCompute : Ref<Context -> TypedExpr -> Type*Option<Field>*Set<string>> = ref( fun _ _ -> UnknownTy,None,Set.empty )

let private refTypeCheckExprCheck : Ref<Context -> TypedExpr -> Type -> Option<Field>*Set<string>> = ref ( fun _ _ _ -> None,Set.empty)

/// Traverses an expression in order to generate typechecking errors or
/// query for information about a program element or the context at some point
/// in the program.
///
/// Returns a triple (ty,field,deps), where ty is the type of the given expression
/// (or UnknownTy for non-well-typed expressions). If the expression happens
/// to be a field lookup, field contains the field being looked up. Deps
/// is the set of all external modules which define types which the type computation
/// depended upon.
let typeCheckExprCompute (ctxt : Context) (expr : TypedExpr) =
    (!refTypeCheckExprCompute) ctxt expr

let rec typeCheckExprCheck (ctxt : Context) (expr : TypedExpr) (expectedTy : Type) =
    (!refTypeCheckExprCheck) ctxt expr expectedTy

// [<Pure>]
let isValidAccumulation (ctxt : Context) (path : List<string>) (target : Field) =
    if ctxt.HasValPath path then
        fst (Type.IsSubtypeOf ctxt.tenv target.ty (ctxt.FieldFromPath path).Value.ty)
    else
        ctxt.IsNewField path

/// Assuming this type was extracted from an annotation, generate errors if any of its
/// constituent types are UserDefined types whose type names are not in the type environment
/// provide query data if necessary
let checkAnnotationTy (ctxt : Context) (ty : Type) =
    let env = ctxt.tenv
    let rec checkAnnotationTyAux (ty : Type) =
        match ty with
        | TupleTy(_,_)
        | OverloadTy(_)
        | AccumulatedTy(_,_) 
        | ErrorTy(_) ->
            //tuples, overloads, error, and accumulated types should not be in annotations
            //failwith "unreachable"
            ()
        | RecordTy(_,_,true,_,_,metaset,fields,_,methods,typeArgs,_) ->
            Map.iter (fun _ (f : Field) -> checkAnnotationTyAux f.ty) fields
            Map.iter (fun _ (m : Method) -> checkAnnotationTyAux m.ty) methods

            let checkBinOpTy (_,rhsTy,retTy,_) =
                checkAnnotationTyAux rhsTy
                checkAnnotationTyAux retTy

            let checkUnOpTy (_,retTy,_) =
                checkAnnotationTyAux retTy

            Option.iter checkBinOpTy metaset.Add
            Option.iter checkBinOpTy metaset.Sub
            Option.iter checkBinOpTy metaset.Mul
            Option.iter checkBinOpTy metaset.Div
            Option.iter checkBinOpTy metaset.Mod
            Option.iter checkBinOpTy metaset.Pow
            Option.iter checkUnOpTy metaset.Unm
            Option.iter checkBinOpTy metaset.Concat
            Option.iter checkUnOpTy metaset.Len
            Option.iter checkBinOpTy metaset.Lt
            Option.iter checkBinOpTy metaset.Le
            Option.iter checkBinOpTy metaset.Gt
            Option.iter checkBinOpTy metaset.Ge
            Option.iter checkBinOpTy metaset.And
            Option.iter checkBinOpTy metaset.Or
            Option.iter checkUnOpTy metaset.Not
            Option.iter checkBinOpTy metaset.Index

            List.iter checkAnnotationTyAux typeArgs
        | FunctionTy(_,pars,_,rets,_,_,_,_) ->
            List.iter (thrd3 >> checkAnnotationTyAux) pars
            List.iter (snd >> checkAnnotationTyAux) rets
        | NillableTy(underlying) ->
            checkAnnotationTyAux underlying
        | UserDefinedTy(name,rng) ->
            if not ( (env.instanceTypes.Contains name) || (env.typedefNames.Contains name) || env.typeParamNames.Contains name ) then
                if ctxt.trackErrors then
                    addError
                        !currentFileName
                        ("type " + name + " undefined")
                        rng
            elif ctxt.queryPos.IsSome && inRange ctxt.queryPos.Value rng then
                let unf = Type.Unfold env ty
                match Type.Unfold env ty with
                | TypeAbsTy(varInfo,RecordTy(_,desc,_,_,_,_,_,_,_,_,defLocation)) ->
                    let varStr (varName,varDesc) = varName + " - " + varDesc
                    let varInfoDesc = String.concat "\n" (List.map varStr varInfo)
                    refQueryResponse := Some {
                        desc = desc + "\n\n" + "Type Parameters:\n" + varInfoDesc
                        ty = None
                        loc = defLocation
                        rng = rng
                    }
                | RecordTy(_,desc,_,displayStructural,_,_,_,_,_,_,defLocation) ->
                    refQueryResponse := Some {
                        // we can't just use the description here, because users like to see the structure of
                        // record types as well
                        desc = if displayStructural then 
                                 (if desc <> "user-defined type" then desc + "\n\n" else "") 
                                 + (Type.Unfold env ty).ToString() 
                               else 
                                 desc
                        ty = None
                        loc = defLocation
                        rng = rng
                    }
                | TypeVarTy(nm,desc,loc,_) ->
                    refQueryResponse := Some {
                        desc = desc
                        ty = None
                        loc = loc
                        rng = rng
                    }
                | _ ->
                    ()
        | TypeAppTy(applicand, args) ->
            match applicand with
            | UserDefinedTy(name,rng) ->
                if ctxt.tenv.typeMap.ContainsKey name then
                    match ctxt.tenv.typeMap.[name] with
                    | TypeAbsTy(varInfoList,body) ->
                        if args.Length <> varInfoList.Length then
                            if ctxt.trackErrors then
                                addError
                                    !currentFileName
                                    ("type abstraction applied to " + args.Length.ToString() + " arguments, but expects " + varInfoList.Length.ToString() + ".")
                                    rng
                    | _ ->
                        if ctxt.trackErrors then
                            addError
                                !currentFileName
                                ("cannot apply type " + ctxt.tenv.typeMap.[name].ToString() + " because it isn't an abstraction.")
                                rng
            | _ ->
                if ctxt.trackErrors then
                    addError
                        !currentFileName
                        ("cannot apply type " + applicand.ToString() + " because it isn't an abstraction.")
                        (0,0) //TODO: I need to obtain the range of this somehow
                                                
            checkAnnotationTyAux applicand
            List.iter checkAnnotationTyAux args
        | TypeVarTy(varName,desc,deflocation,rng) ->
            if ctxt.queryPos.IsSome && inRange ctxt.queryPos.Value rng then
                refQueryResponse := Some {
                    desc = desc
                    ty = None
                    loc = deflocation
                    rng = rng
                }            
        | _ ->
            ()

    if ctxt.trackErrors then
        checkAnnotationTyAux ty
    else
        ()
       
let checkFunctionAnnotation (ctxt : Context) (annotation : Option<Annotation>) (expr : TypedExpr) =
    if ctxt.trackErrors then 
        match expr,annotation with
        | Function(_,_,_,formals,_,_,_,_,_), Some(_,FunctionTagSet(pars,_,_,_,_),_,_) ->
            let checkPar ((par,nameRng) : Param*Range) =
                match List.tryFind (fun (name,desc,ty) -> name = fst3 par) formals with
                | Some(name,desc,ty) ->
                    ()
                | None ->
                    let name = fst3 par
                    addError
                        !currentFileName
                        ("formal " + name + " not found")
                        nameRng

            List.iter checkPar pars
        | _ ->
            ()
    else
        ()

let checkAnnotation (ctxt : Context) (annotation : Option<Annotation>) =

    match annotation with
    | Some(_,FunctionTagSet(pars,_,rets,_,_),_,_) ->
        List.iter (fst >> thrd3 >> (checkAnnotationTy ctxt)) pars
        List.iter (snd >> (checkAnnotationTy ctxt)) rets 
    | Some(_,VarConstTagSet(fields),_,_) ->
        List.iter (fst3 >> (Option.iter (checkAnnotationTy ctxt))) fields
    | Some(_,ModuleHeaderTagSet(typeDefs,_),_,_) ->
        List.iter (snd >> (checkAnnotationTy ctxt)) typeDefs
    | _ ->
        ()
        

/// Takes a context, l-expression, and target type, 
/// If the target type is a subtype of the l-expression's type and the l-expression
/// is a name chain (see NameChain active pattern), returns a name/type
/// pair which represents a modification to the context containing
/// the extra knowledge gained about the l-expression after we have assigned
/// it to an expression having targetType.
///
/// The following assignment under the given context:
/// Context [self : { x : ?number }]
/// self.x = 3
///
/// would yield ("self", RecordTy(_,_,_,fields.Add(x,NumberTy),_))
/// where _ represents an unchanged value from self's old type.
let contextRefinement (ctxt : Context) 
                      (lexp : TypedExpr) 
                      (targetType : Type) 
                      (targetDesc : string)
                      (targetConst : bool)
                      (targetDeps : Set<string>)
                      : Option<List<string>*Field> =

    //Contract.Ensures( 
    //    let res = Contract.Result< Option<List<string>*Field> >()
    //    res.IsNone || isValidAccumulation ctxt (fst res.Value) (snd res.Value)
    //)
        
    match lexp with
    | NameChain(path) when ctxt.IsNewField path ->
        let field = {
            Field.OfType(targetType) with 
                desc = targetDesc
                dependencies = targetDeps 
                isConst = targetConst 
                loc = !currentFileName,getExprRange lexp 
        }
        Some (path,field)
    | NameChain(path) when ctxt.HasValPath path ->
        let existingField = (ctxt.FieldFromPath path).Value
        if fst (Type.IsSubtypeOf ctxt.tenv targetType existingField.ty) then
            Some (path,{existingField with ty = targetType})
        else 
            None
    | _ ->
        None

/// ctxt - The context which the refinement is made to
/// path - a path identifying some field reachable from the value environment. 
///        its head should be an identifier in the value environment and its
///        tail should be a sequence of record field lookups
/// target - The field which we are refining. Its type should be a subtype of 
///          that of the field located by the path argument. 
///
/// If path specifies an new field w.r.t. ctxt, refines the context such that
/// the new field has been added. Otherwise, it must specify an existing field;
/// in this case, we modify the field specified by path so that its type 
/// accumulates target.ty.
let applyAccumulation (ctxt : Context) (path : List<string>) (target : Field) =
    
    //Contract.Requires(isValidAccumulation ctxt path target)
    //Contract.Ensures(Contract.Result<Context>().venv.Count >= ctxt.venv.Count) 
    //Contract.Ensures(Contract.Result<Context>().tenv === ctxt.tenv)
    
    if ctxt.venv.ContainsKey path.Head then
        let value = ctxt.venv.[path.Head]
        ctxt.AddValue(path.Head, { value with ty = Type.ApplyAccumulation ctxt.tenv value path.Tail target }) 
    elif path.Length = 1 && ctxt.addGlobals then
        ctxt.AddValue(path.Head, target) 
    else
        failwith "invalid refinement path"

/// ctxt - the context to undo the accumulation from
/// path - the path (w.r.t. the value environment) of the accumulation to undo
/// 
/// If the field at the specified path has an accumulated type, we replace
/// this accumulated type with its permanent component. Otherwise, we 
/// leave the field as is.
let undoAccumulation (ctxt : Context) (path : List<string>) =
    
    // Contract.Requires(ctxt.HasValPath path)
    // Contract.Ensures(Contract.Result<Context>().venv.Count = ctxt.venv.Count)

    let valName = path.Head
    let value = ctxt.venv.[valName]
    ctxt.AddValue(valName, {value with ty=Type.UndoAccumulation ctxt.tenv path.Tail value})

/// ctxt - the context which we are handling the accumulation update of
/// path - the path of the accumulation update we are handling
/// target - (when some) a field which we are accumulating onto the field at the given path
///          (when none) signifies that we are undoing the accumulation at the given path
///                      if this path is valid, and returning the context unchanged otherwise
///
/// Either adds or undoes an accumulation, depending on whether target is Some or None 
/// respectively.
let handleAccumulationUpdate (ctxt : Context) (path : List<string>) (target : Option<Field>) =    

    // Contract.Ensures(Contract.Result<Context>().venv.Count = ctxt.venv.Count)

    if target.IsSome then
        applyAccumulation ctxt path target.Value
    else
        if (ctxt.HasValPath path) && not (ctxt.IsNewField path) then
            undoAccumulation ctxt path
        else
            ctxt

/// ctxt - the context which the condition expression occurs in
/// cond - a condition expression to deduce accumulations from the negation of
/// 
/// returns a list of all accumulations which can be deduced from the fact that the
/// condition expression evaluates to false.
///
/// Example:
/// ctxt - [x : ?number]
/// the negation of "x == nil" tells us that x has type number
/// the negation of "x ~= nil" tells us that x has type nil
let rec getRefsFromNegCond (ctxt : Context) (cond : TypedExpr) : List<List<string>*Field> =
    let venv, tenv = ctxt.venv, ctxt.tenv
    match cond with
    | BinOpExpr(OpEq, exp, Nil(_),_)
    | BinOpExpr(OpEq, Nil(_), exp, _) ->
        let expTy,expField,_ = typeCheckExprCompute ctxt.DontTrackErrors.IsNotLeftExpr exp
        match expTy with
        | NillableTy(underlyingTy) ->
            let desc = if expField.IsSome then expField.Value.desc else ""
            let isConst = if expField.IsSome then expField.Value.isConst else false
            let ref = contextRefinement ctxt exp underlyingTy desc isConst Set.empty
            if ref.IsSome then [ref.Value] else []
        | _ ->
            []
    | BinOpExpr(OpNe, exp, Nil(_),_)
    | BinOpExpr(OpNe, Nil(_), exp, _) ->
        let expTy,expField,_ = typeCheckExprCompute ctxt.DontTrackErrors.IsNotLeftExpr exp
        match expTy with
        | NillableTy(underlyingTy) ->
            let desc = if expField.IsSome then expField.Value.desc else ""
            let isConst = if expField.IsSome then expField.Value.isConst else false
            let ref = contextRefinement ctxt exp NilTy desc isConst Set.empty
            if ref.IsSome then [ref.Value] else [] 
        | _ ->
            []
    | BinOpExpr(OpNe, expA, expB,_) ->
        let tyA,fieldA,_ = typeCheckExprCompute ctxt.DontTrackErrors.IsNotLeftExpr expA
        let tyB,fieldB,_ = typeCheckExprCompute ctxt.DontTrackErrors.IsNotLeftExpr expB
        let rA = contextRefinement ctxt expA tyB "" false Set.empty
        let rB = contextRefinement ctxt expB tyA "" false Set.empty

        if rA.IsSome then
            [rA.Value]
        elif rB.IsSome then
            [rB.Value]
        else
            []
    | BinOpExpr(OpOr, a, b,_) ->
        List.append (getRefsFromNegCond ctxt a) 
                    (getRefsFromNegCond ctxt b)
    | BinOpExpr(OpAnd, a, b, _) ->
        //TODO: too lazy to implement this now; it will be semi-tricky
        []
    | _ ->
        []

/// ctxt - the context which the condition expression occurs in
/// cond - a condition expression to deduce accumulations from
/// 
/// returns a list of all accumulations which can be deduced from the fact that the
/// condition expression evaluates to true.
///
/// Example:
/// ctxt - [x : ?number]
/// "x == nil" tells us that x has type nil
/// "x ~= nil" tells us that x has type number
let rec getRefinements (ctxt : Context) (cond : TypedExpr) : List<List<string>*Field> =
    let venv, tenv = ctxt.venv, ctxt.tenv
    match cond with
    | BinOpExpr(OpNe, exp, Nil(_),_)
    | BinOpExpr(OpNe, Nil(_), exp, _) ->
        let expTy,expField,_ = typeCheckExprCompute ctxt.DontTrackErrors.IsNotLeftExpr exp
        match expTy with
        | NillableTy(underlyingTy) ->
            let desc = if expField.IsSome then expField.Value.desc else ""
            let isConst = if expField.IsSome then expField.Value.isConst else false
            let refinement = contextRefinement ctxt exp underlyingTy desc isConst Set.empty
        
            if refinement.IsSome then
                [refinement.Value]
            else
                []
        | _ ->
            []
    | BinOpExpr(OpEq, expA, expB,_) ->
        let tyA,fieldA,_ = typeCheckExprCompute ctxt.DontTrackErrors.IsNotLeftExpr expA
        let tyB,fieldB,_ = typeCheckExprCompute ctxt.DontTrackErrors.IsNotLeftExpr expB
        let rA = contextRefinement ctxt expA tyB "" false Set.empty
        let rB = contextRefinement ctxt expB tyA "" false Set.empty

        if rA.IsSome then
            [rA.Value]
        elif rB.IsSome then
            [rB.Value]
        else
            []
    | BinOpExpr(OpAnd, a, b,_) ->
        List.append (getRefinements ctxt a) (getRefinements ctxt b)
    | BinOpExpr(OpOr, a, b, _) ->
        //TODO: too lazy to implement this now; it will be semi-tricky
        []
    | _ ->
        []

/// ctxt - the context in which an "if" statement appears
/// clauses - the clauses of the "if" statement (condition/body pairs)
/// i - an integer at most as large as the number of if statement clauses
/// 
/// Returns the list of all accumulations which can be deduced from the
/// fact that the first (i-1) clauses of the if statement are not taken and
/// the ith is.
///
/// The results of this function are applied to the context going into
/// the body of the (i+1)st clause.
let getCondRefinementsForI (ctxt : Context) 
                       (clauses : List<TypedExpr*TypedStatement>) 
                       (i : int) 
                       : Map<List<string>,Field> =
    
    // Contract.Requires(i <= clauses.Length)

    let negCondRefs = List.collect (fun j -> getRefsFromNegCond ctxt (fst clauses.[j])) [0..i-1]
    let negRefinements = List.fold (fun (m : Map<List<string>,Field>) ref -> m.Add ref) Map.empty negCondRefs
    let condRefs = getRefinements ctxt (fst clauses.[i])
    List.fold (fun (m : Map<List<string>,Field>) ref -> m.Add ref) negRefinements condRefs

/// ctxt - The context an "if" statement appears in
/// clauseAccumulationMaps - A list of accumulation maps for the bodies of
///                          the "if" statement's clauses
/// isComplete - True iff the user provided the "else" clause, false iff it was generated 
///              by LuaAnalyzer.
/// path - a path of field lookups w.r.t. the value environment
/// srcField - the field located at the path
///
/// Given that the field contained at path was srcField going into
/// the "if" statement, return the accumulation made for that path coming
/// out of the if statement (or None if any existing accumulation should be undone).
let pathAccumulationFromIf (ctxt : Context)
                           (clauseAccumulationMaps : List<Map<List<string>, Field>>)
                           (isComplete : bool) 
                           (path : List<string>) 
                           (srcField : Field) 
                           : Option<Field> =

    /// i - the index of a clause (1st is 0, 2nd is 1, etc.)
    /// m - a map of all accumulations made by the body of the
    ///     ith clause
    ///
    /// - Returns the accumulation on path made by the ith clause if one exists.
    /// - Returns the existing accumulation on path if the ith clause
    /// makes no accumulations on it.
    /// - If neither the ith clause or ctxt has accumulations on path, return 
    /// field with ty=UnknownTy
    let mapClause (i : int) (m : Map<List<string>,Field>) =
        let f = 
            if m.ContainsKey path then
                m.[path]
            else
                if (ctxt.FieldFromPath path).IsSome then
                    (ctxt.FieldFromPath path).Value
                else
                    if ctxt.IsNewField path && ctxt.trackErrors then
                        addError
                            !currentFileName 
                            ("field " + (String.concat "." path) + " not assigned in " + Utils.orderStr (i+1) + " clause of if statement.")
                            (snd srcField.loc)

                    Field.OfType(UnknownTy)

        match f with
        | {desc=_;ty=AccumulatedTy(_,temp);isConst=_;loc=_} ->
            { f with ty = temp }
        | _ ->
            f

    if ctxt.trackErrors && not isComplete && ctxt.IsNewField path then
        addError
            !currentFileName
            "new field assignment is made in incomplete if statement. Add else clause or remove assignment."
            (snd srcField.loc)
        None
    else
        let refFields = List.mapi mapClause clauseAccumulationMaps 

        if (List.tryFind (fun (f : Field) -> f.ty = UnknownTy) refFields).IsSome then
            None
        else
            let hd = refFields.Head
            if List.forall (fun (f : Field) -> fst (Type.IsEqual ctxt.tenv hd.ty f.ty)) refFields then
                let deps = Set.unionMany (List.map (fun f -> f.dependencies) refFields)
                Some { hd with dependencies = deps }
            else
                if ctxt.IsNewField path && ctxt.trackErrors then
                    addError 
                        !currentFileName
                        "field is not assigned same type along all branches of if (maybe adding annotations will help)"
                        (snd hd.loc)
                None


/// ctxt - The context an "if" statement appears in
/// clauseAccumulationMaps - A list of accumulation maps for the bodies of
///                          the "if" statement's clauses
/// isComplete - True iff the user provided the "else" clause, false iff it was generated 
///              by LuaAnalyzer.
///
/// For an if statement, we merge the accumulations made by clauses as follows:
/// - If all clauses assign some path to some type, we consider that an accumulation
/// from the entire if statement.
/// - If some path is assigned to different types by different clauses, we undo 
/// accumulations on that path if any exist.
let mergeClauseAccumulations (ctxt : Context) 
                             (clauseAccumulationMaps : List<Map<List<string>, Field>>) 
                             (isComplete : bool) 
                             : Map<List<string>,Option<Field>> =
    
    // Each "if" statement must have an "if" clause as well as 
    // an "else" clause. (If "else" isn't present in concrete syntax, a
    // blank "else" clause is inserted automatically into the abstract syntax.)
    // Contract.Requires(clauseAccumulationMaps.Length >= 2)

    // all paths mentioned in assignments in any clause
    // (we keep some representative field so that we can use its location
    //  for error messages)
    let allRefinements = 
        List.fold cover Map.empty clauseAccumulationMaps
    
    // add the accumulation made on path by the "if" statement
    // f is the field associated with the path in the last clause it is mentioned in
    // f's definition location is used for error messages
    let foldPath (acc : Map<List<string>,Option<Field>>) (path : List<string>) (f : Field) =
        acc.Add(path, pathAccumulationFromIf ctxt clauseAccumulationMaps isComplete path f)

    Map.fold foldPath Map.empty allRefinements

let refGetAllAccumulations = ref (fun ctxt stat -> Map.empty)

/// ctxt - the context which stat appears in
/// stat - the statement which we are extracting accumulations from 
///
/// gets all accumulations made by stat under ctxt
let getAllAccumulations (ctxt : Context) (stat : TypedStatement) : Map<List<string>,Field> =
    (!refGetAllAccumulations) ctxt stat
   
/// If expr's type actualTy is a subtype of expected, return true.
/// Otherwise, produce an error (using expr's range), and return false.
let checkType (ctxt : Context) (expr : TypedExpr) (actualTy : Type) (expected : Type) =
    let ret, expl = (Type.IsSubtypeOf ctxt.tenv actualTy expected) 
    if not ret then
        if ctxt.trackErrors then
            addError
                (!currentFileName)
                ("actual type " + Type.Untuple(actualTy).ToString() + " does not match expected type " + Type.Untuple(expected).ToString() + expl)
                (getExprRange expr)
        false
    else
        true

// given for loop (numeric or generic) and a context, return an updated
// version of the context, appropriate for descending into the body of the loop
let updateCtxtForBody (ctxt : Context) (stat : TypedStatement) =
    match stat with
    | ForNum(var,start,fin,step,body,_) ->
        match var with
        | NameExpr(name,rng) ->
            let counterField = {
                desc = "loop counter"
                ty = NumberTy
                dependencies = Set.empty
                loc = (!currentFileName,rng)
                isVisible = true
                isConst = true
            }

            ctxt.AddValue(name,counterField)
        | _ ->
            failwith "unreachable"
    | ForGeneric([NameExpr(i,irng);NameExpr(v,vrng)],[CallExpr(NameExpr("ipairs",_),[arrayExpr],_)],body,_) ->
        let arrTy,field,_ = typeCheckExprCompute ctxt arrayExpr
        let deps = if field.IsSome then field.Value.dependencies else Set.empty

        let elemTy =
            match arrTy with
            | ArrayTy(elemTy) ->
                elemTy
            | _ ->
                UnknownTy

        let iField = {
            desc = "index"
            ty = NumberTy
            dependencies = Set.empty
            loc = (!currentFileName,irng)
            isVisible = true
            isConst = true
        }

        let vField = {
            desc = "value"
            ty = elemTy
            dependencies = deps
            loc = (!currentFileName,vrng)
            isVisible = true
            isConst = true
        }
        let ctxt = ctxt.AddValue(i,iField)
        let ctxt = ctxt.AddValue(v,vField)
        ctxt
    | ForGeneric(vars,[CallExpr(NameExpr("pairs",_),_,_)],body,_) ->
        //TODO: giving the pairs function an appropriate type (involving "unknown" in many places),
        //this clause should not be necessary
        let makeNamedField (nameExpr : TypedExpr) =
            let name,rng = 
                match nameExpr with
                | NameExpr(name,rng) ->
                    name,rng
                | _ ->
                    failwith "unreachable"
            name,
            {
                desc = "loop iteration variable"
                ty = UnknownTy
                dependencies = Set.empty
                loc = (!currentFileName,rng)
                isVisible = true
                isConst = true
            }

        let iterationFields = List.map makeNamedField vars
        List.fold (fun (c : Context) ((n,f) : string*Field) -> c.AddValue(n,f)) ctxt iterationFields
    | ForGeneric(vars,gens,body,_) ->
        let genTypes = List.map (typeCheckExprCompute ctxt) gens
        let genSeqTy = Type.SeqType (List.map fst3 genTypes)

        let makeNamedField (nameExpr : TypedExpr) =
            let name,rng = 
                match nameExpr with
                | NameExpr(name,rng) ->
                    name,rng
                | _ ->
                    failwith "unreachable"
            name,
            {
                desc = "loop iteration variable"
                ty = UnknownTy
                dependencies = Set.empty
                loc = (!currentFileName,rng)
                isVisible = true
                isConst = true
            }

        let unknownVars = List.map makeNamedField vars
        let unknownVarsCtxt = List.fold (fun (c : Context) ((n,f) : string*Field) -> c.AddValue(n,f)) ctxt unknownVars

        match genSeqTy with
        | [],true
        | (UnknownTy :: _),_
        | _,true ->
            unknownVarsCtxt
        | tuple,_ when tuple.Length <> 3 ->
            unknownVarsCtxt
        | [CallableTy(_,pars,_,rets,_,_,_,_) as I;S;V1],false ->
            if rets.Length <> vars.Length then
                unknownVarsCtxt
            else
                let vTypes = List.map snd rets
                let pTypes = List.map thrd3 pars
                let pTypes = if pTypes.Length = 1 then pTypes @ [NilTy] else pTypes

                if pTypes.Length <> 2 then
                    unknownVarsCtxt
                else
                    match Type.IsSubtypeOf (ctxt.tenv) S (pTypes.[0]) with
                    | true, _ ->
                        match Type.IsSubtypeOf (ctxt.tenv) V1 (vTypes.[0]) with
                        | true,_ ->
                            let vTypes = 
                                match genSeqTy with
                                | [CallableTy(_,_,_,rets,_,_,_,_);_;_],false ->
                                    List.map snd rets
                                | _ ->
                                    failwith "this function should not be called unless the for statement is well typed"

                            let makeNamedField i ((nameExpr,vType) : TypedExpr*Type) =
                                let name,rng = 
                                    match nameExpr with
                                    | NameExpr(name,rng) ->
                                        name,rng
                                    | _ ->
                                        failwith "unreachable"
                                name,
                                {
                                    desc = "loop iteration variable"
                                    ty = if i = 0 then vType.StripNillable() else vType
                                    dependencies = Set.empty
                                    loc = (!currentFileName,rng)
                                    isVisible = true
                                    isConst = true
                                }

                            let iterationFields = List.mapi makeNamedField (List.zip vars vTypes)
                            List.fold (fun (c : Context) ((n,f) : string*Field) -> c.AddValue(n,f)) ctxt iterationFields
                        | false,msg ->
                            unknownVarsCtxt
                    | false,msg ->
                        unknownVarsCtxt
        | [_;_;_],false ->
            unknownVarsCtxt
    | _ ->
        failwith "only for statement asts should be used as the second argument to this function"

/// Given a context and statement, return a modified version 
/// of the context, containing any new variable bindings or refinements made 
/// by the statement.
let rec updateCtxt (ctxt : Context) (stat : TypedStatement) =
    let ctxt = ctxt.DontRecordStub
    let venv, tenv = ctxt.venv, ctxt.tenv
    let result = 
        match stat with
        | Call( CallExpr( NameExpr("assert",_), [cond], _ ), _ ) 
        | Call( CallExpr( NameExpr("assert",_), [cond;_], _ ), _ ) ->
             let refinements = getRefinements ctxt cond
             List.fold (fun ctxt (path,field) -> applyAccumulation ctxt path field) ctxt refinements
        | If(clauses,elseExplicit,rng) ->

            let getClauseRefinements (i : int) =
                let cond,body = clauses.[i]
                let condRefinements = getCondRefinementsForI ctxt clauses i
                let ctxt = Map.fold (fun ctxt path field -> applyAccumulation ctxt path field) ctxt condRefinements
                cover condRefinements (getAllAccumulations ctxt body)
        
            let clauseRefinements = List.init clauses.Length getClauseRefinements
            let merged = mergeClauseAccumulations ctxt clauseRefinements elseExplicit
            Map.fold handleAccumulationUpdate ctxt merged
        | Sequence(_,stats, rng) when stats.Count > 0 ->
            Seq.fold (fun ctxt stat -> updateCtxt ctxt stat) ctxt stats
        | LocalAssign(annotation,lhs,rhs,rng) ->
            let rhs =
                if rhs.Length = 0 then
                    [Nil(rng)]
                else
                    rhs
        
            let ldescs,lascripts,isConstList = Type.InterpretVarAnnotation annotation lhs.Length 
            let rctxt = { ctxt.DontTrackErrors.IsNotLeftExpr with queryPos = None }
            let results = List.map (fun expr -> (typeCheckExprCompute rctxt expr)) rhs
            let rtypes,rdeps = List.map fst3 results, List.map thrd3 results
            let frtypes,indefinite = Type.SeqType rtypes
            let frtypes = coverListOpt frtypes lascripts
            let filler = if indefinite then UnknownTy else NilTy
            let frtypes = sizeListTo frtypes lhs.Length filler
            let rdeps = sizeListTo rdeps lhs.Length Set.empty

            let foldItem (c : Context) (i : int) =
                let lexp = lhs.[i]
                let ldesc = ldescs.[i]
                let ty = Type.TypeEval ctxt.tenv frtypes.[i]
                let deps = rdeps.[i]

                let name,rng = 
                    match lexp with 
                    | NameExpr(name,rng) -> 
                        name,rng 
                    | _ -> 
                        failwith "unreachable"
            
                let field = {
                    Field.OfType(ty) with
                        desc = ldesc
                        dependencies = deps
                        loc = (!currentFileName,rng)
                        isConst = isConstList.[i]
                }

                c.AddValue(name,field)

            List.fold foldItem ctxt [0..lhs.Length-1] 

        | Assign(annotation,lhs,rhs,rng) ->
            let rctxt = { ctxt.DontTrackErrors.IsNotLeftExpr with queryPos = None }
            let descs,ascriptions,isConstList = Type.InterpretVarAnnotation annotation lhs.Length
            let rtypes,_,rdeps = List.unzip3 (List.map (fun expr -> (typeCheckExprCompute rctxt expr)) rhs)
            let frtypes,indefinite = Type.SeqType rtypes
            let frtypes = coverListOpt frtypes ascriptions
            let filler = if indefinite then UnknownTy else NilTy
            let frtypes = sizeListTo frtypes lhs.Length filler 
            let rdeps = sizeListTo rdeps lhs.Length Set.empty
            let pairs = List.zip lhs frtypes

            let foldItem (accCtxt : Context) (i : int)  =
                let expr, ty, desc,isConst,rdeps = lhs.[i], frtypes.[i], descs.[i], isConstList.[i], rdeps.[i]
            
                let ty = Type.TypeEval accCtxt.tenv ty

                match contextRefinement accCtxt expr ty desc isConst rdeps with
                | Some(path,newField) ->
                    applyAccumulation accCtxt path newField
                | None ->
                    accCtxt

            List.fold foldItem ctxt [0..lhs.Length-1]
        | Do(body,_) ->
            //Because do and repeat statements have the guarantee that their
            //bodies will execute, we carry the refinements made in the bodies
            //out of scope.
            let refinements = getAllAccumulations ctxt body
            Map.fold applyAccumulation ctxt refinements
        | Repeat(cond,body,_) ->
            let refinements = getAllAccumulations ctxt body
            let ctxt' = Map.fold applyAccumulation ctxt refinements
            let refinements' = getRefsFromNegCond ctxt' cond
            List.fold (fun ctxt (path,field) -> applyAccumulation ctxt' path field) ctxt' refinements'
        | ForNum(_,_,_,_,body,_)
        | ForGeneric(_,_,body,_) ->

            // introduce itererators into context
            let ctxt' = updateCtxtForBody ctxt stat

            //we have no guarantee that the bodies of these statements will
            //execute (though with a bit more code, we could identify numeric
            //loops for which this is not the case), so we undo all refinements
            //made.
            let refinements = getAllAccumulations ctxt' body
            let undo (ctxt : Context) (path : List<string>) (field : Field) =
                if ctxt.HasValPath path then 
                    undoAccumulation ctxt path 
                else 
                    ctxt
        
            Map.fold undo ctxt refinements
        | While(cond, body, _) ->
            //TODO: the fact that the condition is no longer true after
            //the loop should give us some deductions
            let refinements = getAllAccumulations ctxt body
            let undo (ctxt : Context) (path : List<string>) (field : Field) =
                if ctxt.HasValPath path then 
                    undoAccumulation ctxt path 
                else 
                    ctxt
            let ctxt' = Map.fold undo ctxt refinements
            let refinements' = getRefsFromNegCond ctxt' cond
            List.fold (fun ctxt (path,field) -> applyAccumulation ctxt path field) ctxt' refinements'
        | _ ->
            ctxt
    result.RecordStub

// see getAllAccumulations for description
refGetAllAccumulations := fun (ctxt : Context) (stat : TypedStatement) ->
    // gets all accumulations from stat under ctxt, except those rooted at
    // identifiers contained in locals
    let rec getAllAccumulationsAux (ctxt : Context) (locals : Set<string>) (stat : TypedStatement) =
        let venv, tenv = ctxt.venv, ctxt.tenv
        match stat with
        | If(clauses,_,_) ->
            let clauseRefinements = List.map (getAllAccumulationsAux ctxt locals) (List.map snd clauses)
            List.fold cover Map.empty clauseRefinements
        | Do(body,_)
        | ForNum(_,_,_,_,body,_)
        | ForGeneric(_,_,body,_) ->
            getAllAccumulationsAux ctxt locals body
        | Repeat(cond,body,_) ->
            let refs = getRefsFromNegCond ctxt cond
            let condRefs = List.fold (fun (m : Map<List<string>,Field>) (path,acc) -> m.Add(path,acc)) Map.empty refs
            let bodyRefs = getAllAccumulationsAux ctxt locals body
            cover bodyRefs condRefs
        | While(cond,body,_) ->
            let refs = getRefsFromNegCond ctxt cond
            List.fold (fun m (path,acc) -> m.Add(path,acc)) Map.empty refs
        | Assign(annotation,lvals,rvals,_) ->
            let descriptions,ascriptions,isConstList = Type.InterpretVarAnnotation annotation lvals.Length
            let rctxt = ctxt.DontTrackErrors.IsNotLeftExpr
            let tyCheckRVal expr = 
                let ty,field,deps = typeCheckExprCompute rctxt expr
                ty,if field.IsSome then Set.union deps field.Value.dependencies else deps
            let rtypes,rdeps = List.unzip (List.map tyCheckRVal rvals)
            let frtypes,indefinite = Type.SeqType rtypes
            let filler = if indefinite then UnknownTy else NilTy
            let frtypes = coverListOpt frtypes ascriptions
            let frtypes = sizeListTo frtypes lvals.Length filler
            let rdeps = sizeListTo rdeps lvals.Length Set.empty

            let makeQuad (i : int) =
                lvals.[i],
                { 
                    desc = descriptions.[i]
                    ty = frtypes.[i]
                    dependencies = rdeps.[i]
                    isConst = isConstList.[i]
                    isVisible = true
                    loc = (!currentFileName, getExprRange lvals.[i])
                },
                descriptions.[i],
                isConstList.[i],
                rdeps.[i]

            let items = List.init lvals.Length makeQuad

            let collectField ((expr,f,desc,isConst,deps) : TypedExpr*Field*string*bool*Set<string>) =
                match contextRefinement ctxt expr f.ty desc isConst deps with
                | Some(head :: rest,f) when not (locals.Contains head) ->
                    [(head :: rest,f)]
                | _ ->
                    []

            Map.ofList (List.collect collectField items)        
        | Sequence(_,stats,_) ->
            let foldStat ((ctxt,locals,accs) : Context*Set<string>*Map<List<string>,Field>) 
                         (s0 : TypedStatement) =
             
                let a0 = getAllAccumulationsAux ctxt locals s0
                let ctxt' = updateCtxt ctxt s0

                let locals' =
                    match s0 with
                    | LocalAssign(_,lhs,_,_) ->
                        let foldExpr (locals : Set<string>) (expr : TypedExpr) : Set<string> =
                            match expr with
                            | NameExpr(name,_) ->
                                locals.Add(name)
                            | _ ->
                                failwith "unreachable"

                        List.fold foldExpr locals lhs
                    | _ ->
                        locals

                ctxt', locals', cover accs a0

            let _,_,accs = Seq.fold foldStat (ctxt, locals, Map.empty) stats
            accs
        | _ ->
            Map.empty

    getAllAccumulationsAux ctxt Set.empty stat

/// Gets types of all unbound assigns in statement, checked w.r.t. context
/// example: "local a = 5;a=6" would give []
/// example: "a=6;local a = 'hello'; a=''" would give [("a",NumberTy)]
let rec getAssigns (ctxt : Context) (stat : TypedStatement) : ValueEnvironment =
    let venv,tenv = ctxt.venv, ctxt.tenv
    match stat with
    | If(clauses,_,rng) ->
        // For if statements, we get only those variables that are assigned in
        // each clause, using the type from the first clause
        let clauseAssigns = List.map (getAssigns ctxt) (List.map snd clauses)
        let clause0 = clauseAssigns.[0]
        
        //remove from acc any key not in other
        let clean (acc : ValueEnvironment) (other : ValueEnvironment) =
            let ret = ref acc
            for kv in acc do
                let k = kv.Key
                if not (other.ContainsKey k) then
                    ret := (!ret).Remove k
            !ret
        
        //remove from clause0 any key not contained in any other clause
        //the gives us a context with only those assigns made by all clauses.
        List.fold clean clause0 clauseAssigns
    | While(_,body,_)
    | Do(body,_)
    | ForNum(_,_,_,_,body,_)
    | ForGeneric(_,_,body,_)
    | Repeat(_,body,_) ->
        getAssigns ctxt body
    | Assign(annotation,lvals,rvals,_) ->
        let descriptions,ascriptions,isConstList = Type.InterpretVarAnnotation annotation lvals.Length 
        let rctxt = ctxt.DontTrackErrors.IsNotLeftExpr
        let tyCheckRVal expr = 
            let ty,field,deps = typeCheckExprCompute rctxt expr
            ty,deps
        let rtypes,rdeps = List.unzip (List.map tyCheckRVal rvals)
        let frtypes,indefinite = Type.SeqType rtypes
        let filler =
            if indefinite then
                UnknownTy
            else 
                NilTy
        let frtypes = coverListOpt frtypes ascriptions
        let frtypes = sizeListTo frtypes lvals.Length filler

        let makeQuad (i : int) =
            (lvals.[i],frtypes.[i],descriptions.[i],isConstList.[i], rdeps.[min i (rdeps.Length-1)])

        let quads = List.init lvals.Length makeQuad

        let filter ((lexp,rtype,desc,isConst,deps) : TypedExpr*Type*string*bool*Set<string>) =
            match lexp with
            | NameExpr(name,_) when not (venv.ContainsKey name) ->
                true
            | _ ->
                false

        let newAssigns = List.filter filter quads

        let foldField (mp : ValueEnvironment) (expr,ty,desc,isConst,deps) =
            let n,rng =
                match expr with
                | NameExpr(n,rng) ->
                    n,rng
                | _ ->
                    failwith "unreachable"

            mp.Add(n,{desc=desc;ty=ty;dependencies=deps;loc=(!currentFileName,rng);isVisible=true;isConst=isConst})

        List.fold foldField
                  Map.empty 
                  newAssigns

    | Sequence(_,stats,_) when stats.Count > 0 ->
        let foldStat ((ctxt,res) : Context*Map<string,Field>)
                     (stat : TypedStatement) =
                     
            let assigns = getAssigns ctxt stat 
            let ctxt = 
                {
                ctxt with
                    venv = cover (ctxt.venv) assigns
                }
            let ctxt = updateCtxt ctxt stat
            (ctxt,cover res assigns)

        let _,res = Seq.fold foldStat (ctxt,Map.empty) stats
        res
    | _ ->
        Map.empty

/// ctxt - A context to reason under
/// field - a structural field (i.e. w/ string key) which we extract a key,field pair from
///
/// Gets the key and field corresponding to a structural constructor field
let getLabelAndField (ctxt : Context) (field : TypedConstructorField) : string*Field =
    
    // Contract.Requires(TypedConstructorField.IsStructural field)

    match field with
    | RecField(ann,String(k,_),v,rng) ->
        let desc,ascs,isConstList = Type.InterpretVarAnnotation ann 1
                
        let ctxt = ctxt.IsNotLeftExpr.DontTrackErrors

        let field = 
            match ascs.[0] with
            | Some ty ->
                let ty,field,deps = (typeCheckExprCompute ctxt (Ascription(v,ty,rng)))
                {
                    desc = desc.[0]
                    ty = Type.Untuple ty
                    dependencies = deps
                    loc = (!currentFileName,rng)
                    isVisible = true
                    isConst = isConstList.[0]
                }
            | None -> 
                let ty,field,deps = typeCheckExprCompute ctxt v
                let deps = if field.IsSome then field.Value.dependencies else Set.empty
                {
                    desc = desc.[0]
                    ty = Type.Untuple ty
                    dependencies = deps
                    loc = (!currentFileName,rng)
                    isVisible = true
                    isConst = isConstList.[0]
                }

        (k, field)
    | _ ->
        failwith "only structural fields allowed"

/// typechecks the field's value w.r.t. ctxt, adding any errors encountered to the error
/// list (if tracking errors)
let typeCheckField (ctxt : Context) (field : TypedConstructorField) =
    match field with
    | ListField(annotation,expr, _) ->
        checkAnnotation ctxt annotation
        let descs, ascs, isConstList = Type.InterpretVarAnnotation annotation 1
        let actualTy,_,_ = typeCheckExprCompute ctxt expr
        match ascs.[0] with
        | Some expectedTy ->
            ignore (checkType ctxt expr actualTy expectedTy)
        | None ->
            ()

    | RecField(annotation,keyExpr, valExpr, _) ->
        checkAnnotation ctxt annotation

        ignore (typeCheckExprCompute ctxt keyExpr)
        let descs, ascs,isConstList = Type.InterpretVarAnnotation annotation 1
        let actualTy,_,_ = typeCheckExprCompute ctxt valExpr 
        match ascs.[0] with
        | Some expectedTy ->
            ignore (checkType ctxt valExpr actualTy expectedTy)
        | None ->
            ()
    | ErrorField(msg,rng) ->
        if ctxt.trackErrors then
            addError
                !currentFileName
                msg
                rng
        ()

/// typechecks the field's value w.r.t. ctxt, adding any errors encountered to the error
/// list (if tracking errors). Unlike typeCheckField, this function is used when typechecking 
/// in check mode. It takes and additional argument expectedFieldMap, which contains the fields
/// of the table type we expect the constructor currently being checked to be a subtype of.
let typeCheckFieldCheck (ctxt : Context) (field : TypedConstructorField) (expectedFieldMap : Map<string,Field>) =
    match field with
    | ListField(annotation,expr, _) ->
        checkAnnotation ctxt annotation
        let descs, ascs, isConstList = Type.InterpretVarAnnotation annotation 1
        let actualTy,_,_ = typeCheckExprCompute ctxt expr
        match ascs.[0] with
        | Some expectedTy ->
            ignore (checkType ctxt expr actualTy expectedTy)
        | None ->
            ()

    | RecField(annotation,keyExpr, valExpr, _) ->
        checkAnnotation ctxt annotation
        ignore (typeCheckExprCompute ctxt keyExpr)
        let descs, ascs,isConstList = Type.InterpretVarAnnotation annotation 1
        let actualTy,_,_ = typeCheckExprCompute ctxt valExpr 
        match ascs.[0] with
        | Some expectedTy ->
            ignore (checkType ctxt valExpr actualTy expectedTy)
        | None ->
            ()
    | ErrorField(msg,rng) ->
        if ctxt.trackErrors then
            addError
                !currentFileName
                msg
                rng
        ()

let typeCheckAssignCheck (ctxt : Context) 
                         (lexprs : List<TypedExpr>) 
                         (rexprs : List<TypedExpr>) 
                         (expectedTypes : List<Option<Type>>) =
       
    let lexprRanges = List.map getExprRange lexprs
    let maxLExprIndexChecked = ref 0
    let getExpectedTy (i : int) =
        maxLExprIndexChecked := max !maxLExprIndexChecked i
        if expectedTypes.Length > i then
            expectedTypes.[i]
        else
            None

    let getLExprRng (i : int) (exTy : Option<Type>) =
        if lexprs.Length > i then
            match exTy with
            | Some(NewFieldTy) ->
                match lexprs.[i] with
                | BinOpExpr(OpInd,_,String(name,rng),_) ->
                    rng
                | _ ->
                    // this path will be executed when querying assignments in the globals module
                    getExprRange lexprs.[i]
            | _ ->
                getExprRange lexprs.[i]
        else
            // No query pos will fall in this range
            (-1,-1)

    let checkExpr (i : int) (expr : TypedExpr) =
        let expr = rexprs.[i]
        let exprTy,_,_ = typeCheckExprCompute ctxt.IsNotLeftExpr.DontTrackErrors.DontRecordStub expr
        match exprTy with
        // if this is the last rexpr then we have to check all elements of its return type tuple, rather than just the first one
        | TupleTy(elements,indefinite) when i = rexprs.Length-1 ->
            let checkElement (j : int) (ty : Type) =
                let exTy = getExpectedTy(i+j)

                if exTy.IsSome then
                    let exTy, ty =
                        match exTy.Value,ty with
                        | (TypeAppTy(applicand,args),TypeAbsTy(varNames,body)) ->
                            if varNames.Length <> args.Length then
                                if ctxt.trackErrors then
                                    addError
                                        !currentFileName
                                        ("expected " + varNames.Length.ToString() + " type arguments, but " + args.Length.ToString() + " were supplied.")
                                        (getExprRange expr)
                                UnknownTy, UnknownTy
                            else
                                Type.Apply (Type.Unfold ctxt.tenv applicand) args, Type.Apply ty args
                        | _ ->
                            exTy.Value,ty

                    let isSub,expl = Type.IsSubtypeOf ctxt.tenv ty exTy
                    if not isSub && ctxt.trackErrors then
                        addError
                            !currentFileName
                            ((orderStr (j+1)) + " return type '" + ty.ToString() + "' does not match expected type '" + exTy.ToString() + "'" + expl)
                            (getExprRange expr)

                match ctxt.queryPos with 
                | Some(pos) when inRange pos (getLExprRng (i+j) exTy) ->
                    refQueryResponse := Some {
                        desc = ""
                        ty = match exTy with 
                             | Some(NewFieldTy) | None ->  
                                Some(prepareType ctxt.tenv ty)
                             | Some(exTyVal) ->
                                Some(prepareType ctxt.tenv exTyVal)
                        loc = NoLocation
                        rng = getLExprRng (i+j) exTy
                    }
                | _ ->
                    ()
            
            let elements = 
                if indefinite then
                    sizeListTo elements (lexprs.Length-rexprs.Length+1) UnknownTy 
                else if elements.Length = 0 then
                    [NilTy]
                else
                    elements

            List.iteri checkElement elements
            ignore (typeCheckExprCompute ctxt expr)
        | _ ->
            let exTy = getExpectedTy(i)
            match exTy with
            | Some(NewFieldTy)
            | None ->
                match ctxt.queryPos with 
                | Some(pos) when inRange pos (getLExprRng i exTy) ->
                    refQueryResponse := Some {
                        desc = ""
                        ty = Some(prepareType ctxt.tenv exprTy)
                        loc = NoLocation
                        rng = getLExprRng i exTy
                    }
                | _ ->
                    ()
                ignore (typeCheckExprCompute ctxt expr)
            | Some(ty) ->
               ignore (typeCheckExprCheck ctxt expr ty)

    List.iteri checkExpr rexprs

    let addUnassignedError (i : int) =
        addError
            !currentFileName
            "variable left unassigned"
            (getExprRange lexprs.[i])

    List.iter addUnassignedError [!maxLExprIndexChecked+1 .. lexprs.Length-1]

let typeCheckSeqCheck (ctxt : Context) (exprs : List<TypedExpr>) (expectedTypes : List<Option<Type>>) =
    let getExpectedTy (i : int) =
        if expectedTypes.Length > i then
            expectedTypes.[i]
        else
            None

    let checkExpr (i : int) (expr : TypedExpr) =
        let expr = exprs.[i]
        let exprTy,_,_ = typeCheckExprCompute ctxt.IsNotLeftExpr.DontTrackErrors expr
        match exprTy with
        | TupleTy(elements,indefinite) when i = exprs.Length-1 ->
            let checkElement (j : int) (ty : Type) =
                let exTy = getExpectedTy(i+j)
                if exTy.IsSome then
                    let isSub,expl = Type.IsSubtypeOf ctxt.tenv ty exTy.Value
                    if not isSub && ctxt.trackErrors then
                        addError
                            !currentFileName
                            ((orderStr (j+1)) + " return type '" + ty.ToString() + "' does not match expected type '" + exTy.Value.ToString() + "'" + expl)
                            (getExprRange expr)
            List.iteri checkElement elements
            ignore (typeCheckExprCompute ctxt expr)
        | _ ->
            let exTy = getExpectedTy(i)
            if exTy.IsSome then
                ignore (typeCheckExprCheck ctxt expr exTy.Value)
            else 
                ignore (typeCheckExprCompute ctxt expr)
    
    List.iteri checkExpr exprs

/// ctxt - the context to reason under
/// funcTy - the type of the value being called
/// actuals - the actual arguments supplied to the call
/// actualTypes - the types of the actual arguments supplied to the call
/// rng - the range of the call, used in case typechecking errors are generated
///
/// If funcTy is an OverloadTy, return the first overload which has formals
/// which match the supplied actuals. If none match, return UnknownTy and generate
/// an error. 
///
/// If funcTy is a RecordTy which is callable, convert it into a FunctionTy and return that.
///
/// Otherwise, return funcTy as it was passed in.
let funcTyFromCall (ctxt : Context) 
                   (funcTy : Type) 
                   (actuals : List<TypedExpr>) 
                   (actualTypes : List<Type>)
                   (rng : Range) 
                   : Type = 

    match funcTy with
    | OverloadTy(overloads) ->
        let overloadMatches (ty : Type) =
            match ty with
            | FunctionTy(desc,formals,varPars,rets,varRets,isMethod,latent,loc) ->
                let formalsPad =
                    if varPars.IsSome then
                        ("","",UnknownTy)
                    else
                        ("","",NilTy)

                let maxlen = max formals.Length actuals.Length

                let actuals' = padList actuals (Nil rng) maxlen
                let formals' = padList formals formalsPad maxlen

                let actualMatchesFormal (actual : TypedExpr) (formalTy : Type) =
                    let actualTy,_,_ = typeCheckExprCompute ctxt.DontTrackErrors actual
                    fst (Type.IsSubtypeOf ctxt.tenv actualTy formalTy)

                let compatibleNum = varPars.IsSome || (actuals.Length <= formals.Length)
                let theyMatch = List.forall2 actualMatchesFormal actuals' (List.map (fun (_,_,z) -> z) formals')
                compatibleNum && theyMatch
            | _ ->
                false

        match List.tryFind overloadMatches overloads with
        | Some(overload) ->
            overload
        | None ->
            if ctxt.trackErrors then
                //a string containing all overloads, separated by newlines
                let overloadString = String.concat "\n" (List.map (fun x -> x.ToString()) overloads)
                addError
                    !currentFileName
                    ("argument types " + actualTypes.ToString() + " matches none of the overloads:\n" + overloadString)
                    rng
                             
            UnknownTy
    | CallableTy(desc,formals,varPars,rets,varRets,isMethod,latent,defLocation) ->
        FunctionTy(desc,formals,varPars,rets,varRets,isMethod,latent,defLocation)
    | _ ->
        funcTy

/// Generates an error if an expression in a list-style record constructor does not belong to an expected type
let checkListElement (ctxt : Context) (expectedTy : Type) (field : TypedConstructorField) =
    let fieldTy,valExpr,deps,rng = 
        match field with
        | ListField(_,v,rng) 
        | RecField(_,Number(_,_),v,rng) ->
            let ty,_,deps = typeCheckExprCompute ctxt.IsNotLeftExpr.DontTrackErrors v
            Type.Untuple ty, v, deps, rng
        | _ ->
            failwith "unreachable"

    ignore (checkType ctxt valExpr fieldTy expectedTy)

// see comment above typeCheckStat
refTypeCheckStat := fun (ctxt : Context) (stat : TypedStatement) ->
    let venv,tenv = ctxt.venv, ctxt.tenv
    match stat with
    | If(clauses,_,_) ->
        let checkClause (i : int) ((cond,body) : TypedExpr*TypedStatement) = 
            ignore (typeCheckExprCheck ctxt cond BoolTy)
            let condRefinements = getCondRefinementsForI ctxt clauses i
            let ctxt = Map.fold (fun ctxt path field -> applyAccumulation ctxt path field) ctxt condRefinements
            typeCheckStat ctxt body
            
        List.iteri checkClause clauses
    | While(cond,body,_) ->
        ignore (typeCheckExprCheck ctxt cond BoolTy)
        typeCheckStat ctxt body
    | Do(body,_) ->
        typeCheckStat ctxt body
    | ForNum(var,start,fin,step,body,_) ->
        match var with
        | NameExpr(name,rng) ->
            ignore (typeCheckExprCheck ctxt.IsNotLeftExpr start NumberTy)
            ignore (typeCheckExprCheck ctxt.IsNotLeftExpr fin NumberTy)
            ignore (typeCheckExprCheck ctxt.IsNotLeftExpr step NumberTy)
            let ctxt' = updateCtxtForBody ctxt stat
            typeCheckStat ctxt' body
        | _ ->
            failwith "unreachable"
    | ForGeneric([NameExpr(i,irng);NameExpr(v,vrng)],[CallExpr(NameExpr("ipairs",_),[arrayExpr],_)],body,_) ->
        let arrTy,field,_ = typeCheckExprCompute ctxt arrayExpr
        let ctxt' = updateCtxtForBody ctxt stat
        typeCheckStat ctxt' body
    | ForGeneric(vars,gens,body,_) ->
        let genTypes = List.map (typeCheckExprCompute ctxt) gens
        let genSeqTy = Type.SeqType (List.map fst3 genTypes)
        let start = fst (getExprRange gens.Head)
        let fin = snd (getExprRange gens.[gens.Length-1])

        // NOTE: this should mirror the code in updateCtxtForBody
        match genSeqTy with
        | [],true
        | (UnknownTy :: _),_ ->
            ()
        | _,true ->
            if ctxt.trackErrors then
                addError
                    !currentFileName
                    "for loop generator list should not have an undetermined number of values"
                    (start,fin)
        | tuple,_ when tuple.Length <> 3 ->
            if ctxt.trackErrors then
                addError
                    !currentFileName
                    ("for loop generator contains " + tuple.Length.ToString() + " values when 3 are expected")
                    (start,fin)
        | [CallableTy(_,pars,_,rets,_,_,_,_) as I;S;V1],false ->
            if rets.Length < vars.Length then
                if ctxt.trackErrors then
                    addError
                        !currentFileName
                        ("iterator type " + I.ToString() + " has " + rets.Length.ToString() + " returns, which does not match the number of loop variables " + vars.Length.ToString())
                        (start,fin)
            else
                let rets = sizeListTo rets (vars.Length) ("",UnknownTy)
                let vTypes = List.map snd rets
                let pTypes = List.map thrd3 pars
                let pTypes = if pTypes.Length = 1 then pTypes @ [NilTy] else pTypes

                if pTypes.Length <> 2 then
                    if ctxt.trackErrors then
                        addError
                            !currentFileName
                            ("iterators should take two arguments, but this one takes " + pTypes.Length.ToString())
                            (start,fin)
                else
                    if not (Type.IsNillable pTypes.[1]) then
                        if ctxt.trackErrors then
                            addError
                                !currentFileName
                                ("The second parameter of the iterator must be nillable so that it can accept sentinels. " + (pTypes.[0]).ToString() + " is not nillable.")
                                (start,fin)
                    else
                        match Type.IsSubtypeOf (ctxt.tenv) S (pTypes.[0]) with
                        | true, _ ->
                            match Type.IsSubtypeOf (ctxt.tenv) V1 (pTypes.[1]) with
                            | true,_ ->
                                match Type.IsSubtypeOf (ctxt.tenv) (vTypes.[0]) (pTypes.[1]) with
                                | true, _ ->
                                    ()
                                | false,msg ->
                                    if ctxt.trackErrors then
                                        addError
                                            !currentFileName
                                            ("the first return type of " + I.ToString() + " is not a subtype its second parameter type" + msg)
                                            (start,fin)
                            | false,msg ->
                                if ctxt.trackErrors then
                                    addError
                                        !currentFileName
                                        (V1.ToString() + " is is not a subtype of the second parameter type of the iterator " + (pTypes.[1]).ToString() + msg)
                                        (start,fin)
                        | false,msg ->
                            if ctxt.trackErrors then
                                addError
                                    !currentFileName
                                    (S.ToString() + " is not a subtype of the first parameter of the iterator " + (pTypes.[0]).ToString() + msg)
                                    (start,fin)
        | [I;_;_],false ->
            if ctxt.trackErrors then
                addError
                    !currentFileName
                    ("first return type of " + (I.ToString()) + " is not callable and therefore cannot be used as an iterator") 
                    (start,fin) 
        let ctxt' = updateCtxtForBody ctxt stat
        typeCheckStat ctxt' body
    | Repeat(cond,body,_) ->
        ignore (typeCheckExprCheck ctxt cond BoolTy)
        typeCheckStat ctxt body
    | LocalAssign(annotation,names,exprs,_) ->
        let descriptions,ascriptions,isConstList = 
            Type.InterpretVarAnnotation annotation names.Length
        
        checkAnnotation ctxt annotation
        
        typeCheckAssignCheck ctxt names exprs ascriptions
    | Assign(annotation,lvals, rvals,_) ->
        let descriptions,ascriptions,isConstList = 
            Type.InterpretVarAnnotation annotation lvals.Length 
        
        checkAnnotation ctxt annotation

        let ltypeFields = List.map (fun expr -> typeCheckExprCompute ctxt.IsLeftExpr expr) lvals
        let ltypes,lfields,ldeps = List.unzip3 ltypeFields
        let lranges = List.map getExprRange lvals
        
        let expectedRTypes = List.map Some (coverListOpt ltypes ascriptions)

        ignore (typeCheckAssignCheck (ctxt.IsNotLeftExpr) lvals rvals expectedRTypes)

        let check (i : int) =
            let ltype = ltypes.[i]
            let lrange = lranges.[i]
            let lfield = lfields.[i]
            let lval = lvals.[i]
            let ascriptionTy = ascriptions.[i]

            if lfield.IsSome && lfield.Value.isConst then
                if ctxt.trackErrors then
                    addError
                        !currentFileName
                        "cannot assign constants"
                        lrange    
            
            if lfield.IsSome && ascriptionTy.IsSome then          
                ignore (checkType ctxt lval ltype ascriptionTy.Value)

        List.iter check [0..ltypes.Length-1]
    | Return(rets,rng) ->
        let hasVarRets,varRng =
            if rets.Length > 0 then
                match rets.[rets.Length-1] with
                | VarArgs(rng)
                | CallExpr(_,_,rng) ->
                    true,rng
                | _ ->
                    false,(0,0)
            else
                false,(0,0)
            
        match ctxt.expectedReturnTypes with
        | None ->
            List.iter (fun expr -> ignore (typeCheckExprCompute ctxt expr)) rets
        | Some (tyList) when (tyList.Length < rets.Length) || (tyList.Length > rets.Length && not hasVarRets) ->
            if ctxt.trackErrors then
                addError
                    !currentFileName
                    ("expected " + tyList.Length.ToString() + " return values. Got " + rets.Length.ToString() + ".")
                    rng
        | Some (tyList) ->
            typeCheckSeqCheck ctxt rets (List.map Some tyList)
    | Break(_) ->
        ()
    | Call(callExp,_) ->
        ignore (typeCheckExprCompute ctxt.IsNotLeftExpr callExp)
    | Sequence(optAnn,stats,_) ->
        checkAnnotation ctxt optAnn
        let foldStat (ctxt : Context) (stat : TypedStatement) =
            typeCheckStat ctxt stat
            updateCtxt ctxt.DontRecordStub stat

        ignore (Seq.fold foldStat ctxt stats)
    | ErrorStatement(msg,rng) ->
        ()

let addModuleDependencyIfNecessary (ctxt : Context) (ty : Type) (deps : Set<string>) =
    if ty.HasName && ty.Name <> "string" then
        let results = ctxt.tenv.typeFiles.TryFind ty.Name
        match results with
        | Some(_,moduleName) ->
            deps.Add(moduleName)
        | None ->
            deps
    else
        deps

// typecheck the expression in compute mode
refTypeCheckExprCompute := fun (ctxt : Context) (expr : TypedExpr) ->
    let venv, tenv = ctxt.venv, ctxt.tenv
    match expr with
    | CallExpr(NameExpr("lucbdnioua",_),[],_) ->
        if ctxt.recordStub then
            refStubContext := ctxt
        UnknownTy,None,Set.empty
    | CallExpr(NameExpr("require",_), [String(moduleName,_)], rng) ->
        if tenv.consMap.ContainsKey moduleName then
            tenv.consMap.Item moduleName,None,Set.empty.Add(moduleName)
        else
            UnknownTy,None,Set.empty

    | BinOpExpr(OpOr, opA, opB, rng) when Type.IsNillable (fst3 (typeCheckExprCompute ctxt.DontTrackErrors opA)) ->
        let expected,_,depsA = typeCheckExprCompute ctxt opA
        let expected = expected.StripNillable()

        let actual,_,depsB = typeCheckExprCompute ctxt opB

        if checkType ctxt opB actual expected then
            expected, None, Set.union depsA depsB
        else
            UnknownTy, None, Set.empty
    | BinOpExpr(OpOr, BinOpExpr(OpAnd,opA,opB,_),opC,_) ->
        let tyA,_,depsA = typeCheckExprCompute ctxt opA
        let tyB,_,depsB = typeCheckExprCompute ctxt opB
        let tyC,_,depsC = typeCheckExprCompute ctxt opC

        if (checkType ctxt opA tyA BoolTy) && (checkType ctxt opB tyB tyC) && (checkType ctxt opC tyC tyB) then
            tyB, None, Set.unionMany [depsA;depsB;depsC]
        else
            UnknownTy, None, Set.unionMany [depsA;depsB;depsC]
    | BinOpExpr(OpEq, opA, opB, rng)
    | BinOpExpr(OpNe, opA, opB, rng) ->
        // Eq and Ne are not handled as other metamethods for the following reasons:
        // 1.) equality and non-equality can be computed for tables which do not define the eq metamethod
        // 2.) they allow the rhs to be a subtype or supertype of the lhs 
        let tyA,_,depsA = typeCheckExprCompute ctxt opA
        let tyB,_,depsB = typeCheckExprCompute ctxt opB

        let resA, explA = Type.IsSubtypeOf ctxt.tenv tyA tyB
        let resB, explB = Type.IsSubtypeOf ctxt.tenv tyB tyA

        if resA || resB then
            BoolTy, None, Set.union depsA depsB
        else
            addError 
                !currentFileName
                ("lhs type is not a subtype or supertype of rhs type")
                rng
            UnknownTy,None,Set.union depsA depsB
    | Number(_,rng) ->
        NumberTy,None,Set.empty
    | String(_,rng) ->
        Type.Unfold tenv (UserDefinedTy("string",(0,0))),None,Set.empty       
    | Nil(rng) ->
        NilTy,None,Set.empty
    | True(rng) ->
        BoolTy,None,Set.empty
    | False(rng) ->
        BoolTy,None,Set.empty
    | VarArgs(rng) ->
        TupleTy([],true),None,Set.empty
    | Constructor(fields,rng) ->
        List.iter (fun x -> (typeCheckField ctxt x)) fields

        if fields.Length > 0 && List.forall TypedConstructorField.IsStructural fields then
            let fieldList = List.map (getLabelAndField ctxt) fields
            RecordTy(
                "*UnnamedRecord*", 
                "",
                true,
                true,
                Closed,
                MetamethodSet.empty, 
                new Map<string,Field>(fieldList),
                fieldList,
                new Map<string,Method>([]), 
                [],
                (!currentFileName,rng)
            ),
            None,
            Set.unionMany (List.map (fun ((l,f) : string*Field) -> f.dependencies) fieldList)
        elif fields.Length > 0 && List.forall TypedConstructorField.IsListLike fields then 
            let firstFieldValueType,deps = 
                match fields.[0] with
                | ListField(ann,v,_) 
                | RecField(ann,Number(_,_),v,_) ->
                    checkAnnotation ctxt ann
                    let ty,_,deps = typeCheckExprCompute ctxt.IsNotLeftExpr.DontTrackErrors v
                    Type.Untuple ty,deps
                | _ ->
                    failwith "unreachable"

            List.iter (checkListElement ctxt firstFieldValueType) fields

            let metaset = MetamethodSet.ForArrayTy(firstFieldValueType)

            RecordTy(
                "*UnnamedRecord*",
                "",
                true,
                true,
                Closed,
                metaset,
                Map.empty,
                [],
                Map.empty,
                [],
                (!currentFileName, rng)
            ),
            None,
            deps
        else
            List.iter (typeCheckField ctxt.DontTrackErrors) fields
            UnknownTy, None, Set.empty

    | Function(ann,selfName,latent,formals,varPars,rets,varRets,body,rng) ->
        checkFunctionAnnotation ctxt ann expr
        let desc = if Option.isSome ann then fst4 ann.Value else ""
        let ty = FunctionTy(desc,formals,varPars,rets,varRets,false,latent,(!currentFileName,rng))

        let foldFormal (c : Context) (nm,desc,ty) = 
            let newField = {
                desc = desc
                ty = Type.TypeEval c.tenv ty
                dependencies = Set.empty
                loc = (!currentFileName,rng)
                isVisible = true
                isConst = false
            }

            c.AddValue(nm,newField)

        let funcField = {
            desc = desc
            ty = ty
            dependencies = Set.empty
            loc = (!currentFileName,rng)
            isVisible = true
            isConst = false
        }

        let ctxt = List.fold foldFormal ctxt formals 
        let ctxt =
            match selfName with
            | Some name ->
                ctxt.AddValue(name, funcField)
            | _ ->
                ctxt

        let ctxt = if rets.Length > 0 then ctxt.ExpectReturnTypes (List.map snd rets) else ctxt
        
        if ctxt.MustEnterFunctionBodies then
            typeCheckStat { ctxt.UndoAllAccumulations with inLatent = latent } body
        
        ty,None,Set.empty
    | UnOpExpr(unop,operand,rng) ->
        let operandTy,_,deps = typeCheckExprCompute ctxt operand
        let operandTy = Type.Coerce tenv operandTy 
        
        let deps = addModuleDependencyIfNecessary ctxt operandTy deps

        match operandTy with
        | UnknownTy ->
            UnknownTy,None,Set.empty
        | _ ->
            let metaset = getMetamethodSet ctxt.tenv operandTy
            let operatorTy = getUnOp metaset unop

            match operatorTy with
            | Some(desc,ty,_) ->
                ty,None,deps
            | None ->
                addError 
                    !currentFileName
                    (operandTy.ToString() + " does not define operator " + unop.ToString())
                    rng
                UnknownTy,None,Set.empty

    | BinOpExpr(binop, opA, opB, rng) ->
        let tyA,_,depsA = typeCheckExprCompute ctxt opA
        let tyA = Type.Coerce ctxt.tenv tyA

        let tyB,_,depsB = typeCheckExprCompute ctxt opB
        let tyB = Type.Coerce tenv tyB

        let deps = addModuleDependencyIfNecessary ctxt tyA (Set.union depsA depsB)

        match (Type.Coerce tenv tyA,binop,opB) with
        | (UnknownTy,OpInd,_)
        | (UnknownTy,OpMethInd,_) ->
            UnknownTy,None,Set.empty
        | (UnknownTy,_,_) ->
            ignore (typeCheckExprCompute ctxt opB)
            UnknownTy,None,Set.empty
        | (ArrayTy(retTy),OpInd,opB) ->
            if fst (Type.IsSubtypeOf ctxt.tenv tyB NumberTy) then
                retTy,None,Set.empty
            else
                if ctxt.trackErrors then
                    addError
                        !currentFileName
                        "non-numeric keys cannot be used to index sequences"
                        rng
                UnknownTy,None,Set.empty
        | (RecordTy(_,_,_,_,_,_,fields,_,_,_,_),OpSelect,String(str,srng)) when fields.ContainsKey str ->
            let field = fields.Item str
            if ctxt.queryPos.IsSome && inRange ctxt.queryPos.Value srng then
                refQueryResponse :=  Some {
                    desc = field.desc
                    loc = field.loc
                    ty = Some(prepareType ctxt.tenv field.ty)
                    rng = rng
                }

            field.ty,Some(field),deps
        | (RecordTy(name,_,_,_,Closed,_,_,_,_,_,_),OpSelect,String(str,_)) ->
            if ctxt.trackErrors then
                addError
                    !currentFileName
                    (tyA.ToString() + " does not have a field called " + str)
                    rng
            UnknownTy,None,Set.empty
        | (RecordTy(_,_,_,_,_,_,_,_,methods,_,_),OpMethInd, String(str,mrng)) when methods.ContainsKey str ->
            let meth = methods.Item str
            if ctxt.queryPos.IsSome && inRange ctxt.queryPos.Value mrng then
                refQueryResponse :=  Some {
                    desc = meth.desc
                    loc = meth.loc
                    ty = Some(prepareType ctxt.tenv meth.ty)
                    rng = rng
                }
            meth.ty,None,deps
        | (RecordTy(name,_,_,_,Closed,_,_,_,_,_,_),OpMethInd, String(str,_)) ->
            if ctxt.trackErrors then
                addError
                    !currentFileName
                    (tyA.ToString() + " does not have a method called " + str)
                    rng
            UnknownTy,None,Set.empty
        | (RecordTy(name,_,_,_,Open,_,_,_,methods,_,_),OpSelect, String(str,_)) ->
            if methods.ContainsKey str && ctxt.isLeftExpr then
                addError
                    !currentFileName
                    (tyA.ToString() + " already contains a method called " + str + ". Cannot add a field of the same name.")
                    rng
                UnknownTy,None,Set.empty
            elif ctxt.isLeftExpr then
                NewFieldTy,None,deps
            else
                if ctxt.trackErrors then
                    addError
                        !currentFileName
                        (tyA.ToString() + " does not have a field called " + str)
                        rng
                UnknownTy,None,Set.empty
        | _ ->
            let metaset = getMetamethodSet ctxt.tenv tyA
            let opTy = getBinOp metaset binop
            match (opTy) with
            | (None) ->
                if ctxt.trackErrors then
                    addError
                        !currentFileName
                        (tyA.ToString() + " does not define a " + binop.ToString() + " operator")
                        rng
                UnknownTy,None,Set.empty
            | Some (desc,expectedRhsTy, retTy,_) ->
                match tyB with
                | UnknownTy ->
                    UnknownTy, None, Set.empty
                | _ ->
                    ignore (checkType ctxt opB tyB expectedRhsTy)
                    retTy, None, deps
    | ParenthesizedExpr(expr,rng) ->
        let ty,_,deps = typeCheckExprCompute ctxt.IsNotLeftExpr expr
        Type.Untuple ty, None, deps
    | NameExpr(name, rng) ->
        if ctxt.venv.ContainsKey name then
            let field = ctxt.venv.Item name
            if ctxt.queryPos.IsSome && inRange ctxt.queryPos.Value rng then
                refQueryResponse :=  Some {
                    desc = field.desc
                    loc = field.loc
                    ty = Some(prepareType tenv field.ty)
                    rng = rng
                }

            field.ty, Some(field), field.dependencies
        elif ctxt.addGlobals && ctxt.isLeftExpr then
            NewFieldTy, None, Set.empty
        else
            if ctxt.trackErrors then
                addError
                    (!currentFileName)
                    "variable not in context"
                    rng
            UnknownTy, None, Set.empty  
    | CallExpr(func,actuals,rng) ->
        let actualTypes,_,actualDepSets = List.unzip3 (List.map (fun expr -> typeCheckExprCompute ctxt.IsNotLeftExpr.DontTrackErrors expr) actuals)
        let actualDeps = Set.unionMany actualDepSets
        let funcTy0,optField,funcDeps = typeCheckExprCompute ctxt.IsNotLeftExpr func
        let funcTy = funcTyFromCall ctxt funcTy0 actuals actualTypes rng
        let funcTy = Type.Coerce tenv funcTy        
        let deps = addModuleDependencyIfNecessary ctxt funcTy (Set.union actualDeps funcDeps)
        /// the position following the last position in text of the call expression
        let callFin = snd rng
        let (funcStart,funcFin) = getExprRange func

        match funcTy with
        | CallableTy(_,_,_,_,_,_,isLatent,loc) when isLatent && not ctxt.inLatent ->
            if ctxt.trackErrors then
                addError
                    !currentFileName
                    "cannot call latent functions from non-latent functions"
                    rng
            UnknownTy, None, Set.empty
        | CallableTy(desc,formals,varPars,rets,varRets,isMethod,isLatent,loc) ->
            /// replace query to function with information from correct overload if necessary
            match !refQueryResponse with
            | Some(response) when response.rng = (funcStart,funcFin) ->
                match response.ty with
                | Some(OverloadTy(_)) 
                | Some(CallableTy(_,_,_,_,_,_,_,_)) ->
                    // if the 
                    refQueryResponse := Some {
                        desc = if desc <> "" then desc else response.desc
                        ty = Some(prepareType ctxt.tenv funcTy)
                        loc = response.loc
                        rng = (funcStart,funcFin)
                    } 
                | _ ->
                    ()
            | None | Some(_) ->
                ()

            let formalTypes = List.map thrd3 formals
            
            /// The number of actuals before the first actual which could evaluate to nil
            let nonNilActuals = 
                let ind = (List.tryFindIndex (Type.Nillish ctxt.tenv) actualTypes)
                if ind.IsSome then ind.Value else actualTypes.Length
            
            /// the number of optional trailing formal arguments (if no formal varpars)
            //  equal to the index of the the first non-nillable formal in the reversed formal list
            let numOptionals = 
                let ind = List.tryFindIndex (not << (Type.Nillish ctxt.tenv)) (List.rev formalTypes)
                if ind.IsSome then ind.Value else formalTypes.Length

            /// The minimum number of non-nil actual arguments that can be passed to values have type funcTy without
            /// generating a "to-few-arguments" error
            let minActuals = formals.Length - numOptionals
            
            let ctxt = {ctxt with closestEnclosingFunctionCall = Some(optField,getExprRange func,funcTy0)}

            if (actuals.Length < minActuals) || (nonNilActuals > formals.Length && varPars.IsNone) then
                if minActuals = formals.Length then
                    if ctxt.trackErrors then
                        addError
                            !currentFileName
                            ("received " + actuals.Length.ToString() + " arguments. Expected " + formals.Length.ToString() + ".")
                            (funcStart,callFin)
                else
                    if ctxt.trackErrors then
                        addError
                            !currentFileName
                            ("received " + actuals.Length.ToString() + " arguments. Expected between " + minActuals.ToString() + " and " + formals.Length.ToString() + ".")
                            (funcStart,callFin)
                
                //we still must compute types of all actuals because a stub could be located in these actuals
                List.iter (fun (actual) -> ignore( (typeCheckExprCompute ctxt actual) )) actuals
            else
                let maxlen = max (formals.Length) (actuals.Length)
                // if we need to do any padding here, varPars.IsSome must be true, due to the if condition 
                let formals = padList formals ("","",UnknownTy) maxlen
                let actuals = padList actuals (Nil rng) maxlen
                let actualTypes = padList actualTypes NilTy maxlen
                let triples = List.zip3 actuals actualTypes formals
                let checkTriple ((actual,_,(_,_,formalTy)) : TypedExpr*Type*(string*string*Type)) =
                    ignore (typeCheckExprCheck ctxt actual formalTy)
                List.iter checkTriple triples

            if rets.Length > 0 then
                match snd rets.[0] with
                | ErrorTy(msg) ->
                    if ctxt.trackErrors then
                        addError
                            !currentFileName
                            msg
                            (funcStart,callFin)
                    UnknownTy, None, deps
                | _ ->
                    TupleTy(List.map snd rets,varRets.IsSome), None, deps
            elif varRets.IsSome then
                TupleTy([],true),None,deps
            else
                TupleTy([],false),None,deps
        | UnknownTy ->
            let ctxt = {ctxt with closestEnclosingFunctionCall = Some(optField,getExprRange func,funcTy0)}
            ignore (List.map (fun expr -> typeCheckExprCompute ctxt.IsNotLeftExpr expr) actuals)
            TupleTy([],true), None, deps
        | _ ->
            let (funcStart,funcFin) = getExprRange func
            let (_,callFin) = rng
            if ctxt.trackErrors then
                addError
                    !currentFileName
                    ("instances of type " + funcTy.ToString() + " are uncallable")
                    (funcStart,callFin)  
            UnknownTy, None, deps
    | Ascription(expr,ty,rng) ->
        let exprTy,field,deps = typeCheckExprCompute ctxt expr
        ignore (checkType ctxt expr exprTy ty)
        ty, field, deps
    | ErrorExpr(msg,rng) ->
        UnknownTy, None, Set.empty

refTypeCheckExprCheck := fun (ctxt : Context) (expr : TypedExpr) (expectedTy : Type) ->
    let venv, tenv = ctxt.venv, ctxt.tenv
    match expr with
    | CallExpr(NameExpr("lucbdnioua",_),[],_) ->
        if ctxt.recordStub then
            refStubContext := ctxt
        None,Set.empty
    | CallExpr(NameExpr("require",_), [String(moduleName,_)], rng) ->
        if tenv.consMap.ContainsKey moduleName then
            let actualTy = tenv.consMap.Item moduleName
            ignore (checkType ctxt expr actualTy expectedTy)
            None,Set.empty.Add(moduleName)
        else
            None,Set.empty

    | BinOpExpr(OpOr, opA, opB, rng) when Type.IsNillable (fst3 (typeCheckExprCompute ctxt.DontTrackErrors opA)) ->
        let expected,_,depsA = typeCheckExprCompute ctxt opA
        let expectedB = expected.StripNillable()
        let _,depsB = typeCheckExprCheck ctxt opB expectedB

        ignore (checkType ctxt expr expectedB expectedTy)

        None, Set.union depsA depsB
    | BinOpExpr(OpOr, BinOpExpr(OpAnd,opA,opB,_),opC,_) ->
        let _,depsA = typeCheckExprCheck ctxt opA BoolTy
        let _,depsB = typeCheckExprCheck ctxt opB expectedTy
        let _,depsC = typeCheckExprCheck ctxt opC expectedTy

        None, Set.unionMany [depsA;depsB;depsC]
    | BinOpExpr(OpEq, opA, opB, rng)
    | BinOpExpr(OpNe, opA, opB, rng) ->
        // Eq and Ne are not handled as other metamethods for the following reasons:
        // 1.) equality and non-equality can be computed for tables which do not define the eq metamethod
        // 2.) they allow the rhs to be a subtype or supertype of the lhs 
        let tyA,_,depsA = typeCheckExprCompute ctxt opA
        let tyB,_,depsB = typeCheckExprCompute ctxt opB

        let resA, explA = Type.IsSubtypeOf ctxt.tenv tyA tyB
        let resB, explB = Type.IsSubtypeOf ctxt.tenv tyB tyA

        if resA || resB then
            ignore (checkType ctxt expr BoolTy expectedTy)
            None, Set.union depsA depsB
        else
            addError 
                !currentFileName
                ("lhs type is not a subtype or supertype of rhs type")
                rng
            None,Set.union depsA depsB
    | Number(_,rng) ->
        ignore (checkType ctxt expr NumberTy expectedTy)
        None,Set.empty
    | String(_,rng) ->
        //let actualTy = Type.Unfold tenv (UserDefinedTy("string",(0,0)))
        ignore (checkType ctxt expr (UserDefinedTy("string",(0,0))) expectedTy)
        None,Set.empty       
    | Nil(rng) ->
        ignore (checkType ctxt expr NilTy expectedTy)
        None,Set.empty
    | True(rng) ->
        ignore (checkType ctxt expr BoolTy expectedTy)
        None,Set.empty
    | False(rng) ->
        ignore (checkType ctxt expr BoolTy expectedTy)
        None,Set.empty
    | VarArgs(rng) ->
        let actualTy = TupleTy([],true)
        ignore (checkType ctxt expr actualTy expectedTy)
        None,Set.empty
    | Constructor(consFields,consRng) when consFields.Length > 0 && List.forall TypedConstructorField.IsListLike consFields ->
        let coercedExpectedTy = Type.Coerce2 ctxt.tenv expectedTy
        let ctxt = 
            match coercedExpectedTy with
            | RecordTy(_,_,true,_,_,_,_,_,_,_,_) ->
                { ctxt with closestEnclosingExpectedRecord = Some(coercedExpectedTy) }
            | _ ->
                ctxt
        match coercedExpectedTy with
        | ArrayTy(elemTy) ->
            let foldField accumulatedDeps field =
                let optAnnTy,expr,valExprRng = 
                    match field with
                    | ListField(optAnn, valExpr, rng) ->
                        checkAnnotation ctxt optAnn
                        let ty = Type.TypeFromAnnotation optAnn
                        ty,valExpr,getExprRange valExpr
                    | _ -> 
                        failwith "unreachable"
                
                let elemDeps =
                    match optAnnTy with
                    | Some(annTy) when checkType ctxt expr annTy elemTy ->
                        let _,deps = typeCheckExprCheck ctxt expr annTy
                        deps
                    | Some(annTy) ->
                        addError
                            !currentFileName
                            ("Expected " + elemTy.ToString() + ". Got " + annTy.ToString() + ".")
                            valExprRng
                        Set.empty
                    | None ->
                        let _,deps = typeCheckExprCheck ctxt expr elemTy
                        deps
                    
                Set.union accumulatedDeps elemDeps

            None, List.fold foldField Set.empty consFields
        | _ ->
            let exprTy,_,deps = typeCheckExprCompute ctxt expr
            ignore (checkType ctxt expr exprTy expectedTy)
            None, deps
    | Constructor(consFields,consRng) when consFields.Length > 0 && List.forall TypedConstructorField.IsRecordField consFields ->
        let coercedExpectedTy = Type.Coerce2 ctxt.tenv expectedTy
        let ctxt = 
            match coercedExpectedTy with
            | RecordTy(_,_,true,_,_,_,_,_,_,_,_) ->
                { ctxt with closestEnclosingExpectedRecord = Some(coercedExpectedTy) }
            | _ ->
                ctxt
        match coercedExpectedTy with
        | MapTy(keyTy,valTy) ->
            let foldField accumulatedDeps field =
                let keyExpr,valExpr,keyExprRng,valExprRng = 
                    match field with
                    | RecField(optAnn, keyExpr, valExpr, rng) ->
                        checkAnnotation ctxt optAnn
                        let ty = Type.TypeFromAnnotation optAnn
                        keyExpr,valExpr,getExprRange keyExpr, getExprRange valExpr
                    | _ -> 
                        failwith "unreachable"
                
                let elemDeps =
                    let _, keyDeps = typeCheckExprCheck ctxt keyExpr keyTy
                    let _, valDeps =  typeCheckExprCheck ctxt valExpr valTy
                    Set.union keyDeps valDeps
                                            
                Set.union accumulatedDeps elemDeps

            None, List.fold foldField Set.empty consFields
        | RecordTy(name,desc,true,_,_,x,expectedFieldMap,_,y,_,_) as coercedExpectedTy when x = MetamethodSet.standard && y = Map.empty ->

            let consLabelSet = Set.ofList (List.map (TypedConstructorField.Label) consFields)

            let checkField (ctxt : Context) (field : TypedConstructorField) : Set<string> =
                let optAnnTy,label,valExpr,rng = 
                    match field with
                    | RecField(optAnn, String(label,_), valExpr, rng) ->
                        checkAnnotation ctxt optAnn
                        let ty = Type.TypeFromAnnotation optAnn
                        ty,label,valExpr,rng
                    | _ -> 
                        failwith "unreachable"
                
                match expectedFieldMap.TryFind label with
                | Some(expectedField) ->
                    match optAnnTy with
                    | Some(annTy) ->
                        if (checkType ctxt valExpr annTy (expectedField.ty)) then
                            let field,deps = typeCheckExprCheck ctxt valExpr annTy
                            deps                                
                        else
                            Set.empty
                    | None ->
                        let field,deps = typeCheckExprCheck ctxt valExpr (expectedField.ty)
                        deps
                | None ->
                    let _,_,deps = typeCheckExprCompute ctxt valExpr
                    deps

            let isMissing (label : string, field : Field) =
                ( not (consLabelSet.Contains label) ) && ( not (Type.Nillish ctxt.tenv field.ty) )

            //TODO: generate errors for extraneous fields
            match Array.tryFind isMissing (Map.toArray expectedFieldMap) with
            | Some(label,field) ->
                addError
                    !currentFileName
                    ("Constructor missing field " + label + " of type " + field.ty.ToString())
                    consRng
                ignore (List.map (checkField ctxt.DontTrackErrors) consFields)
                None,Set.empty
            | None ->
                match List.tryFind (fun (field : TypedConstructorField) -> expectedFieldMap.TryFind(TypedConstructorField.Label field).IsNone) consFields with
                | Some(TypedConstructorField.RecField(_,_,_,rng) as field) ->
                    addError
                        !currentFileName
                        ("Constructor has extraneous field " + (TypedConstructorField.Label field))
                        rng
                    ignore (List.map (checkField ctxt.DontTrackErrors) consFields)
                    None,Set.empty
                | None ->
                    None,Set.unionMany (List.map (checkField ctxt) consFields)
                | _ ->
                    failwith "unreachable"
        | _ ->
            let exprTy,_,deps = typeCheckExprCompute ctxt expr
            ignore (checkType ctxt expr exprTy expectedTy)
            None, deps                      
    | Constructor(fields,rng) ->
        let coercedExpectedTy = Type.Coerce2 ctxt.tenv expectedTy
        let ctxt = 
            match coercedExpectedTy with
            |RecordTy(_,_,true,_,_,_,_,_,_,_,_) ->
                { ctxt with closestEnclosingExpectedRecord = Some(coercedExpectedTy) }
            | _ ->
                ctxt
        List.iter (fun x -> (typeCheckField ctxt x)) fields
        None, Set.empty
    | Function(ann,selfName,latent,formals,varPars,rets,varRets,body,exprRng) ->
        match Type.Coerce2 ctxt.tenv expectedTy with
        | CallableTy(expectedDesc, expectedFormals, expectedVarPars, expectedRets, expectedVarRets, false, expectedLatent, _)  ->
            checkFunctionAnnotation ctxt ann expr
            let desc = if ann.IsSome then fst4 ann.Value else ""
            //note that function constructors inherit expected latency status as well as formal parameters and 
            //return types. We need replace constructor's latent status with the expected latent status 
            //so that the subsequent typecheck does not generate an error due to a mismatch in latency status.
            let ty = FunctionTy(desc,formals,varPars,rets,varRets,false,expectedLatent,(!currentFileName,exprRng))
            ignore (checkType ctxt expr ty expectedTy)

            let foldFormal (c : Context) ((nm,desc,ty),(expectedNm,expectedDesc,expectedTy)) = 
                let ty = Type.TypeEval c.tenv ty
                let ty = 
                    match ty with
                    | UnknownTy ->
                        expectedTy
                    | _ ->
                        ty

                let desc =
                    match desc with
                    | "" ->
                        expectedDesc
                    | _ ->
                        desc

                let newField = {
                    desc = desc
                    ty = ty
                    dependencies = Set.empty
                    loc = (!currentFileName,exprRng)
                    isVisible = true
                    isConst = false
                }

                c.AddValue(nm,newField)

            let funcField = {
                desc = desc
                ty = expectedTy
                dependencies = Set.empty
                loc = (!currentFileName,exprRng)
                isVisible = true
                isConst = false
            }

            let formals, expectedFormals =
                if formals.Length > expectedFormals.Length then
                    if expectedVarPars.IsSome then
                        formals, sizeListTo expectedFormals (formals.Length) ("","",UnknownTy)
                    else
                        formals, sizeListTo expectedFormals (formals.Length) ("","",NilTy)
                elif formals.Length < expectedFormals.Length then
                    if varPars.IsSome then
                        sizeListTo formals (expectedFormals.Length) ("","",UnknownTy), expectedFormals
                    else
                        sizeListTo formals (expectedFormals.Length) ("","",NilTy), expectedFormals
                else
                    formals, expectedFormals
           

            let ctxt = List.fold foldFormal ctxt (List.zip formals expectedFormals) 
            let ctxt =
                match selfName with
                | Some name ->
                    ctxt.AddValue(name, funcField)
                | _ ->
                    ctxt

            let rets, expectedRets =
                if rets.Length > expectedRets.Length then
                    if expectedVarRets.IsSome then
                        rets, sizeListTo expectedRets (rets.Length) ("",UnknownTy)
                    else
                        rets, sizeListTo expectedRets (rets.Length) ("",NilTy)
                elif rets.Length < expectedRets.Length then
                    if varRets.IsSome then
                        sizeListTo rets (expectedRets.Length) ("",UnknownTy), expectedRets
                    else
                        sizeListTo rets (expectedRets.Length) ("",NilTy), expectedRets
                else
                    rets, expectedRets
            
            let chooseRet ((_,actualRetTy),(_,expectedRetTy)) =
                match actualRetTy with
                | UnknownTy ->
                    expectedRetTy
                | _ ->
                    actualRetTy

            let zippedRets = List.zip rets expectedRets
            let ctxt = ctxt.ExpectReturnTypes (List.map chooseRet zippedRets)

            if ctxt.MustEnterFunctionBodies then
                typeCheckStat { ctxt.UndoAllAccumulations with inLatent = latent || expectedLatent } body
        
            None,Set.empty
        | _ ->
            let exprTy,_,deps = typeCheckExprCompute ctxt expr
            ignore (checkType ctxt expr exprTy expectedTy)
            None, deps
    | UnOpExpr(unop,operand,rng) ->
        let operandTy,_,deps = typeCheckExprCompute ctxt operand
        let operandTy = Type.Coerce tenv operandTy 
        
        let deps = addModuleDependencyIfNecessary ctxt operandTy deps

        match operandTy with
        | UnknownTy ->
            None,Set.empty
        | _ ->
            let metaset = getMetamethodSet ctxt.tenv operandTy
            let operatorTy = getUnOp metaset unop

            match operatorTy with
            | Some(desc,ty,_) ->
                ignore (checkType ctxt expr ty expectedTy) 
                None,deps
            | None ->
                addError 
                    !currentFileName
                    (operandTy.ToString() + " does not define operator " + unop.ToString())
                    rng
                None,Set.empty

    | BinOpExpr(binop, opA, opB, rng) ->
        let tyA,_,depsA = typeCheckExprCompute ctxt opA
        let tyA = Type.Coerce ctxt.tenv tyA

        let deps = addModuleDependencyIfNecessary ctxt tyA depsA

        match (Type.Coerce tenv tyA,binop,opB) with
        | (UnknownTy,OpInd,_)
        | (UnknownTy,OpSelect,_)
        | (UnknownTy,OpMethInd,_) ->
            None,Set.empty
        | (UnknownTy,_,_) ->
            ignore (typeCheckExprCompute ctxt opB)
            None,Set.empty
        | (ArrayTy(retTy),OpInd,opB) ->
            let _,depsB = typeCheckExprCheck ctxt opB NumberTy
            ignore (checkType ctxt expr retTy expectedTy)
            None,Set.empty
        | (RecordTy(_,_,_,_,_,_,fields,_,_,_,_),OpSelect,String(str,srng)) when fields.ContainsKey str ->
            let field = fields.Item str
            if ctxt.queryPos.IsSome && inRange ctxt.queryPos.Value srng then
                refQueryResponse :=  Some {
                    desc = field.desc
                    loc = field.loc
                    ty = Some(prepareType ctxt.tenv field.ty)
                    rng = rng
                }
            
            ignore (checkType ctxt expr field.ty expectedTy) 
            Some(field),deps
        | (RecordTy(name,_,_,_,Closed,_,_,_,_,_,_),OpSelect,String(str,_)) ->
            if ctxt.trackErrors then
                addError
                    !currentFileName
                    (tyA.ToString() + " does not have a field called " + str)
                    rng
            None,Set.empty
        | (RecordTy(_,_,_,_,_,_,_,_,methods,_,_),OpMethInd, String(str,mrng)) when methods.ContainsKey str ->
            let meth = methods.Item str
            if ctxt.queryPos.IsSome && inRange ctxt.queryPos.Value mrng then
                refQueryResponse :=  Some {
                    desc = meth.desc
                    loc = meth.loc
                    ty = Some(prepareType ctxt.tenv meth.ty)
                    rng = rng
                }

            ignore (checkType ctxt expr meth.ty expectedTy)
            None,deps
        | (RecordTy(name,_,_,_,Closed,_,_,_,_,_,_),OpMethInd, String(str,_)) ->
            if ctxt.trackErrors then
                addError
                    !currentFileName
                    (tyA.ToString() + " does not have a method called " + str)
                    rng
            None,Set.empty
        | (RecordTy(name,_,_,_,Open,_,_,_,methods,_,_),OpSelect, String(str,_)) ->
            if methods.ContainsKey str && ctxt.isLeftExpr then
                addError
                    !currentFileName
                    (tyA.ToString() + " already contains a method called " + str + ". Cannot add a field of the same name.")
                    rng
                None,Set.empty
            elif ctxt.isLeftExpr then
                None,deps
            else
                if ctxt.trackErrors then
                    addError
                        !currentFileName
                        (tyA.ToString() + " does not have a field called " + str)
                        rng
                None,Set.empty
        | _ ->
            let metaset = getMetamethodSet ctxt.tenv tyA
            let opTy = getBinOp metaset binop
            match (opTy) with
            | (None) ->
                if ctxt.trackErrors then
                    addError
                        !currentFileName
                        (tyA.ToString() + " does not define a " + binop.ToString() + " operator")
                        rng
                None,Set.empty
            | Some (desc,expectedRhsTy, retTy,_) ->
                let _,depsB = typeCheckExprCheck ctxt opB expectedRhsTy
                None, Set.union deps depsB
    | ParenthesizedExpr(expr,rng) ->
        let _,deps = typeCheckExprCheck ctxt.IsNotLeftExpr expr expectedTy
        None, deps
    | NameExpr(name, rng) ->
        if ctxt.venv.ContainsKey name then
            let field = ctxt.venv.Item name
            if ctxt.queryPos.IsSome && inRange ctxt.queryPos.Value rng then
                refQueryResponse :=  Some {
                    desc = field.desc
                    loc = field.loc
                    ty = Some(prepareType ctxt.tenv field.ty)
                    rng = rng
                }

            ignore (checkType ctxt expr field.ty expectedTy)

            Some(field), field.dependencies
        elif ctxt.addGlobals && ctxt.isLeftExpr then
            None, Set.empty
        else
            if ctxt.trackErrors then
                addError
                    (!currentFileName)
                    "variable not in context"
                    rng
            None, Set.empty  
    | CallExpr(func,actuals,rng) ->
        let actualTypes,_,actualDepSets = List.unzip3 (List.map (fun expr -> typeCheckExprCompute ctxt.IsNotLeftExpr.DontTrackErrors expr) actuals)
        let actualDeps = Set.unionMany actualDepSets
        let funcTy0,optField,funcDeps = typeCheckExprCompute ctxt.IsNotLeftExpr func
        let funcTy = funcTyFromCall ctxt funcTy0 actuals actualTypes rng
        let funcTy = Type.Coerce tenv funcTy               
        let (_,callFin) = rng
        let (funcStart,funcFin) = getExprRange func
        let deps = Set.unionMany (funcDeps :: actualDepSets)

        match funcTy with
        | CallableTy(_,_,_,_,_,_,isLatent,loc) when isLatent && not ctxt.inLatent ->
            if ctxt.trackErrors then
                addError
                    !currentFileName
                    "cannot call latent functions from non-latent functions"
                    rng
            None, deps
        | CallableTy(_,formals,varPars,rets,varRets,isMethod,isLatent,loc) ->
            let formalTypes = List.map thrd3 formals
            
            /// The number of actuals before the first actual which could evaluate to nil
            let nonNilActuals = 
                let ind = (List.tryFindIndex (Type.Nillish ctxt.tenv) actualTypes)
                if ind.IsSome then ind.Value else actualTypes.Length
            
            /// the number of optional trailing formal arguments (if no formal varpars)
            //  equal to the index of the the first non-nillable formal in the reversed formal list
            let numOptionals = 
                let ind = List.tryFindIndex (not << (Type.Nillish ctxt.tenv)) (List.rev formalTypes)
                if ind.IsSome then ind.Value else formalTypes.Length

            /// The minimum number of non-nil actual arguments that can be passed to values have type funcTy without
            /// generating a "to-few-arguments" error
            let minActuals = formals.Length - numOptionals
            
            let ctxt = {ctxt with closestEnclosingFunctionCall = Some(optField,getExprRange func,funcTy0)}

            if (actuals.Length < minActuals) || (nonNilActuals > formals.Length && varPars.IsNone) then
                if minActuals = formals.Length then
                    if ctxt.trackErrors then
                        addError
                            !currentFileName
                            ("received " + actuals.Length.ToString() + " arguments. Expected " + formals.Length.ToString() + ".")
                            (funcStart,callFin)
                else
                    if ctxt.trackErrors then
                        addError
                            !currentFileName
                            ("received " + actuals.Length.ToString() + " arguments. Expected between " + minActuals.ToString() + " and " + formals.Length.ToString() + ".")
                            (funcStart,callFin)

                //we still must compute types of all actuals because a stub could be located in these actuals
                List.iter (fun (actual) -> ignore( (typeCheckExprCompute ctxt actual) )) actuals
            else
                let maxlen = max (formals.Length) (actuals.Length)
                // if we need to do any padding here, varPars.IsSome must be true, due to the if condition 
                let formals = padList formals ("","",UnknownTy) maxlen
                let actuals = padList actuals (Nil rng) maxlen
                let actualTypes = padList actualTypes NilTy maxlen
                let triples = List.zip3 actuals actualTypes formals
                let checkTriple ((actual,_,(_,_,formalTy)) : TypedExpr*Type*(string*string*Type)) =
                    ignore (typeCheckExprCheck ctxt actual formalTy)
                List.iter checkTriple triples
            
            let retTy = Type.Coerce ctxt.tenv (TupleTy(List.map snd rets,varRets.IsSome))

            match retTy,expectedTy with
            | ErrorTy(msg),_ ->
                if ctxt.trackErrors then
                    addError
                        !currentFileName
                        msg
                        (funcStart,callFin)
            | _ ->
                ignore (checkType ctxt expr retTy expectedTy)

            None,deps

        | UnknownTy ->
            let ctxt = {ctxt with closestEnclosingFunctionCall = Some(optField,getExprRange func,funcTy0)}
            List.iter (fun expr -> ignore (typeCheckExprCompute ctxt.IsNotLeftExpr expr)) actuals
            None, deps
        | _ ->
            let (funcStart,funcFin) = getExprRange func
            let (_,callFin) = rng
            if ctxt.trackErrors then
                addError
                    !currentFileName
                    ("instances of type " + funcTy.ToString() + " are uncallable")
                    (funcStart,callFin)  
            None, deps
    | Ascription(expr,ty,rng) ->
        let field,deps = typeCheckExprCheck ctxt expr ty
        ignore (checkType ctxt expr ty expectedTy)
        field, deps
    | ErrorExpr(msg,rng) ->
        None, Set.empty


let (|MethodDefInCtxt|) (ctxt : Context) (methName : string) (stat : TypedStatement) =
    let venv,tenv = ctxt.venv, ctxt.tenv
    let rec findDef (stat : TypedStatement) (ctxt : Context) : Option<TypedStatement*Context*Range>=        
        match stat with
        | Sequence(_,stats,rng) ->
            let ctxt = ref ctxt
            let def = ref None
            for s in stats do
                if (!def).IsNone then
                    match findDef s !ctxt with
                    | Some x ->
                        def := Some x
                    | None ->
                        ctxt := updateCtxt !ctxt s
            !def
        | Assign(
            _,
            [BinOpExpr(OpSelect,NameExpr(x,_),String(name,nameRng),_)],
            [Function(desc,selfName,latent,formals,varargs,rets,varRets,body,(startLoc,_))],
            _
          ) when name = methName ->
            let foldFormal (mp : ValueEnvironment) (name,desc,ty) =
                let field = {
                    desc = desc
                    ty = Type.Unfold tenv ty
                    dependencies = Set.empty
                    loc = (!currentFileName,nameRng)
                    isVisible = true
                    isConst = false
                }
                 mp.Add(name,field)
            let argVenv : ValueEnvironment = List.fold foldFormal Map.empty formals 
            let ctxt = 
                {
                ctxt with
                    venv = cover ctxt.venv argVenv
                }
            Some (body, ctxt, nameRng)
        | _ ->
            None
   
    findDef stat ctxt

