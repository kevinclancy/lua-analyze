﻿//
// 1st pass: no inheritance
//

module LuaAnalyzer.ScsTypeCollector

open Type
open Syntax
open TypedSyntax
open TypeChecker
open ErrorList

type Range = int*int

/// GlobalStyleModuleDefinition(range)
exception GlobalStyleModuleDefinition of Range

let rec getName (currentFile : string) (ast : TypedStatement) =
    let rec getNameRec (ast : TypedStatement) (ctxt : Map<string,string>) =
        match ast with
        | LocalAssign(
            _,
            _,
            [NameExpr(name,rng)],
            [CallExpr(
                BinOpExpr(OpMethInd,NameExpr(parentName,prng),String("new",_),_),
                _,
                _
            )],
            _
          ) ->
            if name = "class" then
                addError
                  currentFile
                  ("classes called 'class' are not allowed.")
                  prng
                None
            elif ctxt.ContainsKey parentName then
                Some (name, ctxt.Item parentName, rng)
            elif parentName = "class" then
                Some (name, "class", rng)
            else
              addError
                currentFile
                (parentName + " not a class.")
                prng
              None
        | Assign(
            _,
            _,
            [NameExpr(name,rng)],
            [CallExpr(
                BinOpExpr(OpMethInd,NameExpr(_,_),String("new",_),_),
                _,
                _
            )],
            _
          ) ->
      
          raise( GlobalStyleModuleDefinition(rng) )
        | Sequence(s0,s1,_) ->
          let ctxt' = 
              match s0 with
              | LocalAssign(
                _,
                _,
                [NameExpr(name,rng)],
                [CallExpr(NameExpr("require",_),String(modName,_) :: _,_)],
                _
               ) ->
                ctxt.Add(name,modName)
              | _ ->
                ctxt
        
          let n0 = getNameRec s0 ctxt
          let n1 = getNameRec s1 ctxt'

          if (Option.isSome n0) && (Option.isSome n1) then
            failwith "multiple classes defined in a file"
          elif Option.isSome n0 then
            n0
          elif Option.isSome n1 then
            n1
          else
            None
        | _ ->
            None

    getNameRec ast Map.empty

type FieldMap = Map<string, string*Type*DefinitionLocation>

let rec returnsName (name : string) (ast : TypedStatement) =
    match ast with
    | Sequence(s0,DoNothing(_),_) ->
        returnsName name s0
    | Sequence(s0,s1,_) ->
        returnsName name s1
    | Return([NameExpr(x,_)],_) when x = name ->
        true
    | _ ->
        false

let typeGraphBuilder (modname : string) (fileName : string) (ast : TypedStatement) =
    try
        match getName fileName ast with
        | Some (name,parentName, rng) when (returnsName name ast) ->
            let internTypes = [(modname + "|private",rng)]
                
            let edges = 
                if parentName = "class" then
                    [(modname + "|private", modname)]
                else
                    [
                        (modname + "|private", modname)
                        (modname,parentName)
                    ]
            
            Some (rng,internTypes,edges)
        | _ ->
            None
    with
    | GlobalStyleModuleDefinition(rng) ->
        ErrorList.addError fileName "Global style module definitions not allowed" rng
        None

let rec getMethods (parentMethods : FieldMap) (fileName : string) (className : string) (ast : TypedStatement) : FieldMap =
    match ast with
    | Assign(
        _,
        _,
        [BinOpExpr(OpInd,NameExpr(x,_),String(methName,nameRng),_)], 
        [Function(desc,selfName,formals,varargs,rets,body,(startLoc,_))],
        _
      ) when x = className ->

        let formals,rets = 
          match parentMethods.TryFind methName with
          | Some(_,FunctionTy(_,pformals,prets,_,_),_) ->
            let mapFormal (pname : string ,pdesc : string, pty : Type) =
                match List.tryFind (fun (d,n,t) -> n = pname) formals with
                | Some(cdesc,cname,cty) ->
                    (cname,cdesc,cty)
                | None ->
                    (pname, pdesc, pty)

            List.map mapFormal pformals, coverList prets rets
          | _ ->
            let getParamInfo ((desc,name,ty) : string*string*Type) =
                (name, desc, ty)

            List.map getParamInfo formals, rets

        let methodTy = 
            FunctionTy(
                desc, 
                formals,
                rets, 
                true, 
                (fileName, nameRng)
            )

        new FieldMap([(methName,(desc,methodTy,(fileName,nameRng)))])
    | Sequence(s0,s1,_) ->
        //TODO: detect duplicate method definitions
        let f0 = getMethods parentMethods fileName className s0
        let f1 = getMethods parentMethods fileName className s1
        cover f0 f1
    | _ ->
        Map.empty

let externalTypeBuilder (env : TypeEnvironment) (modname : string) (path : string) (ast : TypedStatement) =
    let typeMap = env.typeMap
    match getName path ast with
    | Some (name,parentName,rng) when (returnsName name ast) ->
        let parentMeta, parentFields, parentMethods =
            if parentName = "class" then
                EmptyMetamethodSet, Map.empty, Map.empty
            else
                match typeMap.Item (parentName) with
                | RecordTy(_,_,_,parentMeta,parentFields,parentMethods,_) ->
                    parentMeta, parentFields, parentMethods
                | _ ->
                    failwith "type collector error: module types should only be records"
        let methods = getMethods parentMethods path name ast
        
        let methods = cover parentMethods methods


        if methods.ContainsKey "init" then
            let classTy = RecordTy(modname,"class " + modname,None,EmptyMetamethodSet,Map.empty,methods.Remove("init"),(path,rng))
            let cons = 
                match methods.Item "init" with
                | _,FunctionTy(desc,self :: formals,rets,isMethod,defLocation), loc ->
                    let consMeta = { 
                        EmptyMetamethodSet with
                            Call = Some("",formals,[("new instance of " + modname + " class", classTy)],false,(modname,rng))
                    }
                    let consMethods = Map.empty.Add("new", ("create an instance", UnknownTy, loc))
                    RecordTy(modname+".constructor","",None,consMeta, Map.empty, consMethods, loc)
                | _ ->
                    failwith "simple class system error: constructor types should always be premethods."

            Some ( classTy, cons )
        else
            addError path "no constructor defined" rng
            None
    | _ ->
        raise( TypeChecker.CollectorError "could not build external type" )

let rec extractPrivateFields (fileName : string) (env : TypeEnvironment) (ctxt : Context) (consBody: TypedStatement) =
    match consBody with
    | Sequence(s0,s1,_) ->
        let ctxt' = updateCtxt env ctxt s0
        //TODO: I actually don't want to have multiple field definitions. cover should not be used
        cover (extractPrivateFields fileName env ctxt s0) (extractPrivateFields fileName env ctxt' s1)
    | Assign(
        ascriptions,
        _,
        [BinOpExpr(OpInd,NameExpr(self,_),String(fieldName,rng),_)],
        [rhs],
        _
      ) ->
        let rty = typeCheckExpr env ctxt rhs false

        //unpack from tuple if necessary
        let rty =
            match rty with
            | TupleTy([ty]) ->
                ty
            | _ ->
                rty

        let rty = 
            if ascriptions.Length > 0 then
                ascriptions.[0]
            else
                rty
        
        Map.add fieldName ("",rty,(fileName,rng)) (Map.empty)
    | _ ->
        Map.empty

let internalTypeBuilder (env : TypeEnvironment) 
                        (globalCtxt : Context) 
                        (modname : string) 
                        (fileName : string)
                        (ast : TypedStatement) =
    
    let typeMap = env.typeMap

    let parentName, rng =
        match getName "" ast with
        | Some(_,parentName,rng) ->
            parentName, rng

    let parentPrivFields, parentMethods = 
        if parentName = "class" then
            Map.empty, Map.empty
        else
            match typeMap.Item (parentName + "|private") with
            | RecordTy(_,_,None,_,fields,methods,_) ->
                fields, methods
            
    match ast with
    | TypeChecker.MethodDefInCtxt env globalCtxt "init" (Some (body,ctxt,_)) ->
        let privateFields = extractPrivateFields fileName env ctxt body
        let exTy = typeMap.Item modname
        match exTy with
        | RecordTy(name,desc,None,metaset,publicFields,publicMethods,defLoc) ->
            let fields = cover privateFields parentPrivFields 
            let methods = cover publicMethods parentMethods
            //TODO: must be mutually exclusive
            new Map<string,Type>([(modname + "|private",RecordTy(modname + "|private",desc,None,metaset,fields,methods, (fileName,rng)))])
    | _ ->
        new Map<string,Type>([(modname + "|private",RecordTy(modname + "|private","",None,EmptyMetamethodSet,Map.empty,Map.empty, (fileName,rng)))])

let decorate (env : TypeEnvironment) (modname : string) (ast : TypedStatement) =
    let typeMap = env.typeMap
    let (Some (className,_,_)) = getName "" ast

    let parentName, parentTy = 
        match (env.edges.Item modname).Length with
        | 0 ->
            None, None
        | _ ->
            let parentName = (env.edges.Item modname).[0]
            Some(parentName), Some(env.typeMap.Item parentName)

    let rec decRec (ast : TypedStatement) =
        match ast with
        | Sequence(s0,s1,rng) ->
            Sequence(decRec s0, decRec s1, rng)
        | Assign(
            vartypes,
            vardescs,
            [BinOpExpr(OpInd,NameExpr(x,rcl),String(methName,rmth),rind)], 
            [Function(desc,funcName,self :: restFormals,varargs,rets,body,funRng)],
            rng
            ) when x = className ->

            let parentMethodTy = 
                match parentTy with
                | Some( RecordTy(_,_,_,_,fields,methods,_) ) ->
                    methods.TryFind methName
                | None ->
                    None
            
            let mapFormal (desc,name,ty) =
                match parentMethodTy with
                | Some(_,FunctionTy(_,pformals,prets,_,_),_) ->
                    match List.tryFind (fun (n,d,t) -> n = name) pformals with
                    | Some (n,d,t) ->
                        (d,n,t)
                    | None ->
                        (desc,name,ty)
                | _ ->
                    (desc,name,ty)
            
            let rets =
                match parentMethodTy with
                | Some(_,FunctionTy(_,pformals,prets,_,_),_) ->
                    prets
                | _ ->
                    rets

            let selfDesc,selfName,selfTy = self
            let self' = 
                if methName = "init" && parentTy.IsSome then 
                    
                    let parentInit = 
                        match env.consMap.TryFind (parentName.Value) with
                        | Some( CallableTy(desc,formals,rets,_,defLocation) ) ->
                            desc,FunctionTy(desc,("","",selfTy) :: formals,[],true,defLocation), defLocation
                        | _ ->
                            failwith "parent constructor should be a record"

                    let privateTy = (typeMap.Item (modname + "|private"))
                    let privateTy =
                        match privateTy with
                        | RecordTy(a,b,c,d,e,methods,f) ->
                            RecordTy("*UnnamedTy*",b,c,d,e,methods.Add("init_super", parentInit),f)
                        | _ ->
                            failwith "private self type should be a record type"

                    selfDesc,selfName,privateTy
                else
                    selfDesc,selfName,(typeMap.Item (modname + "|private"))
            Assign(
                vartypes,
                vardescs,
                [BinOpExpr(OpInd,NameExpr(x,rcl),String(methName,rmth),rind)],
                [Function(desc,funcName,self' :: List.map mapFormal restFormals,varargs,rets,body,funRng)],
                rng
            )
        | x ->
            x

    decRec ast

let detectMiscErrors (tenv : TypeEnvironment) (modulePath : string) (filePath : string) (ast : TypedStatement) =
    if (tenv.edges.Item modulePath).Length > 0 then
        match ast with
        | TypeChecker.MethodDefInCtxt tenv Map.empty "init" (Some (body,_,nameRng)) ->
            match body with
            | Sequence(Call(CallExpr(BinOpExpr(BinOp.OpMethInd, NameExpr("self",_),String("init_super",_),_),_,_),_),_,_)
            | Call(CallExpr(BinOpExpr(BinOp.OpMethInd, NameExpr("self",_),NameExpr("init_super",_),_),_,_),_) ->
                ()
            | _ ->
                addError 
                    filePath
                    ("constructor does not call self:init_super on first line")
                    nameRng
        
            
            

let activateModule () =
    TypeChecker.addTypeCollector {
        name = "Simple Class System"
        typeGraphBuilder = typeGraphBuilder
        typeGraphIsTree = true
        externalTypeBuilder = externalTypeBuilder
        internalTypeBuilder = internalTypeBuilder
        decorate = decorate
        detectMiscErrors = detectMiscErrors
    }
