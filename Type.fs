﻿///
/// Defines optional types which are used for classifying 
/// lua expressions and propogating intellisense data.
///
module LuaAnalyzer.Type

open Utils
open Operators

/// Stores the location of a function definition
/// the string is the project-relative filename containing the definition
/// the range is the absolute offsets of the first and last characters of 
/// the definition
type DefinitionLocation = string*(int*int)

let NoLocation : DefinitionLocation = ("",(0,0))

/// the second component of a quadruple (a,b,c,d)
let quadB (a,b,c,d) = b

/// the third component of a quadruple (a,b,c,d)
let quadC (a,b,c,d) = c

/// A structure describing all known types, their properties, and relations, 
/// available at a position in the program.
type TypeEnvironment = {

    /// The set of all instance type names, i.e. those types which can be 
    /// referenced from annotations
    instanceTypes : Set<string>

    /// The names of all type parameters in the type environment
    typeParamNames : Set<string>

    /// The names of all typedefs that are in the type environment
    typedefNames : Set<string> 

    /// Maps all instance, internal, type variable, and typedef type names to their
    /// corresponding type structures.
    typeMap : Map<string,Type>

    /// Maps the name of each type to the range its declaration appears in
    typeDeclarationRanges : Map<string,Range>

    /// Maps each user-defined type name to (file,moduleName)
    /// Where fileName is the name of the file it was defined in
    /// and moduleName is the name of the module it was defined in
    typeFiles : Map<string, string*string>

    /// maps constructor names to their corresponding type structures
    consMap : Map<string,Type>

    /// A nominal subtyping graph, includes both internal and external types
    /// An entry (a,b) means "a is a subtype of each element in b"
    edges : Map<string,List<string>>
}

/// Describes an operation which can be performed by an object.
and Method = 
    {
        /// A description of the method's behavior
        desc : string
        /// The type of the method.
        /// Must be a function type with at least one formal parameter,
        /// the leading parameter being a non-typed self reference.
        // Typing the self argument would make it difficult to reuse methods
        // via inheritance. Instead, the typing of the self argument should
        // be done by a type collector's decorate callback.
        ty : Type
        /// Whether or not the method is abstract. 
        isAbstract : bool
        /// The location at which this identifier was 
        /// delcared.
        loc : DefinitionLocation
    }

/// Many identifiers occurring in programs refer to runtime values. These
/// identifiers are associated with the statically computed satellite data 
/// contained in Field.
and Field =
    {
        /// A description of the identifier's purpose
        desc : string

        /// A statically computed type of the values that the identifier
        /// denotes. Since this is not a sound type system, runtime values
        /// associated with this identifier may sometimes not actually belong
        /// to this type; the goal is to design the type system to minimize
        /// the frequency of theses scenarios without limiting the programmer.
        ty : Type
        
        /// all of the modules whose type information was necessary to compute ty.
        dependencies : Set<string>

        /// Whether or not the field shows up in autocompletion lists
        isVisible : bool

        /// The location (filename, char index range) at which this identifier was 
        /// delcared.
        loc : DefinitionLocation

        /// Whether or not this identifier can be reassigned to expressions
        /// determined to be consistent with *ty*.
        isConst : bool
    }

    override this.ToString() =
        this.ty.ToString()

and Field with
    /// generate an empty field with the given type
    static member OfType(ty : Type) =
        {
            desc = ""
            ty = ty
            loc = NoLocation
            isConst = false
            isVisible = true
            dependencies = Set.empty
        }

and OpenRecordStatus =
    // TODO: I plan to add extra fields to the Open variant which answer the following questions:
    // are new fields which are added to this record visible to autocompletion? are they const?
    | Open
    | Closed

/// Param(name, description, type) represents a formal parameter of a callable type 
/// Name - the name of the formal parameter
/// Description - a description of the formal parameter
/// Typename - the type of the formal parameter
and Param = string*string*Type

/// Ret(description,type) represents a formal return of a callable type
/// description - description of return value
/// type - type for the return value
and Ret = string*Type

////// TODO************************** Override Type's equality checking so that structural subtyping ignores the rng in type vars and usertypes
////// TODO****** actually, keeping subtyping queries cached is wrong if there are any errors.... the most efficient approach
//////            would involve keeping two hash tables, one for subtyping queries which have been verified to pass and the other for queries
//////            which are pending - merging pending queries would slow down the case in which everything is well-typed
//////   *or I could just clear out the queries after every failed query

/// optional types for classifying lua expressions
and Type =
    /// NumberTy classifies expressions which denote double precision floating
    /// point numbers, which are used for both integers and approximating 
    /// real numbers in Lua.
    | NumberTy
    /// StringTy classifies expressions which denote immutable arrays of characters.
    | StringTy
    /// NilTy classifies expressions which denote the value nil
    | NilTy
    /// BoolTy classifies expressions which may denote either true or false.
    | BoolTy
    /// Record(name,description,isStructural,displayStructural,openStatus,metamethods,fieldMap,fieldList,methodMap,typeArguments,defLocation)
    /// fieldMap and methodMap map names to description/type/loc triples
    /// fieldList has the same contents as fieldMap, and exists to preserve the order that the fields were defined in,
    /// for intellisense purposes
    ///
    /// Classifies expressions which denote records. A record contains labeled
    /// fields and methods which can be indexed, as well as a set of operator
    /// overloads (i.e. metamethods). 
    | RecordTy of string*string*bool*bool*OpenRecordStatus*MetamethodSet*Map<string,Field>*List<string*Field>*Map<string,Method>*List<Type>*DefinitionLocation
    /// When an open record in a left expression gets indexed by a field it doesn't
    /// have, the resulting type is NewField. NewField is a treated as a 
    /// supertype of everything so that its variables can be assigned to anything.
    | NewFieldTy
    /// Function(desc,params,varParams,rets,varRets,method,latent,deflocation) -
    /// desc - description of the function
    /// params - name/description/type triples for each formal parameter
    /// varParams - if this function takes arbitrary additional parameters Some(expl) where expl explains what these parameters are. Otherwise, None.
    /// ret - description/type pairs for each return value
    /// varRets - if this function returns arbitrary additional values, Some(expl) explains what these additional values are. Otherwise, None.
    /// method - whether or not this is a method 
    /// latent - whether or not this function may yield and call other functions which might yield
    /// deflocation - location of function definition
    ///
    /// FunctionTy classifies expressions which denote functions or procedures
    /// which can be called.
    | FunctionTy of string*List<Param>*Option<string>*List<Ret>*Option<string>*bool*bool*DefinitionLocation
    /// TupleTy([ty1,ty2,...,tyN],indefinite) classifies expressions which denote an ordered 
    /// list of values, where ty1 classifies the first value, ty2 classifies the
    /// second, etc. The only such expressions in lua are call expressions, which
    /// may return multiple values, and vararg literals. TupleTy may be coerced 
    /// into the first type in its contained list.
    /// If indefinite is true, an infinite sequence of UnknownTy follows ty1,...tyN.
    | TupleTy of List<Type>*bool
    /// OverloadTy([ty1,ty2,...]) classifies record lookups on keys which
    /// which map to several values of different types. This is intended 
    /// to represent overloaded functions defined in C APIs, and should be
    /// used sparingly.
    | OverloadTy of List<Type>
    /// NillableTy(ty) behaves exactly the same as ty, but NilTy is a subtype
    /// of it, allowing NillableTy(ty) variables to be assigned to nil.
    /// These types appear in annotations as ?typename.
    | NillableTy of Type
    /// UserDefined(typename,rng) is converted to userTypes[typename] before it is 
    /// used. Indirection is necessary in order to handle recursive types.
    | UserDefinedTy of string*Range
    /// AccumulatedTy(permTy,tempTy) tempTy must be a subtype of permTy
    /// can be assigned to any value belonging to the permanent type
    /// can be used as temporary type
    /// These do not appear in annotations, but appear in intellisense as 
    /// "perm(temp)"
    | AccumulatedTy of Type*Type
    /// TypeVar(name,desc,loc,rng) a type variable with a specified
    /// name and descriptions, whose definition occurs at loc, and
    /// whose text appears in range rng.
    | TypeVarTy of string*string*DefinitionLocation*Range
    /// TypeAbs(name/descs,body) A type abstraction which binds the specified
    /// names over the specified body.
    | TypeAbsTy of List<string*string>*Type
    /// TypeAppTy(abstraction, arguments)
    /// where abstraction is an abstraction to apply a type argument to
    /// and argument is the type argument we are applying the abstraction to
    | TypeAppTy of Type*List<Type>
    /// ErrorTy(msg) -
    /// Whenever a function returns an item belonging to type ErrorTy,
    /// the type checker reports the error contained in msg. This can
    /// be used to notify the user of common problems, such as trying
    /// to instantiate an abstract class. 
    | ErrorTy of string
    /// Classifies all expressions which we are unable to classify by other
    /// means. Expressions of this type can occur in any context without
    /// generating errors.
    /// Appears in annotations as "unknown".
    | UnknownTy

    /// Provides an elaborate, multiline description of the type (or a short one-liner when sufficient) 
    /// for use in calltips.
    member this.ToStringElaborate() =
        match this with
        | FunctionTy(desc,pars,varPars,rets,varRets,meth,latent,deflocation) ->
            let desc = if desc = "" then "" else "\n" + desc
            let ret = 
                if latent then
                    ref ("Function (latent)" + desc)
                else
                    ref ("Function" + desc)
            
            if pars.Length > 0 || varPars.IsSome then
                ret := !ret + "\nParameters:"

            if pars.Length > 0 then
                for pname,pdesc,pty in pars do
                    ret := !ret + "\n" + pname + " : " + pty.ToString() + " - " + pdesc
                
            match varPars with
            | Some(desc) ->
                ret := !ret + "\n" + "... - " + desc
            | None ->
                ()

            if rets.Length > 0 || varRets.IsSome then
                ret := !ret + "\nReturns:"

            if rets.Length > 0 then
                for rdesc,rty in rets do
                    ret := !ret + "\n" + rty.ToString() + " - " + rdesc

            match varRets with
            | Some(desc) ->
                ret := !ret + "\n" + "... - " + desc
            | None ->
                ()

            !ret
        | RecordTy(_,desc,_,_,_,_,_,_,_,_,_) ->
            this.ToString() + "\n" + desc
        | AccumulatedTy(perm, temp) ->
            "\n" + "Bound Type: " + perm.ToString() + "\n" +
            "Accumulated Type: " + temp.ToStringElaborate()
        | _ ->
            this.ToString()

      override this.ToString() =
        match this with
        | NumberTy ->
            "number"
        | StringTy ->
            "string"
        | NilTy ->
            "nil"
        | BoolTy ->
            "boolean"
        | RecordTy(name,_,_,false,_,_,_,_,_,typeArgs,_) ->
            if typeArgs.IsEmpty then
                name
            else
                name + "<" + String.concat "," (List.rev (List.map (fun x -> x.ToString()) typeArgs)) + ">"
        | RecordTy(name,_,_,true,_,metaset,_,_,_,_,_) when metaset.Index.IsSome && (quadB metaset.Index.Value = NumberTy) && not (Type.IsNillable (quadC metaset.Index.Value)) ->
            let (_,_,elemTy,_) = metaset.Index.Value
            "{" + elemTy.ToString() + "}"
        | RecordTy(name,_,_,true,_,metaset,_,_,_,_,_) when metaset.Index.IsSome && Type.IsNillable (quadC metaset.Index.Value)->
            let (_,keyTy,valTy,_) = metaset.Index.Value
            "{" + keyTy.ToString() + " => " + valTy.StripNillable().ToString() + "}"
        | RecordTy(name,_,_,true,_,_,_,fieldList,methods,_,_) ->
            let fieldToStr ((name,field) : string*Field) =
                name + (if field.isConst then " :~ " else " : ") + field.ty.ToString()
            
            let methodToStr ((name,meth) : string*Method) =
                name + " :: " + meth.ty.ToString()

            let methodsString = 
                if methods.Count = 0 then
                    ""
                else
                    (String.concat ", " (Seq.map methodToStr (Map.toSeq methods)))

            let fieldsString = 
                if fieldList.IsEmpty then
                    ""
                else
                    (String.concat ", " (List.map fieldToStr fieldList))
            
            let separatorString = if (not fieldList.IsEmpty) && methods.Count > 0 then ", " else ""

            "{" + fieldsString + separatorString + methodsString + "}"
        | NewFieldTy ->
            "newfield"
        | FunctionTy(desc,pars,varPars,rets,varRets,meth,latent,deflocation) ->
            let opener = if latent then "[" else "["
            let closer = if latent then "]" else "]"
            let arrow = if latent then "-L>" else "->"
            let foldPar acc (name,desc,ty) =
                acc + ty.ToString()
            let foldRet acc (desc,ty) =
                acc + ty.ToString()

            let parTys = List.map (fun (name,desc,ty) -> ty) pars
            let retTys = List.map (fun (desc,ty) -> ty) rets

            let toStr (l : List<Type>) (indefinite : bool) =
                let ret = ref opener

                for i in 0..l.Length-2 do
                    ret := !ret + l.[i].ToString() + ","

                if l.Length > 0 then
                    ret := !ret + l.[l.Length-1].ToString() + (if indefinite then ", ..." else "")
                else
                    ret := !ret + (if indefinite then "..." else "") 

                ret := !ret + closer
                !ret

            (toStr parTys varPars.IsSome) + arrow + (toStr retTys varRets.IsSome)

        | NillableTy(ty) ->
            "?" + ty.ToString()
        | UserDefinedTy(name,_) ->
            name
        | TupleTy(tyList,indefinite) ->
            "tuple: " + tyList.ToString() + (if indefinite then "..." else "")
        | OverloadTy(tyList) ->
            "overload: " + tyList.ToString()
        | AccumulatedTy(perm,temp) ->
            perm.ToString() + "(" + temp.ToString() + ")"
        | ErrorTy(_) ->
            "Error"
        | UnknownTy ->
            "unknown"
        | TypeAbsTy(names,body) ->
            "∀" + (String.concat "," (List.map fst names)) + " " + body.ToString() + "<" + (String.concat "," (List.map fst names)) + ">"
        | TypeVarTy(name,desc,loc,_) ->
            name
        | TypeAppTy(absTy, args) ->
            absTy.ToString() + "<" + (String.concat ", " (List.map (fun x -> x.ToString()) args)) + ">"

    
    member this.HasName
        with get() =
            match this with
            | RecordTy(_,_,_,_,_,_,_,_,_,_,_) 
            | UserDefinedTy(_) ->
                true
            | AccumulatedTy(_,underlying)
            | NillableTy(underlying) ->
                underlying.HasName
            | _ ->
                false

    member this.Name 
        with get() =
            match this with
            | RecordTy(name,_,_,_,_,_,_,_,_,_,_) 
            | UserDefinedTy(name,_) ->
                name
            | AccumulatedTy(_,underlying)
            | NillableTy(underlying) ->
                underlying.Name
            | _ ->
                failwith "tried to access the name of a non-record type"

    /// Computes the type of a lua tuple expression e1,e2,...,en
    /// given the types of its constituent expressions.
    ///
    /// if any of e1,...,e(n-1) have tuple types, we include only the head element
    /// in our result. If they have non-tuple-types, we include their types unchanged.
    /// If en has a tuple type, we append all its elements onto our result.
    /// Otherwise, we include en's type unchanged.
    ///
    /// The second return value (the boolean) represents whether the sequence is
    /// indefinite or not. If its last expression en is a varargs expression 
    /// or a call to a function with indefinite returns, the sequence is indefinite.
    /// Otherwise, it is not indefinite.
    static member SeqType (exprTys : List<Type>) =
        let ret = ref []

        // if any of the first n-1 elements are tuples, only include the head
        for i in 0..(exprTys.Length-2) do
            match exprTys.[i] with
            | TupleTy(head :: _,_) ->
                ret := head :: !ret
            | TupleTy([],true) ->
                ret := UnknownTy :: !ret
            | x ->
                ret := x :: !ret

        ret := List.rev !ret
        if exprTys.Length > 0 then
            match exprTys.[exprTys.Length-1] with
            | TupleTy(tyList,indefinite) ->
                List.append !ret tyList, indefinite
            | _ ->
                List.append !ret [exprTys.[exprTys.Length-1]],false
        else
            !ret,false

    member this.StripNillable() =
        match this with
        | NillableTy(ty) ->
            ty.StripNillable()
        | _ ->
            this

    member this.StripOuterAbstractions() =
        match this with
        | TypeAbsTy(nm,body) ->
            body.StripOuterAbstractions()
        | _ ->
            this

    /// Given a type name, return the Type structure for the type with that name
    static member FromString (name : string) =
        let name,nillable,isConst =
            if name.[0] = '?' then
                name.Substring(1),true,false
            else
                name,false,false

        let ty =
            match name with
            | "nil" ->
                NilTy
            | "number" ->
                NumberTy
            | "boolean" ->
                BoolTy
            | "unknown" ->
                UnknownTy
            | _ ->
                UserDefinedTy(name,(0,0))

        if nillable then
            NillableTy(ty)
        else
            ty
    
    // if ty is a UserDefinedTy, look up the type in the given type environment
    // return UnknownTy if the type is not contained in the type environment
    // if ty is not a UserDefinedTy, return ty
    static member Unfold (env : TypeEnvironment) (ty : Type) =
        let tyMap = env.typeMap
        match ty with
        | UserDefinedTy(name,_) ->
            match tyMap.TryFind name with
            | Some(userTy) ->
                userTy
            | None ->
                UnknownTy
        | _ ->
            ty
    
    /// if ty is a nonempty TupleTy, return the first element of the tuple
    /// if ty is an empty TupleTy, return UnknownTy if it's indefinite and NilTy if it is not indefinite
    /// otherwise, return ty
    static member Untuple (ty : Type) =
        match ty with
        | TupleTy(head::_,indefinite) ->
            head
        | TupleTy([],true) ->
            UnknownTy
        | TupleTy([],false) ->
            NilTy
        | _ ->
            ty
                
    /// True iff ty is nillable
    static member IsNillable (ty : Type) =
        match Type.Untuple ty with
        | NillableTy(_) ->
            true
        | _ ->
            false
    
/// (description, rhsType, retTy, defLocation) for binary operator
and BinOpTy = string*Type*Type*DefinitionLocation

/// description, return type, definition location of unary operator
and UnOpTy = string*Type*DefinitionLocation

and MetamethodSet = {
    /// Binary '+' operator type, if defined
    Add : Option<BinOpTy>
    /// Binary '-' operator type, if defined
    Sub : Option<BinOpTy>
    /// Binary '*' operator type, if defined
    Mul : Option<BinOpTy>
    /// Binary '/' operator type, if defined
    Div : Option<BinOpTy>
    /// Binary '%' operator type, if defined
    Mod : Option<BinOpTy>
    /// Binary '^' operator type, if defined
    Pow : Option<BinOpTy>
    /// Unary '-' operator type, if defined
    Unm : Option<UnOpTy>
    /// Binary '..' operator type, if defined
    Concat : Option<BinOpTy>
    /// Unary '#' operator type, if defined
    Len : Option<UnOpTy>
    /// Binary '<' operator type, if defined
    Lt : Option<BinOpTy>
    /// Binary '<=' opeartor type, if defined
    Le : Option<BinOpTy>
    /// Binary '>' operator type, if defined
    Gt : Option<BinOpTy>
    /// Binary '>=' operator type, if defined
    Ge : Option<BinOpTy>
    
    /// Binary 'and' operator type, if defined; for certain built-in types only
    And : Option<BinOpTy>
    /// Binary 'or' operator type, if defined; for certain built-in types only
    Or : Option<BinOpTy>
    /// Unary 'not' operator type, if defined; for certain built-in types only
    Not : Option<UnOpTy>

    /// Binary '[]' operator type, if defined
    /// note that string cannot be used as the rhs type, as string indices are 
    /// considered structural.
    Index : Option<BinOpTy>

    // newindex is not handled by the type system
    // whenever an assign happens, we check that the
    // lhs and rhs have equal types. So newindex is really
    // handled using the index metamethod in our type system.

    /// call operator
    Call : Option< string*List<string*string*Type>*Option<string>*List<string*Type>*Option<string>*bool*bool*DefinitionLocation > 
}

let private assumptions : HashSet<Type*Type> = new HashSet<Type*Type>()

let clearAssumptions () = assumptions.Clear()

type private SubtypeResult =
    /// Error message
    | ErrorMsg of string
    | IsSubtype

type private StructuralSubtypeResult =
    /// Error message, filename, range
    | ErrorAndLoc of string*string*Range
    | IsStructuralSubtype

let private typeEval = ref (fun env ty -> ty)

let private refSubstTy = ref (fun (typeVarName : string) (substitution : Type) (target : Type) -> UnknownTy) 

let private substTy (typeVarName : string) (substitution : Type) (target : Type) =
    (!refSubstTy) typeVarName substitution target

let private refIsSubtypeOfAux = ref (fun _ _ _ -> ErrorMsg(""))

let private isSubtypeOfAux (env : TypeEnvironment) (a : Type) (b : Type) =
    (!refIsSubtypeOfAux) env a b

let private isEqual (env : TypeEnvironment) (a : Type) (b : Type) =
    let res1 = (isSubtypeOfAux env a b)
    match res1 with
    | IsSubtype ->
        isSubtypeOfAux env b a
    | ErrorMsg(msg) ->
        ErrorMsg(msg)

type Type with
    /// env - the type environment we are reasoning under
    /// a - propoesed subtype
    /// b - proposed supertype
    /// 
    /// Given a type environment and two types a and b, returns true iff a is a subtype of b
    /// w.r.t. the type environment.
    /// The second return value is an explanation for why a is not a subtype of b.
    /// (It is equal to "" if a is a subtype of b.)
    static member IsSubtypeOf (env : TypeEnvironment) (a : Type) (b : Type) : bool*string =
        match isSubtypeOfAux env a b with
        | IsSubtype ->
            true, ""
        | ErrorMsg(msg) ->
            assumptions.Clear()
            false,msg

    /// True iff a is a subtype of b and b is a subtype of a w.r.t. env.
    /// if this returns false, the second return value gives an explanation of why 
    static member IsEqual (env : TypeEnvironment) (a : Type) (b : Type) : bool*string =
        match isEqual env a b with
        | IsSubtype ->
            true,""
        | ErrorMsg(msg) ->
            false,msg

type MetamethodSet with

    static member Substitute (typeVarName : string) (substitution : Type) (target : MetamethodSet) =
         let substBinOp (binop : BinOpTy) =
            let (desc,rhsTy,retTy,defLoc) = binop
            (desc,substTy typeVarName substitution rhsTy,substTy typeVarName substitution retTy,defLoc)

         let substUnOp (unop : UnOpTy) =
            let (desc,retTy,defLoc) = unop
            (desc,substTy typeVarName substitution retTy,defLoc)

         let substFunc (desc,pars,varpars,rets,varrets,isMethod,isLatent,defLocation) =
            let pars' = List.map (fun (nm,desc,ty) -> (nm,desc,substTy typeVarName substitution ty)) pars
            let rets' = List.map (fun (desc,ty) -> (desc, substTy typeVarName substitution ty)) rets
            (desc,pars',varpars,rets',varrets,isMethod,isLatent,defLocation)

         {
            Add = Option.map substBinOp target.Add
            Sub = Option.map substBinOp target.Sub
            Mul = Option.map substBinOp target.Mul
            Div = Option.map substBinOp target.Div
            Mod = Option.map substBinOp target.Mod
            Pow = Option.map substBinOp target.Pow
            Unm = Option.map substUnOp target.Unm 
            Concat = Option.map substBinOp target.Concat
            Len = Option.map substUnOp target.Len
            Lt = Option.map substBinOp target.Lt
            Le = Option.map substBinOp target.Le
            Gt = Option.map substBinOp target.Gt
            Ge = Option.map substBinOp target.Ge
            And = Option.map substBinOp target.And
            Or = Option.map substBinOp target.Or
            Not = Option.map substUnOp target.Not
            Index = Option.map substBinOp target.Index
            Call = Option.map substFunc target.Call
         }

    static member public Compose (env : TypeEnvironment) (setA : MetamethodSet) (setB : MetamethodSet) =
        //TODO: this is wrong, but there's really no need to implement this since we don't
        //hav structural metamethod annotations
        setA

    static member public empty 
        with get () =
            {
                Add = None
                Sub = None
                Mul = None
                Div = None
                Mod = None
                Pow = None
                Unm = None
                Concat = None
                Len = None
                Lt = None
                Le = None
                Gt = None
                Ge = None
                And = None
                Or = None
                Not = None
                Index = None
                Call = None
            }

    /// Default metamethod set, defining metamethods only for the == and ~=
    /// operators, which can be applied to operands of any type.
    static member public standard
        with get () =
            {
                Add = None
                Sub = None
                Mul = None
                Div = None
                Mod = None
                Pow = None
                Unm = None
                Concat = None
                Len = None
                Lt = None
                Le = None
                Gt = None
                Ge = None
                And = None
                Or = None
                Not = None
                Index = None
                Call = None
            }
            
    /// Returns a metamethod set which is the result of covering a with b.
    ///
    /// i.e. a metamethod set which uses all of the metamethods
    /// of b which are defined. For those which does b does not define,
    /// it uses a's metamethods if they exist. If neither set defines
    /// an operator, the resulting set does not define that operator.
    static member cover (a : MetamethodSet) (b : MetamethodSet) =
        { MetamethodSet.empty with
            Add = if Option.isSome b.Add then b.Add else a.Add
            Sub = if Option.isSome b.Sub then b.Sub else a.Sub
            Mul = if Option.isSome b.Mul then b.Mul else a.Mul
            Div = if Option.isSome b.Div then b.Div else a.Div
            Mod = if Option.isSome b.Mod then b.Mod else a.Mod
            Pow = if Option.isSome b.Pow then b.Pow else a.Pow
            Unm = if Option.isSome b.Unm then b.Unm else a.Unm
            Concat = if Option.isSome b.Concat then b.Concat else a.Concat
            Len = if Option.isSome b.Len then b.Len else a.Len
            Lt = if Option.isSome b.Lt then b.Lt else a.Lt
            Le = if Option.isSome b.Le then b.Le else a.Le
            Gt = if Option.isSome b.Gt then b.Gt else a.Gt
            Ge = if Option.isSome b.Ge then b.Ge else a.Ge
            And = if Option.isSome b.And then b.And else a.And
            Or = if Option.isSome b.Or then b.Or else a.Or
            Not = if Option.isSome b.Not then b.Not else a.Not
            Index = if Option.isSome b.Index then b.Index else a.Index
            Call = if Option.isSome b.Call then b.Call else a.Call
        }

    /// Returns a metamethod set which is the result of covering b with a.
    /// Unlike cover, this uses a callback to generate errors wherever overlap is detected.
    ///
    /// if both sets define the same metamethod, we include only a's method in our result, and 
    /// pass the name and range of b's covered metamethod to errorFunc
    static member weave (a : MetamethodSet) (b : MetamethodSet) (errorFunc : string -> DefinitionLocation -> unit) =
        let weaveBinOp (name : string) (aOp : Option<BinOpTy>) (bOp : Option<BinOpTy>) =
            match (aOp,bOp) with
                | (Some x,Some (desc,rhsTy,retTy,loc) ) ->
                    errorFunc name loc
                    aOp
                | (Some x, None)
                | (None, Some x) ->
                    Some x
                | (None, None) ->
                    None

        let weaveUnOp (name : string) (aOp : Option<UnOpTy>) (bOp : Option<UnOpTy>) =
            match (aOp, bOp) with
                | (Some x,Some (desc,retTy,loc) ) ->
                    errorFunc name loc
                    aOp
                | (Some x, None)
                | (None, Some x) ->
                    Some x
                | (None, None) ->
                    None
                                             
        { MetamethodSet.empty with
            Add = weaveBinOp "add" a.Add b.Add
            Sub = weaveBinOp "sub" a.Sub b.Sub
            Mul = weaveBinOp "mul" a.Mul b.Mul
            Div = weaveBinOp "div" a.Div b.Div
            Mod = weaveBinOp "mod" a.Mod b.Mod
            Pow = weaveBinOp "pow" a.Pow b.Pow
            Unm = weaveUnOp "negation" a.Unm b.Unm
            Concat = weaveBinOp "concat" a.Concat b.Concat
            Len = weaveUnOp "'length operator'" a.Len b.Len
            Lt = weaveBinOp "less than" a.Lt b.Lt
            Le = weaveBinOp "less equals" a.Le b.Le
            Gt = weaveBinOp "greater than" a.Gt b.Gt
            Ge = weaveBinOp "greater equals" a.Ge b.Ge
            And = weaveBinOp "and" a.And b.And
            Or = weaveBinOp "or" a.Or b.Or
            Not = weaveUnOp "'not operator'" a.Not b.Not
            Index = weaveBinOp "'index operator'" a.Index b.Index

            Call = 
                match (a.Call, b.Call) with
                | (Some (_,_,_,_,_,_,_,defLocationA), Some (_,_,_,_,_,_,_,defLocationB)) ->
                    errorFunc
                        "call operator already defined"
                        defLocationB
                    a.Call
                | (Some x,None)
                | (None, Some x) ->
                    Some x
                | (None,None) ->
                    None
        }
            
type TypeEnvironment with
    /// An empty type environment
    static member empty
        with get () =
            {
                instanceTypes = Set.empty
                typedefNames = Set.empty
                typeParamNames = Set.empty
                typeMap = Map.empty
                typeDeclarationRanges = Map.empty
                typeFiles = Map.empty
                consMap = Map.empty
                edges = Map.empty
            }
    
    /// Returns a sequence of the names of all ancestors of this type,
    /// and this type's name as well.
    member this.AncestorsOfInclusive (typeName : string) =
        (Set.unionMany (List.map (fun x -> this.AncestorsOfInclusive x) this.edges.[typeName])).Add typeName

    /// A list of all typenames appearing in the nominal subtyping
    /// graph, topologically sorted. what does it mean for a list to be 
    /// topologically sorted? See the wikipedia page on it: *insert url here*.
    /// Also, read CLRS; you'll learn a bunch of other cool algorithms in 
    /// addition to the topological sort.
    member this.TopSortedTypeNames
        with get() =
            let subtypeEdges = this.edges

            let visited = new HashSet<string>()
            let ret = ref []

            let rec topSortAux (root : string) =
                for adj in subtypeEdges.Item root do
                    if visited.Contains adj then
                        ()
                    else
                        topSortAux adj

                ignore (visited.Add(root))
                ret := root :: !ret

            for kv in this.typeFiles do
                if subtypeEdges.ContainsKey kv.Key && not (visited.Contains kv.Key) then
                    topSortAux kv.Key
                elif not (visited.Contains kv.Key) then
                    ret := kv.Key :: !ret

            List.rev !ret        

/// Matches any type (either function, or record with call metamethod), which
/// classifies callable values. Yields pattern variables which describe the
/// call interface:
/// CallableTy(desc,formals,varPars,rets,varRets,isMethod,latent,defLocation) 
///
/// desc - description of the function or call metamethod
/// formals - name/description/type pairs for each formal parameter
/// varPars - a description of the meaning of trailing varPars, if any
/// rets - description/type pairs for each return value
/// varRets - a description of the meaning of trailing varrets, if any
/// method - whether or not this is a method (with implicit self)
/// latent - whether or not this callable may yield during execution
/// deflocation - location of function definition
let rec (|CallableTy|_|) (ty : Type) =
    match ty with
    | FunctionTy(desc,formals,varPars,rets,varRets,isMethod,latent,defLocation) ->
        Some (desc,formals,varPars,rets,varRets,isMethod,latent,defLocation)
    | RecordTy(_,_,_,_,_,metamethods,_,_,_,_,_) ->
        match metamethods.Call with
        | Some(desc,formals,varPars,rets,varRets,isMethod,latent,defLocation) ->
            Some (desc,formals,varPars,rets,varRets,isMethod,latent,defLocation)
        | None ->
            None
    | _ ->
        None

/// | ArrayTy(elemTy)
/// Matches any record type which has has an index metatmethod
/// defined whose key type is NumberTy and whose value type
/// is elemTy
let (|ArrayTy|_|) (ty : Type) =
    match ty with
    | RecordTy(_,_,_,_,_,metamethods,_,_,_,_,_) ->
        match metamethods.Index with
        | Some(_,rhsTy,retTy,_) when rhsTy = NumberTy ->
            Some (retTy)
        | _ ->
            None
    | _ ->
        None

/// | ArrayTy(elemTy)
/// Matches any record type which has has an index metatmethod
/// defined whose key type is not NumberTy and whose value type
/// is nillable
let (|MapTy|_|) (ty : Type) =
    match ty with
    | RecordTy(_,_,_,_,_,metamethods,_,_,_,_,_) ->
        match metamethods.Index with
        | Some(_,rhsTy,retTy,_) when rhsTy <> NumberTy && Type.IsNillable retTy ->
            Some (rhsTy, retTy)
        | _ ->
            None
    | _ ->
        None

/// Given a metamethod set, returns the metamethod (if any) defined for
/// the specified unary operator.
let getUnOp (metaset : MetamethodSet) (unop : UnOp) =
    match unop with
    | OpNegate ->
        metaset.Unm
    | OpNot ->
        metaset.Not
    | OpLen ->
        metaset.Len

/// Given a metamethod set, returns the metamethod (if any) defined for
/// the specified binary operator.
let getBinOp (metaset : MetamethodSet) (binop : BinOp) =
    match binop with
    | OpAdd ->
        metaset.Add
    | OpSub ->
        metaset.Sub
    | OpMul ->
        metaset.Mul
    | OpDiv ->
        metaset.Div
    | OpMod ->
        metaset.Mod
    | OpPow ->
        metaset.Pow
    | OpConcat ->
        metaset.Concat
    | OpLt ->
        metaset.Lt
    | OpLe ->
        metaset.Le
    | OpGt ->
        metaset.Gt
    | OpGe ->
        metaset.Ge
    | OpAnd ->
        metaset.And
    | OpOr ->
        metaset.Or
    | OpInd ->
        metaset.Index
    | OpMethInd ->
        metaset.Index
    | OpSelect ->
        metaset.Index
    | OpEq
    | OpNe ->
        failwith "unreachable"

let private numberMeta = 
        {
            MetamethodSet.empty with
                Add = Some ("Add two numbers",NumberTy,NumberTy,NoLocation)
                Sub = Some ("Subtract right-hand number from left-hand number",NumberTy,NumberTy,NoLocation)
                Mul = Some ("Multiply two numbers",NumberTy,NumberTy,NoLocation)
                Div = Some ("Divide left-hand number by right-hand number",NumberTy,NumberTy,NoLocation)
                Mod = Some ("Mod left-hand number by right-hand number",NumberTy,NumberTy,NoLocation)
                Pow = Some ("Raise left-hand number to right-hand number's power",NumberTy,NumberTy,NoLocation)
                Unm = Some ("Negate number",NumberTy,NoLocation)
                Lt = Some ("is the left number less than the right number?",NumberTy,BoolTy,NoLocation)
                Le = Some ("is the left number less than or equal to the right number?",NumberTy,BoolTy,NoLocation)
                Gt = Some ("is the left number greater than the right number?",NumberTy,BoolTy,NoLocation)
                Ge = Some ("is the left number less than or equal to the right number?",NumberTy,BoolTy,NoLocation)
        }

let private boolMeta = 
        {
            MetamethodSet.empty with
                And = Some ("boolean and", BoolTy, BoolTy,NoLocation)
                Or = Some ("boolean or", BoolTy, BoolTy,NoLocation)
                Not = Some ("boolean not", BoolTy,NoLocation)
        }    

let private unknownMeta =
        {
            Add = Some("add",UnknownTy,UnknownTy,NoLocation)
            Sub = Some("sub",UnknownTy,UnknownTy,NoLocation)
            Mul = Some("mul",UnknownTy,UnknownTy,NoLocation)
            Div = Some("div",UnknownTy,UnknownTy,NoLocation)
            Mod = Some("mod",UnknownTy,UnknownTy,NoLocation)
            Pow = Some("pow",UnknownTy,UnknownTy,NoLocation)
            Unm = Some("negate",UnknownTy,NoLocation)
            Concat = Some("..",UnknownTy,UnknownTy,NoLocation)
            Len = Some("len", UnknownTy,NoLocation)
            Lt = Some("less than",UnknownTy,UnknownTy,NoLocation)
            Le = Some("less than or equal",UnknownTy,UnknownTy,NoLocation)
            Gt = Some("greater than",UnknownTy,UnknownTy,NoLocation)
            Ge = Some("greater or equal",UnknownTy,UnknownTy,NoLocation)
            And = Some("and",UnknownTy,UnknownTy,NoLocation)
            Or = Some("or",UnknownTy,UnknownTy,NoLocation)
            Not = Some("not",UnknownTy,NoLocation)
            Index = Some("index",UnknownTy,UnknownTy,NoLocation)
            Call = None
        }           


type MetamethodSet with

    /// creates a metamethod set for an array type having
    /// the given element type
    static member ForArrayTy(elemTy : Type) =
        { 
        MetamethodSet.standard with
            Index = Some("index into array",NumberTy,elemTy,NoLocation)
            Len = Some("length of array",NumberTy,NoLocation)
        }

    /// creates a method set for an array type having
    /// the given domain and codomain types
    static member ForMap(domTy : Type, codTy: Type) =
        {
        MetamethodSet.standard with
            Index = Some("index into array",domTy,NillableTy(codTy),NoLocation)
        }

type Type with

    static member Compose (env : TypeEnvironment) (tyA : Type) (tyB : Type) =
        // Contract.Requires(fst (Type.IsSubtypeOf env tyA tyB))
        // Contract.Ensures(fst (Type.IsSubtypeOf env (Contract.Result<Type>()) tyB))
        
        match (tyA,tyB) with
        | (UnknownTy,ty)
        | (ty, UnknownTy) ->
            ty
        | (ty, NewFieldTy) ->
            ty
        | (NillableTy(a), NillableTy(b)) ->
            NillableTy(Type.Compose env a b)
        | (a, NillableTy(b)) ->
            Type.Compose env a b
        | (TupleTy(a :: _,_),b) 
        | (a, TupleTy(b :: _,_)) ->
            Type.Compose env a b
        | ((RecordTy(nameA,descA,isStructuralA,displayStructuralA,isOpenA,metamethodsA,fieldsA,fieldListA,methodsA,typeArgsA,defLocA) as tyA), 
           (RecordTy(nameB,_,true,_,_,metamethodsB,fieldsB,fieldListB,methodsB,typeArgsB,defLocB) as tyB)) ->
            
           let composeField (acc : Map<string,Field>) (key : string) _ =
             let ty' = Type.Compose env fieldsA.[key].ty fieldsB.[key].ty
             acc.Add(key, { fieldsA.[key] with ty = ty' })
           
           let fields = Map.fold composeField fieldsA fieldsB

           let composeMethod (acc : Map<string,Method>) (key : string) _ =
             let ty' = Type.Compose env methodsA.[key].ty methodsB.[key].ty
             acc.Add(key, { methodsA.[key] with ty = ty' })

           let methods = Map.fold composeMethod methodsA methodsB
           let metamethods = MetamethodSet.Compose env metamethodsA metamethodsB
           let typeArgs = List.map2 (fun tyA tyB -> (Type.Compose env tyA tyB)) typeArgsA typeArgsB
           RecordTy(nameA,descA,isStructuralA,displayStructuralA,isOpenA,metamethods,fields,Map.toList fields,methods,typeArgs,defLocA)

        | ((RecordTy(nameA,_,_,_,_,_,_,_,_,_,_) as tyA), (RecordTy(nameB,_,false,_,_,_,_,_,_,_,_) as tyB)) ->
            tyA     
        | (FunctionTy(desc,paramsA,varParsA,retsA,varRetsA,meth,latent,defLoc), FunctionTy(_,paramsB,varParsB,retsB,varRetsB,_,_,_)) ->
            let maxLen = max paramsA.Length paramsB.Length

            let parFillerA =
                if varParsA.IsSome then
                    UnknownTy
                else
                    NilTy

            let parFillerB =
                if varParsB.IsSome then
                    UnknownTy
                else
                    NilTy

            let paramsA = sizeListTo paramsA maxLen ("","",parFillerA)
            let paramsB = sizeListTo paramsB maxLen ("","",parFillerB)

            let maxLen = max retsA.Length retsB.Length

            let retFillerA =
                if varRetsA.IsSome then
                    UnknownTy
                else
                    NilTy

            let retFillerB =
                if varRetsB.IsSome then
                    UnknownTy
                else
                    NilTy

            let retsA = sizeListTo retsA maxLen ("",retFillerA)
            let retsB = sizeListTo retsB maxLen ("",retFillerB)

            let composeParam (paramA : Param) (paramB : Param) =
                let (nameA,descA,tyA),(_,_,tyB) = paramA,paramB
                let ty = Type.Compose env tyA tyB
                (nameA,descA,ty)
            
            let composeRet (retA : Ret) (retB : Ret) =
                let (descA, tyA),(_,tyB) = retA,retB
                let ty = Type.Compose env tyA tyB
                (descA,ty)

            let pars = List.map2 composeParam paramsA paramsB
            let rets = List.map2 composeRet retsA retsB

            let varPars = 
                if varParsA.IsSome && varParsB.IsSome then
                    varParsA
                else
                    None

            let varRets =
                if varRetsA.IsSome && varRetsB.IsSome then
                    varRetsA
                else
                    None

            FunctionTy(desc,pars,varPars,rets,varRets,meth,latent,defLoc)
        | (x,y) ->
            x

    /// Replace all occurrences of type variable having name typeVarName in target with substitution    
    static member Substitute (typeVarName : string) (substitution : Type) (target : Type) =
        //capture avoidance should not be an issue due to the below contract (which I am too lazy to implement)
        //Contract.Requires(Type.AbsFree target)
        match target with
        | NumberTy 
        | StringTy
        | NilTy
        | BoolTy
        | NewFieldTy
        | ErrorTy(_)
        | UnknownTy ->
            target
        | RecordTy(name,desc,isStructural,displayStructural,openStatus,metamethods,fieldMap,fieldList,methodMap,typeArgs,defLocation) ->
            let fieldMap' = Map.map (fun key (field : Field) -> {field with ty = Type.Substitute typeVarName substitution field.ty} ) fieldMap
            let fieldList' = List.map (fun (key : string, field : Field) -> (key, {field with ty = Type.Substitute typeVarName substitution field.ty})) fieldList
            let methodMap' = Map.map (fun key (meth : Method) -> {meth with ty = Type.Substitute typeVarName substitution meth.ty} ) methodMap
            let metamethods' = MetamethodSet.Substitute typeVarName substitution metamethods
            let typeArgs' = (substitution :: typeArgs)
            RecordTy(
                name,
                desc,
                isStructural,
                displayStructural,
                openStatus,
                metamethods',
                fieldMap',
                fieldList',
                methodMap',
                typeArgs',
                defLocation
            )
        | FunctionTy(desc,pars,varPars,rets,varRets,meth,latent,deflocation) ->         
            let pars' = List.map (fun (name,desc,ty) -> (name,desc,Type.Substitute typeVarName substitution ty)) pars
            let rets' = List.map (fun (desc,ty) -> (desc, Type.Substitute typeVarName substitution ty)) rets
            FunctionTy(desc,pars',varPars,rets',varRets,meth,latent,deflocation)
        | NillableTy(ty) ->
            NillableTy(Type.Substitute typeVarName substitution ty)
        | UserDefinedTy(name,rng) ->
            UserDefinedTy(name,rng)
        | TupleTy(tyList,indefinite) ->
            TupleTy(List.map (Type.Substitute typeVarName substitution) tyList,indefinite)
        | OverloadTy(tyList) ->
            OverloadTy(List.map (Type.Substitute typeVarName substitution) tyList)
        | AccumulatedTy(perm,temp) ->
            AccumulatedTy(
                Type.Substitute typeVarName substitution perm,
                Type.Substitute typeVarName substitution temp
            )
        | TypeAbsTy(nm,body) ->
            TypeAbsTy(nm, Type.Substitute typeVarName substitution body)
        | TypeVarTy(nm,desc,defLoc,rng) ->
            if nm = typeVarName then
                substitution
            else
                TypeVarTy(nm,desc,defLoc,rng)
        | TypeAppTy(abstraction, arguments)->
            TypeAppTy(
                Type.Substitute typeVarName substitution abstraction,
                List.map (Type.Substitute typeVarName substitution) arguments
            )

    //
    static member Apply (abs : Type) (args : List<Type>) =
        match abs with
        | TypeAbsTy(varNames,body) ->
            if (varNames.Length = args.Length) then
                let bindings = List.zip (List.map fst varNames) args
                List.fold (fun body (varName,argTy) -> Type.Substitute varName argTy body) body bindings
            else
                UnknownTy
        | _ ->
            UnknownTy

    static member TypeEval (env : TypeEnvironment) (ty : Type) =
        match ty with
        | TypeAppTy(applicand,args) ->
            Type.Apply (Type.Unfold env applicand) args
        | UserDefinedTy(name,deflocation) ->
            Type.Unfold env ty
        | _ ->
            ty

    /// This is a [] operator definition that allows you to access the fields
    /// of a record type using recTy.[fieldName]
    member this.Item (name : string) =
        match this with
        | RecordTy(_,_,_,_,_,_,fields,_,_,_,_) ->
            fields.[name]
        | AccumulatedTy(_,underlying)
        | NillableTy(underlying) ->
            underlying.[name]
        | _ ->
            failwith "tried to access a field of some non-record type"
                
    /// True iff NilTy is a subtype of ty
    static member Nillish (tenv : TypeEnvironment) (ty : Type) =
        fst (Type.IsSubtypeOf tenv NilTy ty)

/// Used for when someone tries to override a method using an incorrect 
/// type signature, or when a plugin author has set up a bogus nominal subtyping
/// relation which doesn't respect structural subtyping.
///
/// error message, fileName, rng
exception StructuralSubtypingError of string*string*(int*int)

/// Reference to the following function:
let private refIsStructuralSubtypeOf =    
    ref (fun _ _ _ -> ErrorAndLoc("","",(0,0)))

/// (env : TypeEnvironment) - the type environment to reason under
/// (a : Type) - The proposed subtype
/// (b : Type) - The proposed supertype
///
/// If a is a subtype of b, returns None
/// Otherwise, returns Some(msg : string, fileName : string, rng : Range)
/// containing an error which explains why a is not a subtype of b under env.
let private isStructuralSubtypeOf (env : TypeEnvironment) (a : Type) (b : Type) =
    !refIsStructuralSubtypeOf env a b

/// Raises an error if a is not structurally a subtype of b w.r.t. env.
let checkStructuralSubtyping (env : TypeEnvironment) (a : Type) (b : Type) =
    let res = isStructuralSubtypeOf env a b
    match res with
    | ErrorAndLoc(expl,file,rng) ->
        raise(StructuralSubtypingError(expl,file,rng))
    | IsStructuralSubtype ->
        ()

         
/// env - The type environment to reason under.
/// opName - The name of a certain binary metamethod (i.e. "add", "sub", "mul").
/// nameA - The name of some object type A
/// binOpA - The type of A's opName metamethod 
/// nameB - The name of some object type B
/// binOpB - The type of B's opName metamethod
/// errorLocation - The location to use if an error is generated. (The default is the locations of the metamethod definitions).
///
/// Used as a subroutine for judging if A is a subtype of B w.r.t. env. Specifically, if the metamethod corresponding to
/// opName is the source of a subtyping mismatch between A and B, returns Some(errorMessage, fileName, range) 
/// explaining the subtyping error. Otherwise, returns None.
let private checkBinOpSubtyping (env : TypeEnvironment) 
                                (A : StructuralSubtypeResult)
                                (opName : string)
                                (nameA : string) 
                                (binOpA : Option<BinOpTy>) 
                                (nameB : string) 
                                (binOpB : Option<BinOpTy>) 
                                (errorLocation : Option<DefinitionLocation>) =

        match A with
        | ErrorAndLoc(_,_,_) ->
            A
        | IsStructuralSubtype ->
            match (binOpA,binOpB) with
            | (Some(descA,rhsTyA,retTyA,defLocationA), Some(descB,rhsTyB,retTyB,defLocationB)) ->
                let funcTyA = 
                    FunctionTy(
                        descA,
                        [("self","",UnknownTy);("rhs","",rhsTyA)],
                        None,
                        [("ret",retTyA)],
                        None,
                        true,
                        false,
                        defLocationA
                      )
                let funcTyB =
                    FunctionTy(
                        descB,
                        [("self","",UnknownTy);("rhs","",rhsTyB)],
                        None,
                        [("ret",retTyB)],
                        None,
                        true,
                        false,
                        defLocationB
                      )

                let res = isEqual env funcTyA funcTyB
                match res with
                | ErrorMsg(msg) ->
                    let msg' = "\nIn " + opName + " metamethod:"
                    let loc = if errorLocation.IsSome then errorLocation.Value else defLocationA
                    ErrorAndLoc(msg' + msg,fst loc, snd loc)
                | IsStrucutralSubtype ->
                    IsStructuralSubtype
            | (Some(_,_,_,_),None) ->
                IsStructuralSubtype
            | (None,Some(_,_,_,defLocationB)) ->
                let loc = if errorLocation.IsSome then errorLocation.Value else defLocationB
                ErrorAndLoc(nameA + " does not have " + opName + " metamethod.", fst loc, snd loc)
            | (None,None) ->
                IsStructuralSubtype

/// env - The type environment to reason under.
/// opName - The name of a certain unary metamethod (i.e. "unm", "len").
/// nameA - The name of some object type A
/// binOpA - The type of A's opName metamethod 
/// nameB - The name of some object type B
/// binOpB - The type of B's opName metamethod
/// errorLocation - The location to use if an error is generated. (The default is the locations of the metamethod definitions).
///
/// Used as a subroutine for judging if A is a subtype of B w.r.t. env. Specifically, if the metamethod corresponding to
/// opName is the source of a subtyping mismatch between A and B, returns Some(errorMessage, fileName, range) 
/// explaining the subtyping error. Otherwise, returns None.
let private checkUnOpSubtyping  (env : TypeEnvironment) 
                                (A : StructuralSubtypeResult)
                                (opName : string)
                                (nameA : string) 
                                (unOpA : Option<UnOpTy>) 
                                (nameB : string) 
                                (unOpB : Option<UnOpTy>) 
                                (errorLocation : Option<DefinitionLocation>) =
    match A with
    | ErrorAndLoc(_,_,_) ->
        A
    | IsStructuralSubtype ->
        match (unOpA,unOpB) with
        | (Some(descA,retTyA,defLocationA), Some(descB,retTyB,defLocationB)) ->
            let funcTyA = 
                FunctionTy(
                    descA,
                    [("self","",UnknownTy)],
                    None,
                    [("ret",retTyA)],
                    None,
                    true,
                    false,
                    defLocationA
                )
            let funcTyB =
                FunctionTy(
                    descB,
                    [("self","",UnknownTy)],
                    None,
                    [("ret",retTyB)],
                    None,
                    true,
                    false,
                    defLocationA
                )

            let res = isEqual env funcTyA funcTyB
            match res with
            | ErrorMsg(msg) ->
                let msg' = "\nIn " + opName + " metamethod:"
                let loc = if errorLocation.IsSome then errorLocation.Value else defLocationA
                ErrorAndLoc(msg' + msg, fst loc, snd loc)
            | IsSubtype ->
                IsStructuralSubtype
        | (Some(_,_,_),None) ->
            IsStructuralSubtype
        | (None,Some(_,_,defLocationB)) ->
            let loc = if errorLocation.IsSome then errorLocation.Value else defLocationB
            ErrorAndLoc(nameA + " does not have " + opName + " metamethod.", fst loc, snd loc)
        | (None,None) ->
            IsStructuralSubtype

/// env - The type environment to reason under.
/// nameA - The name of some object type A
/// metaA - A's metamethod set
/// nameB - The name of some object type B
/// metaB- B's metamethod set
/// errorLocation - A location to use if an error is generated. (A default will be used if none is provided).
///
/// Used as a subroutine for judging if A is a subtype of B w.r.t. env. For a subtype relation to hold, A's 
/// metamethod set must be a superset of B's metamethod set. Furthermore, any metamethods common between A
/// and B must have matching types. If this is not the case, we return an error of the form 
/// Some(errrorMessage,fileName,range) describing why subtyping between metamethod sets does not hold. 
/// Otherwise, we return None.
let private getSubtypeViolation (env : TypeEnvironment) 
                                (A0 : StructuralSubtypeResult)
                                (nameA : string) 
                                (metaA : MetamethodSet)
                                (nameB : string)
                                (metaB : MetamethodSet) 
                                (errorLocation : Option<DefinitionLocation>) =
       
    let binOps = [
        ("add",nameA,metaA.Add,nameB,metaB.Add)
        ("sub",nameA,metaA.Sub,nameB,metaB.Sub)
        ("mul",nameA,metaA.Mul,nameB,metaB.Mul)
        ("div",nameA,metaA.Div,nameB,metaB.Div)
        ("mod",nameA,metaA.Mod,nameB,metaB.Mod)
        ("pow",nameA,metaA.Pow,nameB,metaB.Pow)
        ("concat",nameA,metaA.Concat,nameB,metaB.Concat)
        ("'less than'",nameA,metaA.Lt,nameB,metaB.Lt)
        ("index",nameA,metaA.Index,nameB,metaB.Index)
    ]

    let unOps = [
        ("unm",nameA,metaA.Unm,nameB,metaB.Unm)
        ("len",nameA,metaA.Len,nameB,metaB.Len)
    ]

    let foldBinOp (A : StructuralSubtypeResult) ((opName,nameA,opA,nameB,opB) : string*string*Option<BinOpTy>*string*Option<BinOpTy>) =
        checkBinOpSubtyping env A opName nameA opA nameB opB errorLocation
        
    let foldUnOp (A : StructuralSubtypeResult) ((opName,nameA,opA,nameB,opB) : string*string*Option<UnOpTy>*string*Option<UnOpTy>) =
        checkUnOpSubtyping env A opName nameA opA nameB opB errorLocation

    let A1 = List.fold foldBinOp A0 binOps
    let A2 = List.fold foldUnOp A1 unOps

    match A2 with
    | ErrorAndLoc(_,_,_) ->
        A2
    | IsStructuralSubtype ->
        match (metaA.Call,metaB.Call) with
        | (Some(_,parsA,varParsA,retsA,varRetsA,methodA,latentA,deflocationA),Some(_,parsB,varParsB,retsB,varRetsB,methodB,latentB,deflocationB)) when latentA = latentB ->
            let funcTyA = FunctionTy("",parsA,varParsA,retsA,varRetsA,methodA,latentA,deflocationA)
            let funcTyB = FunctionTy("",parsB,varParsB,retsB,varRetsB,methodB,latentB,deflocationB)
            let res = isEqual env funcTyA funcTyB
            match res with
            | ErrorMsg(msg) ->
                ErrorAndLoc(msg, fst deflocationA, snd deflocationB)
            | IsSubtype ->
                IsStructuralSubtype
        | (Some(_,_,_,_,_,_,true,deflocationA),Some(_,_,_,_,_,_,false,_)) ->
            ErrorAndLoc(
                "\n" + nameA + " is latently callable, wheras " + nameB + " is non-latently callable", 
                fst deflocationA, 
                snd deflocationA
            )
        | (Some(_,_,_,_,_,_,false,deflocationA),Some(_,_,_,_,_,_,true,_)) ->
            ErrorAndLoc(
                "\n" + nameA + " is non-latently callable, wheras " + nameB + " is latently callable", 
                fst deflocationA, 
                snd deflocationA
            )
        | (None,Some(_,parsB,varParsB,retsB,varRetsB,methodB,latentB,deflocationB)) ->
            ErrorAndLoc("\n" + nameA + " is not callable, wheras " + nameB + " is callable.", fst deflocationB, snd deflocationB)
        | (Some(_,_,_,_,_,_,_,_),None) ->
            IsStructuralSubtype
        | (None,None) ->
            IsStructuralSubtype
        | _ ->
            failwith "unreachable"

refIsStructuralSubtypeOf := fun (env : TypeEnvironment) (a : Type) (b : Type) ->
    match (a,b) with
    | (UnknownTy, RecordTy(_,_,_,_,_,_,_,_,_,_,_))
    | (RecordTy(_,_,_,_,_,_,_,_,_,_,_), UnknownTy)
    | (UnknownTy,UnknownTy) ->
        // if, for some reason, an object type cannot be correctly constructed,
        // we might assign it type UnknownTy and still include it in our nominal
        // subtyping graph
        IsStructuralSubtype
    | (RecordTy(nameA,_,_,_,_,metamethodsA,fieldsA,fieldListA,methodsA,_,(fileA,rngA)),TypeAbsTy(_,_))
    | (TypeAbsTy(_,RecordTy(nameA,_,_,_,_,metamethodsA,fieldsA,fieldListA,methodsA,_,(fileA,rngA))),TypeAbsTy(_,_)) ->
        ErrorAndLoc("no type can be considered a subtype of type abstractions like " + b.ToString(), fileA,rngA)
    | (TypeAbsTy(_,RecordTy(nameA,_,_,_,_,metamethodsA,fieldsA,_,methodsA,_,(fileA,rngA))), RecordTy(nameB,_,_,_,_,metamethodsB,fieldsB,_,methodsB,_,(fileB,rngB)))
    | (RecordTy(nameA,_,_,_,_,metamethodsA,fieldsA,_,methodsA,_,(fileA,rngA)), RecordTy(nameB,_,_,_,_,metamethodsB,fieldsB,_,methodsB,_,(fileB,rngB))) ->
        let foldMethod (A : StructuralSubtypeResult) (methNameB : string) (methodB : Method) =
            match A with
            | ErrorAndLoc(_,_,_) ->
                A
            | IsStructuralSubtype ->
                if not (methodsA.ContainsKey methNameB) then
                    ErrorAndLoc("type " + a.ToString() + " does not contain " + methNameB + " method, as type " + b.ToString() + " does.", fileA, rngA)
                else
                    let methodA = methodsA.Item methNameB
                    let (fileA,rngA) = methodA.loc
                    let methNameA = methNameB

                    let res = isSubtypeOfAux env methodA.ty methodB.ty
                    match res with
                    | ErrorMsg(msg) ->
                        let msg' = "\ntype of " + nameA + ":"+methNameA + " " + methodA.ty.ToString() + ", is not a subtype of " + nameB+":"+methNameB + " " + methodB.ty.ToString()
                        ErrorAndLoc(msg'+msg, fileA, rngA)
                    | IsSubtype ->
                        IsStructuralSubtype

        let foldField (A : StructuralSubtypeResult) (fieldNameB : string) (fieldB : Field) =
            match A with
            | ErrorAndLoc(_,_,_) ->
                A
            | IsStructuralSubtype ->
                if not (fieldsA.ContainsKey fieldNameB) then
                        ErrorAndLoc("\nrecord does not contain " + fieldNameB + " field ", fileA,rngA)
                else
                    let fieldA = fieldsA.Item fieldNameB
                    let (fieldFileA,fieldRngA) = fieldA.loc
                    if fieldA.isConst && fieldB.isConst then
                        let res = isSubtypeOfAux env fieldA.ty fieldB.ty
                        match res with
                        | ErrorMsg(msg) ->
                            let msg' = "\ntype of " + fieldNameB + ", " + fieldA.ty.ToString() + ", is not a subtype of " + fieldB.ty.ToString()
                            ErrorAndLoc(msg' + msg, fieldFileA, fieldRngA)
                        | IsSubtype ->
                            IsStructuralSubtype
                    elif (not fieldA.isConst) && (not fieldB.isConst) then
                        let res = isEqual env fieldA.ty fieldB.ty
                        match res with
                        | ErrorMsg(msg) ->
                            let msg' = "\n" + fieldNameB + " does not match between " + nameA + " and " + nameB
                            ErrorAndLoc(msg' + msg, fieldFileA, fieldRngA)
                        | IsSubtype ->
                            IsStructuralSubtype
                    elif (not fieldA.isConst) && fieldB.isConst then
                        ErrorAndLoc("\n" + fieldNameB + " is const in " + b.ToString() + " and not " + a.ToString() + ".", fieldFileA, fieldRngA)
                    else
                        ErrorAndLoc("\n" + fieldNameB + " is const in " + a.ToString() + " and not " + b.ToString() + ".", fieldFileA, fieldRngA)

        let A1 = Map.fold foldMethod IsStructuralSubtype methodsB
        let A2 = Map.fold foldField A1 fieldsB
        getSubtypeViolation env A2 nameA metamethodsA nameB metamethodsB (Some(fileA,rngA))
    | (_,_) ->
        failwith "unreachable"


type Type with


    /// tenv - the type environment we are apply the accumulation under
    /// baseField - the field that path indexes into
    /// path - a list of lookups into baseField, which yield the field we are making an accumulation on
    /// targetField - the target field/type that we are accumulating  
    ///
    /// Given that a sequence of indexes into baseField (the strings of path) identifies a
    /// field which we intend to accumulate targetField.ty onto, returns a modified version
    /// of baseField.ty, such that the result is equal to baseField.ty, except with the specified
    /// accumulation performed.
    static member ApplyAccumulation (tenv : TypeEnvironment) 
                                    (baseField : Field) 
                                    (path : List<string>) 
                                    (targetField : Field) =
                                     
        let rec applyAt (accField : Field) (path : List<string>) (targetField : Field) =
            match path with
            | [last] ->
                let ty = Type.Coerce tenv accField.ty
                let lastField = 
                    match ty with
                    | RecordTy(_,_,_,_,_,_,fields,_,_,_,_) when fields.ContainsKey last ->
                        // for open or closed records containing the field, we use the existing field
                        ty.[last]
                    | RecordTy(_,_,_,_,Open,_,_,_,_,_,_) ->
                        // for open records which do not contain the field, this must be a new field
                        targetField
                    | _ ->
                        Field.OfType(UnknownTy)
                        
                let newLast = { 
                    lastField with 
                        ty = Type.MakeAccumulatedTy tenv lastField.ty targetField.ty 
                }

                // dig through type accumulations until a non-accumulated type is
                // encountered, replace that type with the new accumulation
                let rec replace (ty : Type) =
                    match ty with 
                    | RecordTy(a,b,c,d,e,f,fields,g,h,i,j) ->
                        RecordTy(a,b,c,d,e,f,fields.Add(last,newLast),g,h,i,j)
                    | UserDefinedTy(typename, rng) ->
                        tenv.typeMap.[typename]
                    | AccumulatedTy(a, b) ->
                        AccumulatedTy(a, replace b)
                    | _ ->
                        assert(false)
                        UnknownTy
                replace accField.ty
            | head :: rest ->
                match Type.Coerce tenv accField.ty with 
                | RecordTy(a,b,c,d,e,f,fields,g,h,i,j) ->
                    let headField = 
                        if fields.ContainsKey head then
                            fields.[head]
                        else
                            Field.OfType(UnknownTy)
                    let accumulated = { headField with ty = applyAt headField rest targetField }
                    RecordTy(a,b,c,d,e,f,fields.Add(head,accumulated),g,h,i,j)
                | AccumulatedTy(a, RecordTy(b,c,d,e,f,g,fields,h,i,j,k)) ->
                    let headField = 
                        if fields.ContainsKey head then
                            fields.[head]
                        else
                            Field.OfType(UnknownTy)
                    let accumulated = { headField with ty = applyAt headField rest targetField }
                    AccumulatedTy(a,RecordTy(b,c,d,e,f,g,fields.Add(head,accumulated),h,i,j,k))                  
                | _ ->
                    UnknownTy
            | [] ->
                failwith "unreachable"
              
        if path.IsEmpty then
            Type.MakeAccumulatedTy tenv baseField.ty targetField.ty
        else   
            applyAt baseField path targetField
    
    /// Let path = [p0;p1;...;pn]. If field.p0.p1...pn has an accumulated 
    /// type, we return the bound part of that type (i.e. with accumulation removed).
    /// otherwise, we return its type as is.
    static member UndoAccumulation (tenv : TypeEnvironment) (path : List<string>) (field : Field) =
        let rec undoAt (path : List<string>) (accField : Field) =
            match path with
            | [last] ->
                let accFieldCoerced : Type = Type.Coerce2 tenv accField.ty
                let lastField = accFieldCoerced.[last]

                let undoneLast =
                    match lastField.ty with
                    | AccumulatedTy(perm,temp) ->
                        { lastField with ty = perm }
                    | _ ->
                        lastField

                match accField.ty with 
                | RecordTy(a,b,c,d,e,f,fields,g,h,i,j) ->
                    RecordTy(a,b,c,d,e,f,fields.Add(last,undoneLast),g,h,i,j)
                | AccumulatedTy(a,RecordTy(b,c,d,e,f,g,fields,h,i,j,k)) ->
                    AccumulatedTy(a,RecordTy(b,c,d,e,f,g,fields.Add(last,undoneLast),h,i,j,k))
                | _ ->
                    UnknownTy           
                
            | head :: rest ->
                match accField.ty with 
                | RecordTy(a,b,c,d,e,f,fields,g,h,i,j) ->
                    let headField = fields.[head]
                    let undone = { headField with ty = undoAt rest headField }
                    RecordTy(a,b,c,d,e,f,fields.Add(head,undone),g,h,i,j)
                | AccumulatedTy(a,RecordTy(b,c,d,e,f,g,fields,h,i,j,k)) ->
                    let headField = fields.[head]
                    let undone = { headField with ty = undoAt rest headField }
                    AccumulatedTy(a,RecordTy(b,c,d,e,f,g,fields.Add(head,undone),h,i,j,k))
                | _ ->
                    UnknownTy
            | _ ->
                failwith "UndoDeduction should not be called with empty fields lists"

        if path.IsEmpty then
            match field.ty with
            | AccumulatedTy(perm,temp) ->
                perm
            | _ ->
                field.ty
        else   
            undoAt path field

    /// Recursively remove all accumulations from this type and all
    /// of the the types contained in it (i.e. the fields of a record, 
    /// the underlying type of a nillable type, etc.)
    static member UndoAccumulations (ty : Type) =
        match ty with
        | RecordTy(a,b,c,d,e,f,fields,g,h,i,loc) ->
            RecordTy(a,b,c,d,e,f,Map.map (fun k f -> {f with ty=Type.UndoAccumulations f.ty}) fields,g,h,i,loc)
        | NillableTy(ty) ->
            NillableTy(Type.UndoAccumulations ty)
        | AccumulatedTy(perm,temp) ->
            Type.UndoAccumulations perm
        | _ ->
            ty

    /// Generate an accumulated type for when a field of type original 
    /// has been determined to have type accumulated.
    static member MakeAccumulatedTy (env : TypeEnvironment) (original : Type) (accumulated : Type) =
        
        let perm =
            match original with
            | AccumulatedTy(permOriginal,_) ->
                permOriginal
            | _ ->
                original

        let temp =
            match accumulated with
            | AccumulatedTy(_,accumulatedOriginal) ->
                accumulatedOriginal
            | _ ->
                accumulated
        
        if perm = temp then
            perm
        else
            AccumulatedTy(perm,Type.Compose env temp perm)
    

    /// When an expression is an argument of an operator (unary operand, binary operand,
    /// callee, or table/object being indexed), we Coerce its type as a first
    /// step to typechecking the operator.
    ///
    /// -Converts a tuple type into the first underlying type in the tuple
    /// -Converts an accumulated type into its underlying temporary type
    /// -Unfolds user defined types
    /// -Evaluates type application
    /// -and recurses if needed.
    static member Coerce (env : TypeEnvironment) (ty : Type) =

        // A - all intermediate forms of the type being coerced so far
        // ty the current form of the type being coerced
        let rec coerceAux (A : Set<Type>) (ty : Type) =
            if A.Contains ty then
                //TODO: should be Bot actually, but we don't have that
                UnknownTy
            else
                let A = A.Add ty
                match ty with
                | TupleTy(head::_,indefinite) ->
                    coerceAux A head
                | TupleTy([],true) ->
                    NilTy
                | AccumulatedTy(_,temp) ->
                    coerceAux A temp
                | UserDefinedTy(_) ->
                    coerceAux A (Type.Unfold env ty)
                | TypeAppTy(applicand,args) ->
                    Type.Apply (Type.Unfold env (coerceAux A applicand)) args
                | _ ->
                    ty

        coerceAux Set.empty ty



    /// Suppose an accumulated, nillable, or user defined type is used as the expected type of an expression in check mode.
    /// This function appropriately removes the aformentioned type decorators: accumulated types are converted to their bound
    /// types, nillable types are converted to their underlying types, and user-defined types are unfolded.
    static member Coerce2 (env : TypeEnvironment) (ty : Type) =
        
        let rec coerce2Aux (A : Set<Type>) (ty : Type) =
            if A.Contains ty then
                UnknownTy
            else
                let A = A.Add ty
                match ty with
                | NillableTy(ty) ->
                    coerce2Aux A ty
                | AccumulatedTy(bound,_) ->
                    coerce2Aux A bound
                | UserDefinedTy(_) ->
                    coerce2Aux A (Type.Unfold env ty)
                | _ ->
                    ty
        
        coerce2Aux Set.empty ty

refSubstTy := Type.Substitute

let private reachable (env : TypeEnvironment) (src : string) (target : string) =
    let edges = env.edges
    let visited = ref( new Set<string>([]) )

    let rec reachableAux (src : string) (target : string) =
        if src = target then
            true
        elif (!visited).Contains src then
            false
        else
            visited := (!visited).Add(src)
            let children = edges.Item src
            List.fold (fun state child -> state || (reachableAux child target)) false children
            
    reachableAux src target
        
refIsSubtypeOfAux := fun (env : TypeEnvironment) (a : Type) (b : Type) ->
    match (a,b) with
    | (UserDefinedTy(nameA,_), UserDefinedTy(nameB,_)) when nameA = nameB ->
        IsSubtype
    | (TypeAppTy(UserDefinedTy(nameA,_),_),_)
    | (UserDefinedTy(nameA,_),_) ->
        if a = b || assumptions.Contains(a,b) then
            IsSubtype
        else
            ignore (assumptions.Add(a,b))
            isSubtypeOfAux env (Type.TypeEval env a) b
    | (_,TypeAppTy(_,_))
    | (_,UserDefinedTy(_)) ->
        if a = b  || assumptions.Contains(a,b) then
            IsSubtype
        else
            ignore (assumptions.Add(a,b))
            isSubtypeOfAux env a (Type.TypeEval env b)
    | _ ->
        match (a,b) with
        | (UnknownTy,_)
        | (_, UnknownTy) ->
            IsSubtype
        | (_, NewFieldTy) ->
            IsSubtype
        | (BoolTy, BoolTy)
        | (NumberTy, NumberTy)
        | (NilTy, NilTy) ->
            IsSubtype
        | (AccumulatedTy(_,a), b) ->
            isSubtypeOfAux env a b
        | (a, AccumulatedTy(b,_)) ->
            isSubtypeOfAux env a b
        | (NilTy, NillableTy(_)) ->
            IsSubtype
        | (NillableTy(a),NillableTy(b))
        | (a, NillableTy(b)) ->  
            isSubtypeOfAux env a b
        | (TupleTy(a :: _,_),b) 
        | (a, TupleTy(b :: _,_)) ->
            isSubtypeOfAux env a b
        | (TupleTy([],_),NilTy)
        | (NilTy,TupleTy([],_)) ->
            IsSubtype
        | (TupleTy([],true),_) 
        | (_,TupleTy([],true)) ->
            IsSubtype
        | ((RecordTy(nameA,_,_,_,_,_,_,_,_,_,_) as tyA), (RecordTy(nameB,_,true,_,_,_,_,_,_,_,_) as tyB)) ->
            let res = isStructuralSubtypeOf env tyA tyB
            match res with
            | IsStructuralSubtype ->
                IsSubtype
            | ErrorAndLoc(msg,_,_) ->
                ErrorMsg(msg)
        | ((RecordTy(nameA,_,true,_,_,_,_,_,_,_,_) as tyA), (RecordTy(nameB,_,false,_,_,_,_,_,_,_,_) as tyB)) ->
            ErrorMsg("\nstructural tables such as " + nameA + " are never subtypes of nominal tables such as " + nameB)
        | ((RecordTy(nameA,_,false,_,_,_,_,_,_,typeArgsA,_) as tyA), (RecordTy(nameB,_,false,_,_,_,_,_,_,typeArgsB,_) as tyB)) ->
            if typeArgsA.Length <> typeArgsB.Length then
                ErrorMsg("\ntypes have differing number of type arguments: " + nameA + " has " + typeArgsA.Length.ToString() + " while " + nameB + " has " + typeArgsB.Length.ToString() + ".")
            else 
                let foldTypeArgs res (i,a,b) =
                    match res with
                    | ErrorMsg(msg) ->
                        let msg' = "\n" + orderStr(i) + " type arguments do not match."
                        ErrorMsg(msg' + msg)
                    | IsSubtype ->
                        isEqual env a b
                        
                match List.fold foldTypeArgs IsSubtype (List.zip3 [1..typeArgsA.Length] typeArgsA typeArgsB) with
                | ErrorMsg(msg) ->
                    ErrorMsg(msg)
                | IsSubtype ->
                    if reachable env nameA nameB then
                        IsSubtype
                    else
                        ErrorMsg("\n" + nameA + " and " + nameB + " are not related by nominal subtyping") 
        | (CallableTy(_,_,_,_,_,_,true,_), FunctionTy(_,_,_,_,_,_,false,_)) ->
            ErrorMsg("\n" + a.ToString() + " is latent whereas " + b.ToString() + " is not.")
        | (CallableTy(_,_,_,_,_,_,false,_), FunctionTy(_,_,_,_,_,_,true,_)) ->
            ErrorMsg("\n" + a.ToString() + " is not latent, whereas " + b.ToString() + " is.")
        | (CallableTy(_,paramsA,varParsA,retsA,varRetsA,_,_,_), FunctionTy(_,paramsB,varParsB,retsB,varRetsB,_,_,_)) ->
            let maxLen = max paramsA.Length paramsB.Length

            let parFillerA =
                if varParsA.IsSome then
                    UnknownTy
                else
                    NilTy

            let parFillerB =
                if varParsB.IsSome then
                    UnknownTy
                else
                    NilTy

            let paramsA = sizeListTo paramsA maxLen ("","",parFillerA)
            let paramsB = sizeListTo paramsB maxLen ("","",parFillerB)

            let maxLen = max retsA.Length retsB.Length

            let retFillerA =
                if varRetsA.IsSome then
                    UnknownTy
                else
                    NilTy

            let retFillerB =
                if varRetsB.IsSome then
                    UnknownTy
                else
                    NilTy

            let retsA = sizeListTo retsA maxLen ("",retFillerA)
            let retsB = sizeListTo retsB maxLen ("",retFillerB)

            let foldParamContra res ((s0A,s1A,tA),(s0B, s1B, tB),i) =
                match res with
                | ErrorMsg(_) ->
                    res
                | IsSubtype ->
                    match isSubtypeOfAux env tB tA with
                    | IsSubtype ->
                        IsSubtype
                    | ErrorMsg(msg) ->
                        let msg' = "\ntype of " + (orderStr i) + " argument (" + tA.ToString() + ") is not a supertype of " + tB.ToString()
                        ErrorMsg(msg' + msg)

            let foldRetCov res ((_,tyA),(_,tyB),i) =
                match res with
                | ErrorMsg(_) ->
                    res
                | IsSubtype ->
                    match isSubtypeOfAux env tyA tyB with
                    | ErrorMsg(msg) ->
                        let msg' = "\ntype of " + (orderStr i) + " return value (" + tyA.ToString() + ") is not a subtype of " + tyB.ToString()
                        ErrorMsg(msg' + msg)
                    | IsSubtype ->
                        IsSubtype
                
            let r1 = List.fold foldParamContra IsSubtype (List.zip3 paramsA paramsB [1..paramsA.Length])
            List.fold foldRetCov r1 (List.zip3 retsA retsB [1..retsA.Length])
        | (TypeVarTy(nameA,_,_,_),TypeVarTy(nameB,_,_,_)) when nameA = nameB ->
            IsSubtype
        | (TypeAppTy(_,_),_)
        | (UserDefinedTy(_),_) ->
            isSubtypeOfAux env (Type.TypeEval env a) b
        | (_,TypeAppTy(_,_))
        | (_,UserDefinedTy(_)) ->
            isSubtypeOfAux env a (Type.TypeEval env b)
        | _ ->
            ErrorMsg("")

type TypeEnvironment with
    /// Raises an exception if any of the edges in the nominal subtyping graph
    /// violates structural subtyping
    member this.CheckStructuralSubtyping () =
        // Given the name of a type, along with a list of all
        // outgoing edges for that type, check that each edge
        // satisfies structural subtyping
        let checkType srcName outEdges =
            let checkEdge dstName =
                let srcType = this.typeMap.Item srcName
                let dstType = this.typeMap.Item dstName
                checkStructuralSubtyping this srcType dstType

            List.iter checkEdge outEdges
        Map.iter checkType this.edges

/// Given a type environment and a type, return the metamethod set for 
/// the given type, using the environment to look up metamethod sets for named object types.
let rec getMetamethodSet (env : TypeEnvironment) (ty : Type) : MetamethodSet =
    match Type.Coerce env ty with    
    | UserDefinedTy(x,(0,0)) ->
        match env.typeMap.TryFind x with
        | Some (y) ->
            getMetamethodSet env y
        | None ->
            failwith "unreachable"
    | NumberTy ->
        numberMeta
    | BoolTy ->
        boolMeta
    | RecordTy(_,_,_,_,_,metaset,_,_,_,_,_) ->
        metaset
    | UnknownTy ->
        unknownMeta
    | _ ->
        MetamethodSet.empty

typeEval := Type.TypeEval