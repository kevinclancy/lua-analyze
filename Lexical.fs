﻿module Lexical

open FParsec
open FParsec.Primitives
open FParsec.CharParsers

type UserState = unit
type Parser<'t> = Parser<'t, unit>

type T = Parser<unit>

let nameChar = asciiLetter <|> digit <|> (pchar '_')

let pos p = pipe3 getPosition p getPosition (fun x y z -> (y, (x.Index,z.Index)))

let longComment =
    let parseEq : Parser<int64> =
        pos (many (skipChar '=')) |>> (fun (_, (l,h)) -> h-l) 
    
    let closer (l : int64) =
        let a = (skipString "]") >>? parseEq .>>? (skipString "]")
        let levelMatch x = 
            if (x = l) then
                preturn ()
            else
                pzero

        (a >>= levelMatch)

    let opener = (skipChar '[') >>? parseEq .>>? (skipChar '[')

    let singleLine l = (manyTill anyChar (lookAhead (skipNewline <|> (closer l))))

    let afterOpener l =
        (skipMany ((singleLine l) >>? skipNewline)) .>> ((singleLine l) >>? (closer l))

    (opener >>= afterOpener)

let comment : Parser<unit> = 
    (skipString "--") >>. (attempt longComment <|> (skipRestOfLine false)) 

let ws = spaces >>. (skipSepEndBy comment spaces)

let tokenTail = (notFollowedBy nameChar) >>. ws

let l_and : T = (skipString "and") .>> tokenTail <??> "and"
let l_break : T = (skipString "break") .>> tokenTail <??> "break"
let l_do : T = (skipString "do") .>> tokenTail <??> "do"
let l_else : T = (skipString "else") .>> tokenTail <??> "else"
let l_elseif : T = (skipString "elseif") .>> tokenTail <??> "elseif"
let l_end : T = (skipString "end") .>> tokenTail <??> "end"
let l_false : T = (skipString "false") .>> tokenTail <??> "false"
let l_for : T = (skipString "for") .>> tokenTail <??> "for"
let l_function : T = (skipString "function") .>> tokenTail <??> "function"
let l_if : T = (skipString "if") .>> tokenTail <??> "if"
let l_in : T = (skipString "in") .>> tokenTail <??> "in"
let l_local : T = (skipString "local") .>> tokenTail <??> "local"
let l_nil : T = (skipString "nil") .>> tokenTail <??> "nil"
let l_not : T = (skipString "not") .>> tokenTail <??> "not"
let l_or : T = (skipString "or") .>> tokenTail <??> "or"
let l_repeat : T = (skipString "repeat") .>> tokenTail <??> "repeat"
let l_return : T = (skipString "return") .>> tokenTail <??> "return"
let l_then : T = (skipString "then") .>> tokenTail <??> "then"
let l_true : T = (skipString "true") .>> tokenTail <??> "true"
let l_until : T = (skipString "until") .>> tokenTail <??> "until"
let l_while : T = (skipString "while") .>> tokenTail <??> "while"

let keywords = choice [
    l_and
    l_break
    l_do
    l_else
    l_elseif
    l_end
    l_false
    l_for
    l_function
    l_if
    l_in
    l_local
    l_nil
    l_not
    l_or
    l_repeat
    l_return
    l_then
    l_true
    l_until
    l_while
]

let l_ind   : T = (skipChar '.') .>> ws
let l_concat : T = (skipString "..") .>> ws <??> ".."
let l_varargs : T = (skipString "...") .>> ws <??> "..."
let l_equals : T = (skipString "==") .>> ws <??> "=="
let l_greaterOrEqual : T = (skipString ">=") .>> ws <??> ">="
let l_lessOrEqual : T = (skipString "<=") .>> ws <??> "<="
let l_notEqual : T = (skipString "~=") .>> ws <??> "~="

let l_size : T = (skipChar '#') .>> ws
let l_assign : T = (skipChar '=') .>> ws
let l_plus : T = (skipChar '+') .>> ws
let l_minus : T = (skipChar '-') .>> ws
let l_mult : T = (skipChar '*') .>> ws
let l_div : T = (skipChar '/') .>> ws
let l_pow : T = (skipChar '^') .>> ws
let l_comma : T = (skipChar ',') .>> ws
let l_openParen : T = (skipChar '(') .>> ws
let l_closeParen : T = (skipChar ')') .>> ws
let l_openBracket : T = (skipChar '{') .>> ws
let l_closeBracket : T = (skipChar '}') .>> ws
let l_openSquareBracket : T = (skipChar '[') .>> ws
let l_closeSquareBracket : T = (skipChar ']') .>> ws
let l_methodInd : T = (skipChar ':') .>> ws
let l_semiColon : T = (skipChar ';') .>> ws
let l_number : Parser<double> =
    let l_int = (many1 digit) 
    let decTail = (skipChar '.') .>> puint32
    let decLead = l_int >>. opt ( (pchar '.') >>. (opt l_int) ) >>% ()
    let exponent = (skipAnyOf "eE") .>> (opt (skipAnyOf "+-")) .>> l_int

    (decTail <|> decLead) >>. (opt exponent) >>. ws >>% 0.0

//pfloat .>> ws

//TODO: This needs to change... regex parsers are slow
let l_name : Parser<string> =
    (notFollowedBy (attempt keywords)) >>.
    (regex "(_|[a-zA-z])(_|[a-zA-Z0-9])*") .>> ws <??> "name"

let l_longString : Parser<string> =
    let parseEq : Parser<int64> =
        pos (many (skipChar '=')) |>> (fun (_, (l,h)) -> h-l) 
    
    let closer (l : int64) =
        let a = (skipString "]") >>? parseEq .>>? (skipString "]")
        let levelMatch x = 
            if (x = l) then
                preturn ()
            else
                pzero

        (a >>= levelMatch)

    let opener = (skipChar '[') >>? parseEq .>>? (skipChar '[')

    let singleLine l = (manyTill anyChar (lookAhead (skipNewline <|> (closer l))))

    let afterOpener l =
        (many ((singleLine l) >>? newline)) .>> ((singleLine l) >>? (closer l)) |>>
        (List.fold (fun (acc : string) (ch : char) -> acc + ch.ToString()) "")

    (opener >>= afterOpener) .>> ws

let l_string : Parser<string> =
    let dquote = (pchar '"')  
    let notdquote = 
        ( (pchar '\\') .>>. anyChar |>> (fun (x,y) -> [x;y]) ) <|>
        ( (noneOf "\"") |>> (fun x -> [x]) )
        
    let dquoteString = dquote >>. (many notdquote) .>> dquote .>> ws

    let quote = (pchar '\'')
    let notquote = 
        ( (pchar '\\') .>>. anyChar |>> (fun (x,y) -> [x;y]) ) <|>
        ( (noneOf "\'") |>> (fun x -> [x]) ) 
        
    let quoteString = quote >>. (many notquote) .>> quote .>> ws

    (quoteString |>> List.concat |>> string) <|> 
    (dquoteString |>> List.concat |>> string) <??> "string"

let l_eof = eof

let l_any = choice [
    l_name |>> ignore
    
    l_and
    l_break
    l_do
    l_else
    l_elseif
    l_end
    l_false
    l_for
    l_function
    l_if
    l_in
    l_local
    l_nil
    l_not
    l_or
    l_repeat
    l_return
    l_then
    l_true
    l_until
    l_while
    l_ind
    l_concat
    l_varargs
    l_equals
    l_greaterOrEqual
    l_lessOrEqual
    l_notEqual
    l_size
    l_assign
    l_plus
    l_minus
    l_div
    l_pow
    l_comma
    l_openParen
    l_openBracket
    l_closeSquareBracket
    l_methodInd
    l_semiColon
    l_number |>> ignore
    l_string |>> ignore
    l_longString |>> ignore
    l_eof
    skipAnyChar
]

let nextTokenRange = (getPosition .>> l_any) .>>. getPosition